﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TentacleDeath : MonoBehaviour
{
    public bool kill = false;

    private Animator anim;
    public List<Transform> childrenInOrder;
	public List<Rigidbody> rigidBodies;


	void Update() {
		if (kill) {
			kill = false;
			MakeRagdoll ();
		}
	}

    void Awake()
    {
        anim = GetComponent<Animator>();
        childrenInOrder = new List<Transform>();
        Transform currentTransform = transform;

        while (currentTransform.childCount > 0)
        {
            Transform temp = currentTransform.GetChild(0);
            childrenInOrder.Add(currentTransform);
            currentTransform = temp;
        }

        childrenInOrder.Add(currentTransform);

        //foreach (Transform temp in childrenInOrder)
           // Debug.Log(temp.name);

    }


    public void MakeRagdoll()
    {
        anim.Stop();
        anim.enabled = false;
        rigidBodies = new List<Rigidbody>();

        for (int i = 1; i < childrenInOrder.Count; i++)
        {
			childrenInOrder [i].gameObject.AddComponent<Rigidbody> ();
			rigidBodies.Add(childrenInOrder[i].GetComponent<Rigidbody>());
            rigidBodies[i - 1].useGravity = true;
            rigidBodies[i - 1].mass = 10.0f;
            rigidBodies[i - 1].drag = 1f;
            rigidBodies[i - 1].angularDrag = 1f;
        }

        rigidBodies[0].constraints = RigidbodyConstraints.FreezePosition;

        //Skip the root node
        for (int i = 2; i < childrenInOrder.Count; i++)
        {
            HingeJoint hinge = childrenInOrder[i].gameObject.AddComponent<HingeJoint>();
            hinge.connectedBody = rigidBodies[i-2];
            hinge.autoConfigureConnectedAnchor = false;
            hinge.useLimits = true;
            JointLimits limits = hinge.limits;
            limits.min = -300;
            limits.max = 300;
            hinge.limits = limits;

            hinge.axis = Vector3.forward;
        }
    }

			
}
