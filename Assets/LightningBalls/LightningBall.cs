﻿using UnityEngine;
using System.Collections;

public class LightningBall : MonoBehaviour {

    public Transform innerOrb;
    public Transform outerOrb;
    public Transform[] lightningArcs;
    private MeshRenderer[] lightningArcsMR;
    public float innerRotationSpeed = 40.0f;
    public float outerRotationSpeed = 10.0f;

    public float deltaScale = 0.2f;

    public Transform playerTarget;

	// Use this for initialization
	void Start () {
		playerTarget = GameObject.FindGameObjectWithTag("Head").transform;
        //Grab all the mesh renderes for each access
        lightningArcsMR = new MeshRenderer[lightningArcs.Length];
        for(int i = 0; i < lightningArcs.Length; i++)
        {
            lightningArcsMR[i] = lightningArcs[i].gameObject.GetComponent<MeshRenderer>();
        }
    }
	
    private void Spark()
    {
        //Get a random number of sparks to fire off
        int numberOfSparks = Random.Range(0, lightningArcsMR.Length);
        Vector3 delta = Vector3.one * Random.Range(1.0f - deltaScale, 1.0f + deltaScale);
        outerOrb.transform.localScale = delta;

        //apply random rotation facing player
        for (int i = 0; i < lightningArcsMR.Length; i++)
        {
            lightningArcs[i].LookAt(playerTarget.transform.position);
            lightningArcs[i].Rotate(Vector3.forward, Random.Range(-90.0f, 90.0f));
            lightningArcs[i].transform.localScale = delta;
        }


    }

	// Update is called once per frame
	void Update () {

        innerOrb.Rotate((Vector3.up + Vector3.right) * innerRotationSpeed * Time.deltaTime);
        outerOrb.Rotate((Vector3.up + -Vector3.right) * outerRotationSpeed * Time.deltaTime);
        Spark();

    }
}
