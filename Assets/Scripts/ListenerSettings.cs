﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioListener))]
public class ListenerSettings : MonoBehaviour {

	AudioListener ear;
	GameObject player;

	// Use this for initialization
	void Awake () {
		player = GameObject.FindGameObjectWithTag("Player");
		ear = GetComponent<AudioListener>();
		Debug.Log(ear.velocityUpdateMode);
		ear.velocityUpdateMode = AudioVelocityUpdateMode.Dynamic;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = player.transform.position+Vector3.up*1.67f;
	}
}
