﻿using UnityEngine;
using System.Collections;

public class EyeballAttack : MonoBehaviour {

	private bool angry;
	private Transform playerHead;
	private float counter;
	private GameObject projectile;

	public GameObject projectilePrefab;
	public float fireDelay = 4;
	public float distanceThresh = 25;

	// Use this for initialization
	void Start () {
	    
	}
	
	
	public void EyeActivate(Transform t)
	{
		if (!angry) {
			Debug.Log ("becoming angry");
			playerHead = t;
			angry = true;
			GetComponentInParent<EyeballManager> ().TrackPlayer (true);
			StartCoroutine (AngryMode ());
		}
	}

	IEnumerator AngryMode()
	{
		while (angry) {
			counter += Time.deltaTime;
			if (counter > fireDelay) {
				Attack ();
				counter = 0;
			}
			if (Vector3.Distance (playerHead.position, transform.position) > distanceThresh) {
				Debug.Log ("becoming calm");
				angry = false;
				GetComponentInParent<EyeballManager> ().TrackPlayer (false);
			}
			yield return null;
		}
	}

	void Attack()
	{
		projectile = Instantiate (projectilePrefab, this.transform.position, this.transform.rotation) as GameObject;
	}
}
