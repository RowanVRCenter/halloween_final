﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using UnityEngine.SceneManagement;


public class SkinnyMonster : MonoBehaviour {

	public bool attacking = false;
	private NavMeshAgent agent;
	private Transform target;
    private DamageEffect playerHealth;

    public float animStrikeLeftLength;


	public enum SkinnyMonsterState
	{
		idle,
		lumber,
		scared,
		strike,
		scream
	}

	public SkinnyMonsterHashIDs hashIds;
	public SkinnyMonsterState state;
	public AudioSource roar;

	protected Animator anim;

	// Use this for initialization
	void Awake () {
		agent = this.GetComponent<NavMeshAgent> ();
		anim = GetComponent<Animator>();
		hashIds = GetComponent<SkinnyMonsterHashIDs>();
		roar = GetComponent<AudioSource>();

		RuntimeAnimatorController ac = anim.runtimeAnimatorController;
		for (int i = 0; i < ac.animationClips.Length; i++) {
			if (ac.animationClips [i].name == "Strike_Left") {
				animStrikeLeftLength = ac.animationClips [i].length;
			}
		}
	
	}

	void Update() {
		if (target != null) {
			agent.destination = target.position;
		}

	}
		
	public void SetState( SkinnyMonsterState inputState)
	{
		switch(inputState)
		{
		case SkinnyMonsterState.idle:
			anim.SetBool(hashIds.lumber,false);
			anim.SetBool(hashIds.idleTurnHeadState,true);
			break;

		case SkinnyMonsterState.lumber:
			anim.SetBool(hashIds.lumber, true);
			break;

		case SkinnyMonsterState.scared:
			break;

		case SkinnyMonsterState.strike:
			anim.SetTrigger(hashIds.strikeLeftHash);
			break;

		case SkinnyMonsterState.scream:
			anim.SetTrigger(hashIds.screamHash);
			roar.Play();
			break;
		}
	}

	public void setTarget(Transform t) {
		target = t;
        //Debug.Log(target.gameObject);
	}

	void OnTriggerEnter(Collider c) {
        Debug.Log(c.gameObject);
		if ((c.tag == "Belt") && !attacking) {
			StartCoroutine (AttackSequence ());
		}
	}


	IEnumerator AttackSequence() {
		Debug.Log ("Monster attacking...");
		attacking = true;
		agent.Stop();
		anim.SetTrigger ("StrikeLeft");
		yield return new WaitForSeconds (animStrikeLeftLength);
		if (agent.remainingDistance < 2) {
            playerHealth = target.gameObject.GetComponent<DamageEffect>();
            playerHealth.Hit(1f);
            agent.Resume();
        } else {
			agent.Resume ();
		}
		attacking = false;
	}






	/*void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Door")
		{
			OpenGateDoor door = other.gameObject.GetComponent<OpenGateDoor>();
			if(door != null)
			{
			}
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(other.tag == "Door")
		{
			OpenGateDoor door = other.gameObject.GetComponent<OpenGateDoor>();
			if(door != null)
			{

			}
		}
	}*/

}
