﻿using UnityEngine;
using System.Collections;

public class TimeTransitionManager : MonoBehaviour {
	
	public int trigger1Hit = 1;
	public int trigger2Hit = 1;

	public GameObject[] firstLoopOffGameObjects;
	public GameObject[] firstOpenGates;
	public GameObject[] secondLoopGameobjects;
	public GameObject[] secondOpenGates;
	public GameObject[] thirdLoopGameObjects;
	public GameObject[] thirdOpenGates;
	public GameObject[] oldMeshLevel2;
	public Transform[] monsterPaths;
	public WaypointManager monsterPathManager;
	public GameObject[] firstPhaseFlesh;


	// Use this for initialization
	void Start () {
		Death deathScript = GameObject.FindGameObjectWithTag("Timer").GetComponent<Death>();
		trigger1Hit = deathScript.level2Loop;
		trigger2Hit = trigger1Hit;
		FirstPhase();
		if(trigger1Hit == 2){
			SecondPhase();
		}
	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetKeyDown("1")){
		   FirstPhase();
			trigger1Hit = 0;
			trigger2Hit = 0;
		}
		if(Input.GetKeyDown("2")){
			trigger1Hit = 1;
			trigger2Hit = 1;
			CheckPhase();
		}
		if(Input.GetKeyDown("3")){
			trigger1Hit = 2;
			trigger2Hit = 2;
			CheckPhase();
		}

	}

	void FirstPhase(){
		monsterPathManager.SetWaypointGroup(monsterPaths[0]);
		foreach(GameObject obj in firstOpenGates){
			obj.GetComponentInChildren<OpenGateDoor>().locked = false;
		}
		foreach(GameObject obj in thirdLoopGameObjects){
			obj.SetActive(false);
		}
		foreach(GameObject obj in secondLoopGameobjects){
			obj.SetActive(false);
		}
		
	}

	void FirstPhaseFlesh(){
		foreach(GameObject obj in firstPhaseFlesh){
			obj.SetActive(true);
		}
	}

	void SecondPhase(){
		monsterPathManager.SetWaypointGroup(monsterPaths[1]);
		foreach(GameObject obj in firstOpenGates){
			obj.GetComponentInChildren<OpenGateDoor>().locked = true;
		}
		foreach(GameObject obj in secondOpenGates){
			obj.GetComponentInChildren<OpenGateDoor>().locked = false;
		}
		foreach(GameObject obj in firstLoopOffGameObjects){
			obj.SetActive(false);
		}
		foreach(GameObject obj in secondLoopGameobjects){
			obj.SetActive(true);
		}
	}

	void ThirdPhase(){
		monsterPathManager.SetWaypointGroup(monsterPaths[2]);
		foreach(GameObject obj in secondOpenGates){
			obj.GetComponentInChildren<OpenGateDoor>().locked = true;
		}
		foreach(GameObject obj in thirdOpenGates){
			obj.GetComponentInChildren<OpenGateDoor>().locked = false;
		}
		foreach(GameObject obj in thirdLoopGameObjects){
			obj.SetActive(true);
		}
		foreach(GameObject obj in oldMeshLevel2){
			obj.SetActive(false);
		}
	}

	public void CheckPhase(){
		if(trigger1Hit == 2 && trigger2Hit == 1){
			SecondPhase();
		}else if(trigger1Hit == 3 && trigger2Hit == 2){
			ThirdPhase();
		}
	}

}
