﻿using UnityEngine;
using System.Collections;

public class TimeTrigger1 : MonoBehaviour {
		
	public TimeTransitionManager phaseManager;

	// Use this for initialization
	void OnTriggerEnter(Collider collider){
		if(collider.gameObject.tag == "Player"){
			if(phaseManager.trigger1Hit == phaseManager.trigger2Hit){
				phaseManager.trigger1Hit++;
				phaseManager.CheckPhase();
			}
		}
	}
}
