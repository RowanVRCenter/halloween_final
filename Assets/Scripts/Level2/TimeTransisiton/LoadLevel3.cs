﻿using UnityEngine;
using System.Collections;

public class LoadLevel3 : MonoBehaviour {

	void OnTriggerEnter(Collider collider){
		if(collider.gameObject.tag == "Player"){
			Application.LoadLevel(Application.loadedLevel+1);
		}
	}
}
