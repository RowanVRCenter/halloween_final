﻿using UnityEngine;
using System.Collections;

public class TimeTrigger2 : MonoBehaviour {

	public TimeTransitionManager phaseManager;
	
	// Use this for initialization
	void OnTriggerEnter(Collider collider){
		if(collider.gameObject.tag == "Player"){
			if(phaseManager.trigger2Hit == phaseManager.trigger1Hit - 1){
				phaseManager.trigger2Hit++;
				phaseManager.CheckPhase();
			}
		}
	}
}
