﻿using UnityEngine;
using System.Collections;

public class PickUpSpear : MonoBehaviour {

	public Animator anim;
	public GameObject fleshtubethingy;
	public AudioClip OpenMouthclip;

	void OnTriggerStay(Collider collider){
		if(collider.tag == "Player"){
			if(Input.GetKeyDown(KeyCode.E)){
				anim.SetTrigger("Open");
				fleshtubethingy.GetComponent<AudioSource>().clip = OpenMouthclip;
				fleshtubethingy.GetComponent<AudioSource>().Play();
				gameObject.transform.parent.GetComponent<AudioSource>().Play ();
				Destroy(gameObject);
			}
		}
	}
}
