﻿using UnityEngine;
using System.Collections;

public class StartNavTrigger : MonoBehaviour {

	public WaypointManager wayPointManager;

	void OnTriggerEnter(Collider collider){
		if(collider.tag == "Player"){
			wayPointManager.StartNavigating();
		}
	}
}
