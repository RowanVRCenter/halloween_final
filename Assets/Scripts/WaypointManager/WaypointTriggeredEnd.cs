﻿using UnityEngine;
using System.Collections;

public class WaypointTriggeredEnd : MonoBehaviour {

	public WaypointManager waypointManager;
	
	void OnTriggerEnter( Collider other)
	{
		if(other.tag == "FleshMonster")
			waypointManager.SignalWaypointReached(this.gameObject.transform, WaypointManager.WaypointActions.idle);
	}
}
