﻿using UnityEngine;
using System.Collections;

public class ScreamBoundary : MonoBehaviour {

	public SkinnyMonster skinnyMonster;
	protected bool hasScreamed = false;
	public WaypointManager waypointManager;

	// Use this for initialization
	void Start () {
		//waypointManager = GameObject.FindGameObjectWithTag("WaypointManager").GetComponent<WaypointManager>();
		//waypointManager = gameObject.GetComponentInParent<WaypointManager>();
	}

	void OnTriggerEnter(Collider collider)
	{
		//Debug.Log("OnTrigger" + collider.name);
		if( collider.tag == "Player")
		{

			//Debug.Log("OnTriggerEnter Scream Boundary");
			if(!hasScreamed)
			{
				waypointManager.SignalWaypointReached(collider.transform, WaypointManager.WaypointActions.scream);
				hasScreamed = true;
			}
		}
	}

	void OnTriggerExit(Collider collider)
	{

		hasScreamed = false;

	}


	// Update is called once per frame
	void Update () {
	
	}
}
