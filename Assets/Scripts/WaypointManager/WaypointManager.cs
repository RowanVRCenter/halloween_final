﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//[ExecuteInEditMode]
public class WaypointManager : MonoBehaviour 
{
	public enum WaypointActions
	{
		none,
		swipe,
		scream,
		idle,
		kill
	}

	public Transform waypointsInGroup;
	public GameObject skinnyMonster;
	protected SkinnyMonster skinnyMonsterScript;
	public float speed = 1.0f;

	protected bool isNavigating = false;
	protected List<Transform> wayPoints;
	protected int currentWaypoint = 0;

	protected Vector3 currentDirection;
	public bool isLooping = true;	//Bridges the start and end of waypoints
	public bool waitToStart = false;
	private GameObject player;
	//public GameObject lookSpot;

	public OpenGateDoor gateToOpen;

	private Death deathScript;

	//Need a trigger to tell manager to begin...

	private static int CompareTransform( Transform a, Transform b)
	{
		return a.name.CompareTo(b.name);
	}

	// Use this for initialization
	void Start ()
	{
		Debug.Log("Run start");
		wayPoints = new List<Transform>();
		skinnyMonsterScript = skinnyMonster.GetComponent<SkinnyMonster>();
		player = GameObject.FindGameObjectWithTag("Player");
		deathScript = GameObject.FindGameObjectWithTag("Timer").GetComponent<Death>();
		ExtractWaypoints();
		PlaceAtStart();
		IncWaypoint();
		StartNavigating();
		if(waitToStart){
			StopNavigating();
		}
	}

	public void IsLooping(bool value)
	{
		if(value)
		{
			isLooping = true;
			SetWaypointGroup(waypointsInGroup);
		}

		else
		{
			isLooping = false;
			SetWaypointGroup(waypointsInGroup);
		}
	}

	/*
	void OnDrawGizmos()
	{
		for( int i = 0 ; i < wayPoints.Count - 1; i++)
		{
			Gizmos.DrawLine(wayPoints[i].transform.position, wayPoints[i+1].transform.position);
		}
	}
	*/

	protected void PlaceAtStart()
	{
		skinnyMonster.transform.parent.position = wayPoints[0].transform.position;
		skinnyMonster.transform.parent.rotation = Quaternion.LookRotation(wayPoints[1].transform.position - wayPoints[0].transform.position, Vector3.up);

	}

	protected void IncWaypoint()
	{

		if(currentWaypoint < wayPoints.Count - 1)
		{
			currentWaypoint++;
		}
		else{
			currentWaypoint = 0;
		}

	}

	protected void ExtractWaypoints()
	{
		foreach(Transform child in waypointsInGroup.transform)
		{
			wayPoints.Add(child);
			child.GetComponent<MeshRenderer>().enabled = false;
		}

		wayPoints.Sort(CompareTransform);

		//If we are looping re-add the first node
//		if(isLooping)
//		{
//			wayPoints.Add(wayPoints[0]);
//		}
	}

	protected void MoveTowardCurrentWaypoint()
	{
		Vector3 direction = (wayPoints[currentWaypoint].position - skinnyMonster.transform.parent.position).normalized;
		//Orient toward target;
		if(currentWaypoint != 0){
			skinnyMonster.transform.parent.rotation = Quaternion.LookRotation(wayPoints[currentWaypoint].transform.position - wayPoints[currentWaypoint-1].transform.position, Vector3.up);
		}else if(currentWaypoint == wayPoints.Count - 1){
			if(!isLooping){
				skinnyMonster.transform.parent.rotation = Quaternion.LookRotation(Vector3.right,Vector3.up);;
			}else{
				skinnyMonster.transform.parent.rotation = Quaternion.LookRotation(wayPoints[0].transform.position - wayPoints[wayPoints.Count-1].transform.position, Vector3.up);
			}
		}else{
			skinnyMonster.transform.parent.rotation = Quaternion.LookRotation(wayPoints[0].transform.position - wayPoints[wayPoints.Count-1].transform.position, Vector3.up);
		}
		//Move toward target;
		skinnyMonster.transform.parent.position += direction * speed * Time.deltaTime;
	}

	public void SignalWaypointReached( Transform waypointTriggered, WaypointActions action)
	{

		//Have we reached the end?

		if( !isLooping && currentWaypoint == wayPoints.Count - 1)
		{
			isNavigating = false;
		}

		if(waypointTriggered == wayPoints[currentWaypoint])
		{
			IncWaypoint();
		}

		Debug.Log ("Waypoint signaled");

		switch( action)
		{
		case WaypointActions.none:
			break;
		case WaypointActions.swipe:
			StartCoroutine("SwipeAndWait");
			break;
		case WaypointActions.scream:
			isNavigating = false;
			skinnyMonster.transform.parent.LookAt(new Vector3(player.transform.position.x,skinnyMonster.transform.parent.position.y,player.transform.position.z));
			StartCoroutine("ScreamAndWait");
			break;
		case WaypointActions.idle:
			Idle();
			break;
		case WaypointActions.kill:
			StartCoroutine("Kill");
			break;
		}
	}

	/// <summary>
	/// Sets the waypoint group.
	/// </summary>
	/// <param name="transform">Transform.</param>
	public void SetWaypointGroup(Transform transform)
	{
		wayPoints.Clear();
		waypointsInGroup = transform;
		currentWaypoint = 0;
		ExtractWaypoints();
		PlaceAtStart();
		IncWaypoint();
		StartNavigating();
	}

	protected IEnumerator SwipeAndWait()
	{
		isNavigating = false;
		skinnyMonsterScript.SetState(SkinnyMonster.SkinnyMonsterState.strike);
		//Trigger door to open here...
		yield return new WaitForSeconds(0.75f);
		gateToOpen.openDoor = true;
		gateToOpen.OpenDoor();
		isNavigating = true;
	}

	protected IEnumerator ScreamAndWait()
	{
		isNavigating = false;
		skinnyMonsterScript.SetState(SkinnyMonster.SkinnyMonsterState.scream);
		//Trigger door to open here...
		yield return new WaitForSeconds(4.0f);
		isNavigating = true;
	}

	private void Idle(){
		isNavigating = false;
		skinnyMonsterScript.SetState(SkinnyMonster.SkinnyMonsterState.idle);
	}

	protected IEnumerator Kill(){
		isNavigating = false;
		player.GetComponent<Movement>().forwardSpeed = 0f;
		player.GetComponent<Movement>().rotationSpeed = 0f;
		player.transform.LookAt(new Vector3(skinnyMonster.transform.position.x,player.transform.position.y,skinnyMonster.transform.position.z),Vector3.up);
		yield return new WaitForSeconds(0.5f);
		skinnyMonsterScript.SetState(SkinnyMonster.SkinnyMonsterState.strike);
		yield return new WaitForSeconds(1f);
		skinnyMonsterScript.SetState(SkinnyMonster.SkinnyMonsterState.scream);
		yield return new WaitForSeconds(2.0f);
		deathScript.KillPlayer(Application.loadedLevelName);
	}

	public void StartNavigating()
	{
		isNavigating = true;
		skinnyMonsterScript.SetState(SkinnyMonster.SkinnyMonsterState.lumber);
	}

	public void StopNavigating(){
		isNavigating = false;
		skinnyMonsterScript.SetState(SkinnyMonster.SkinnyMonsterState.idle);
	}

	// Update is called once per frame
	void Update () {
	
		if(isNavigating)
		{
			MoveTowardCurrentWaypoint();
		}
	}
}
