﻿using UnityEngine;
using System.Collections;

public class WaypointTriggered : MonoBehaviour {


	public WaypointManager waypointManager;

	// Use this for initialization
	void Start () {
		//waypointManager = GameObject.FindGameObjectWithTag("WaypointManager").GetComponent<WaypointManager>();
	}

	// Update is called once per frame
	void Update () {
	}

	void OnTriggerEnter( Collider other)
	{
		if(other.tag == "FleshMonster")
		{
			waypointManager.SignalWaypointReached(this.gameObject.transform, WaypointManager.WaypointActions.none);
		}
	}

}
