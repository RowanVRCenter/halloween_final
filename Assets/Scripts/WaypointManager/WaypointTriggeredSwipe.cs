﻿using UnityEngine;
using System.Collections;

public class WaypointTriggeredSwipe : MonoBehaviour {

	public WaypointManager waypointManager;
	public OpenGateDoor gateToOpen;

	void OnTriggerEnter( Collider other)
	{
		if(other.tag == "FleshMonster"){
			waypointManager.gateToOpen = gateToOpen;
			waypointManager.SignalWaypointReached(this.gameObject.transform, WaypointManager.WaypointActions.swipe);
		}
	}

}
