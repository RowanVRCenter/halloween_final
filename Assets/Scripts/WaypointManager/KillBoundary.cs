﻿using UnityEngine;
using System.Collections;

public class KillBoundary : MonoBehaviour {

	public SkinnyMonster skinnyMonster;
	public WaypointManager waypointManager;
	
	void OnTriggerEnter(Collider collider)
	{
		Debug.Log("OnTrigger" + collider.name);
		if( collider.tag == "Player")
		{
			waypointManager.SignalWaypointReached(collider.transform, WaypointManager.WaypointActions.kill);
		}
	}

}
