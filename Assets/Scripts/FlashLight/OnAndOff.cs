﻿using UnityEngine;
using System.Collections;

public class OnAndOff : MonoBehaviour {

	public GameObject yellowFlashLight;
	private Light yellowLight;
	private bool flashlightOn = true;
	private float onTimer;
	private float onTimerReset = 10f;
	private float offTimer;
	private float offTimerReset = 15f;

	// Use this for initialization
	void Start () {
		onTimer = onTimerReset;
		offTimer = offTimerReset;
		yellowLight = yellowFlashLight.GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
		if(flashlightOn){
			onTimer -= Time.deltaTime;
		}else{
			offTimer -= Time.deltaTime;
		}

		if(onTimer <= 0f){
			flashlightOn = false;
			onTimer = onTimerReset;
		}

		if(offTimer <= 0f){
			flashlightOn = true;
			offTimer = offTimerReset;
		}

		if(flashlightOn){
			yellowLight.enabled = true;
		}else{
			yellowLight.enabled = false;
		}
	
	}
}
