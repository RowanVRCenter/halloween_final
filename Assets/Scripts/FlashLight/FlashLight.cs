using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class FlashLight : MonoBehaviour {

	private bool blueState = true;
	private bool yellowState = true;
	private bool blueTemp;
	private bool yellowTemp;
	public GameObject blueFlashlight;
	public GameObject yellowFlashlight;
	private bool blueButton = false;
	private bool yellowButton = false;
	public float blueBatteryFull = 30;
	public float yellowBatteryFull = 5;
	public float blueBatteryChargeTime = 5;
	public float yellowBatteryChargeTime = 10;
	private float blueBattery;
	private float yellowBattery;
	private bool blueRunout = false;
	private bool yellowRunout = false;
	private bool devMode = false;
	private Movement movement;
	private int numberOfPlayers;
	private GamePadState blueGameState, yellowGameState;
	private bool blueBlinking = false,yellowBlinking = false;


	// Use this for initialization
	void Awake(){
		if (GameObject.FindObjectOfType<PlayTimer>() != null){
			numberOfPlayers = GameObject.FindObjectOfType<PlayTimer>().numOfPlayers;
		}
		else 
			numberOfPlayers = 4;
	}

//	void OnGUI(){
//		GUI.Label(new Rect(Screen.width*0.6f,30,300,20),numberOfPlayers.ToString());
//	}
	
	void Start () {
		yellowGameState = GamePad.GetState(PlayerIndex.Two);
		yellowButton = (yellowGameState.Buttons.B == ButtonState.Pressed);
		if (numberOfPlayers == 3){
			blueButton = !yellowButton;
			blueFlashlight.transform.parent = yellowFlashlight.transform;
			blueFlashlight.GetComponent<BigBlueTracker>().enabled=false;
			blueFlashlight.transform.localPosition = Vector3.zero;
			blueFlashlight.transform.localRotation = Quaternion.identity;
		}
		else {
			blueGameState = GamePad.GetState(PlayerIndex.Three);
			blueButton = (blueGameState.Buttons.B == ButtonState.Pressed);
		}
		yellowGameState = GamePad.GetState(PlayerIndex.Two);
		yellowButton = (yellowGameState.Buttons.B == ButtonState.Pressed);
		blueBattery = blueBatteryFull;
		yellowBattery = yellowBatteryFull;
		movement = GetComponent<Movement>();
	}
	
	// Update is called once per frame
	void Update () {
		if (!devMode){
			yellowGameState = GamePad.GetState(PlayerIndex.Two);
			if (numberOfPlayers == 4){
				blueGameState = GamePad.GetState(PlayerIndex.Three);
				blueButton = (blueGameState.Buttons.B == ButtonState.Pressed);
				yellowButton = (yellowGameState.Buttons.B == ButtonState.Pressed);
				if (!blueRunout){
					if (blueButton){
						if (blueBattery <= 0){
							blueTemp = false;
							blueRunout = true;
						}
						else{
							blueTemp = true;
							blueBattery -= Time.deltaTime;
						}
					}
					else if (blueBattery <= 0){
						blueTemp = false;
						blueRunout = true;
					}
					else if (blueBattery < blueBatteryFull){
						blueTemp = false;
						blueBattery += Time.deltaTime * blueBatteryFull/blueBatteryChargeTime;
					}
					else{
						blueTemp = false;
					}
				}
				else{
					if (blueButton){
						blueTemp = false;
					}
					else if (blueBattery<blueBatteryFull){
						blueTemp = false;
							blueBattery += Time.deltaTime * blueBatteryFull/blueBatteryChargeTime;
					}
					else {
						blueTemp = false;
						blueRunout = false;
						StartCoroutine(BlinkBlueLight());
					}
				}
				if (!yellowRunout){
					if (yellowButton){
						if (yellowBattery <= 0){
							yellowTemp = false;
							yellowRunout = true;
						}
						else{
							yellowTemp = true;
							yellowBattery -= Time.deltaTime;
						}
					}
					else if (yellowBattery <= 0){
						yellowTemp = false;
						yellowRunout = true;
					}
					else if (yellowBattery < yellowBatteryFull){
						yellowTemp = false;
							yellowBattery += Time.deltaTime * yellowBatteryFull/yellowBatteryChargeTime;
					}
					else{
						yellowTemp = false;
					}
				}
				else{
					if (yellowButton){
						yellowTemp = false;
					}
					else if (yellowBattery<yellowBatteryFull){
						yellowTemp = false;
						yellowBattery += Time.deltaTime * yellowBatteryFull/yellowBatteryChargeTime;
					}
					else {
						yellowTemp = false;
						yellowRunout = false;
						StartCoroutine(BlinkYellowLight());
					}
				}
				if (blueTemp != blueState){
					blueState = blueTemp;
					//getReal3D.RpcManager.call("toggleBlue",blueState);
				}
				if (yellowTemp != yellowState){
					yellowState = yellowTemp;
					//getReal3D.RpcManager.call("toggleYellow",yellowState);
				}
				if (!blueBlinking){
					if (blueButton){
						GamePad.SetVibration(PlayerIndex.Three, 0f, 0.5f);
					}
					else{
						GamePad.SetVibration(PlayerIndex.Three, 0f, 0f);
					}
				}
				if (!yellowBlinking){
					if (yellowButton){
						GamePad.SetVibration(PlayerIndex.Two, 0f, 1f);
					}
					else{
						GamePad.SetVibration(PlayerIndex.Two, 0f, 0f);
					}
				}
			}
			else{
				yellowButton = (yellowGameState.Buttons.B == ButtonState.Pressed);
				blueButton = !yellowButton;
				if (!blueRunout){
					if (blueButton){
						if (blueBattery <= 0){
							blueTemp = false;
							blueRunout = true;
						}
						else{
							blueTemp = true;
							blueBattery -= Time.deltaTime;
						}
					}
					else if (blueBattery <= 0){
						blueTemp = false;
						blueRunout = true;
					}
					else if (blueBattery < blueBatteryFull){
						blueTemp = false;
						blueBattery += Time.deltaTime * blueBatteryFull/blueBatteryChargeTime;
					}
					else{
						blueTemp = false;
					}
				}
				else{
					if (blueButton){
						blueTemp = false;
					}
					else if (blueBattery<blueBatteryFull){
						blueTemp = false;
						blueBattery += Time.deltaTime * blueBatteryFull/blueBatteryChargeTime;
					}
					else {
						blueTemp = false;
						blueRunout = false;
						StartCoroutine(BlinkYellowLight());
					}
				}
				if (!yellowRunout){
					if (yellowButton){
						if (yellowBattery <= 0){
							yellowTemp = false;
							yellowRunout = true;
						}
						else{
							yellowTemp = true;
							yellowBattery -= Time.deltaTime;
						}
					}
					else if (yellowBattery <= 0){
						yellowTemp = false;
						yellowRunout = true;
					}
					else if (yellowBattery < yellowBatteryFull){
						yellowTemp = false;
						yellowBattery += Time.deltaTime * yellowBatteryFull/yellowBatteryChargeTime;
					}
					else{
						yellowTemp = false;
					}
				}
				else{
					if (yellowButton){
						yellowTemp = false;
					}
					else if (yellowBattery<yellowBatteryFull){
						yellowTemp = false;
						yellowBattery += Time.deltaTime * yellowBatteryFull/yellowBatteryChargeTime;
					}
					else {
						yellowTemp = false;
						yellowRunout = false;
						StartCoroutine(BlinkYellowLight());
					}
				}
				if (blueTemp != blueState){
					blueState = blueTemp;
					//getReal3D.RpcManager.call("toggleBlue",blueState);
				}
				if (yellowTemp != yellowState){
					yellowState = yellowTemp;
					//getReal3D.RpcManager.call("toggleYellow",yellowState);
				}
				if (!yellowBlinking){
					if (yellowButton){
						GamePad.SetVibration(PlayerIndex.Two, 0f, 1f);
					}
					else{
						GamePad.SetVibration(PlayerIndex.Two, 0f, 0f);
					}
				}
			}
		}
	}

	void devModeOn(){
		toggleBlue(true);
		toggleYellow(true);
		devMode = true;
		movement.forwardSpeed = 10f;
	}
	void FlashlightsOn(){
		toggleBlue(true);
		toggleYellow(true);
		devMode = true;
		//movement.forwardSpeed = 10f;
	}
	void toggleBlue(bool state){
			blueFlashlight.SetActive(state);
	}
	void toggleYellow(bool state){
			yellowFlashlight.SetActive(state);
	}
	IEnumerator BlinkBlueLight(){
		blueBlinking = true;
		for (int i=0;i<4;i++){
			GamePad.SetVibration(PlayerIndex.Three, 0f, 0.5f);
			yield return new WaitForSeconds(0.25f);
			GamePad.SetVibration(PlayerIndex.Three, 0f, 0f);
			yield return new WaitForSeconds(0.25f);
		}
		blueBlinking = false;
	}

	IEnumerator BlinkYellowLight(){
		yellowBlinking = true;
		for (int i=0;i<4;i++){
			GamePad.SetVibration(PlayerIndex.Two, 0f, 1f);
			yield return new WaitForSeconds(0.25f);
			GamePad.SetVibration(PlayerIndex.Two, 0f, 0f);

			yield return new WaitForSeconds(0.25f);
		}
		yellowBlinking = false;
	}
}