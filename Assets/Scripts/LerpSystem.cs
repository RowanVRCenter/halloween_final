﻿using UnityEngine;
using System.Collections;

public class LerpSystem : MonoBehaviour {

	private Transform[] transforms;
	private Transform top;
	private Transform bot;
	private Transform lerper;
	private Vector3 topPos;
	private Vector3 botPos;

	public float frequency = 1;
	public float delay = 1;
	private bool lerpingUp;
	private float counter;

	// Use this for initialization
	void Start () {
		transforms = GetComponentsInChildren<Transform> ();
		top = transforms [1];
		bot = transforms [2];
		lerper = transforms [3];
		topPos = top.position;
		botPos = bot.position;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (counter < delay) {
			counter += Time.deltaTime;
		} else 
		{
			if (!lerpingUp) {
				lerpingUp = true;
				StartCoroutine (LerpUp ());
			}
		}
	}

	IEnumerator LerpUp()
	{
		topPos = top.position;
		botPos = bot.position;
		float i=0;
		while (i < frequency) {
			lerper.position = Vector3.Lerp (botPos, topPos, i / frequency);
			i += Time.deltaTime;
			yield return null;
		}
		lerpingUp = false;
	}
}
