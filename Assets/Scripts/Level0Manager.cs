using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class Level0Manager : MonoBehaviour {

	public enum Signals
	{
		Phase1PressX,
		Phase2PressForward,
		Phase3Flashlights,
		Phase4Controller,
		Phase5Radio,
		Phase6LevelSwap
	}

	private PlayTimer timer;

	public GameObject playerBody;
	public GameObject yellowFlashlight,blueFlashlight;

	public float timeBetweenStations = 8f;

	public GameObject signTheRite;
	public GameObject signTheRitePressX;
	public GameObject locatorPhase1ToPhase2;
	public float playerTransitionSpeed = 2.0f;

	public GameObject signBlueFlashlight;
	public GameObject signBlueFlashlightText;
	public GameObject signYellowFlashlight;
	public GameObject signYellowFlashlightText;
	public GameObject signFlashlightOffText;
	public GameObject signTurnFlashlightsOnToContinue;
	public GameObject signMoveForwardFlashlight;

	public GameObject signController;
	//public GameObject signControllerText;
	public GameObject signMoveForwardController;

	public GameObject signRadio;

	public int numberOfPlayers=4;

	public Texture2D activeButtonBackground;
	public Texture2D inactiveButtonBackground;

	public Collider colliderTitle;
	public Collider colliderFlashlight;
	public Collider colliderXbox;
	public Collider colliderRadio;
	
	public AudioClip Prologue,IntroLevel;

	protected Rect guiRect;
	protected GUIStyle styleOn;
	protected GUIStyle styleOff;
	private int PlayCounter = 0;

	protected bool playerLockedTransition = false;

	protected bool waitingForPlayerInput = true;
	protected Movement playerMovementScript = null;
	private bool player2Press = false,player3Press = false;
	private bool playpress=false;


	//Vive Stuff
	public ViveMovement vivePlayerMovementScript;
	public GameObject VivePhase3Plane;
	public float genericDleay = 15f;
	public GameObject textGrabThings;
	public GameObject textLookAtBelt;
	public GameObject textDropThings;
    public GameObject textAttachToGuantlet;
	public GameObject textThrow;
    public GameObject textShake;
    public Vector3 player2LocatorVector;
    public GameObject moveForwardAgain;
    public GameObject phase3Trigger;
    public bool running = false;


    void Start () {
		//Vive Stuff
		vivePlayerMovementScript.disableMovement();

		timer = GameObject.FindObjectOfType<PlayTimer>();
		//playerMovementScript = playerBody.transform.GetComponentInChildren<Movement>();
		//playerMovementScript.disableMovement();
		Phase1TheRite();
		guiRect = new Rect(10f, Screen.height/ 3.0f, Screen.width /4.0f, Screen.height / 3.0f);
		styleOn = new GUIStyle();
		styleOff = new GUIStyle();
		styleOn.normal.background = activeButtonBackground;
		styleOn.normal.textColor = Color.white;
		styleOff.normal.background = inactiveButtonBackground;
		styleOff.normal.textColor = new Color(0.75f, 0.75f, 0.75f);
		GetComponent<AudioSource>().volume = 0.2f;
		GetComponent<AudioSource>().clip = IntroLevel;
		GetComponent<AudioSource>().loop = true;
		GetComponent<AudioSource>().Play ();
		yellowFlashlight.SetActive(true);
		blueFlashlight.SetActive(true);
	}

	protected void Phase1TheRite()
	{
		//Vive Stuff
		Debug.Log ("Phase 1 Start ~Saad");


		signTheRite.SetActive(true);
		signTheRitePressX.SetActive(false);
		
		signBlueFlashlight.SetActive(false);
		signBlueFlashlightText.SetActive(false);
		signYellowFlashlight.SetActive(false);
		signYellowFlashlightText.SetActive(false);
		signTurnFlashlightsOnToContinue.SetActive(false);
		signMoveForwardFlashlight.SetActive(false);
		signFlashlightOffText.SetActive(false);
		
		signController.SetActive(false);
		//signControllerText.SetActive(false);
		signMoveForwardController.SetActive(false);
		
		signRadio.SetActive(false);
		
		colliderTitle.enabled = true;
		colliderFlashlight.enabled = false;
		colliderXbox.enabled = false;
		colliderRadio.enabled = false;
	}

	void OnGUI()
	{
		if (!playpress){
			GUILayout.BeginArea(guiRect);
			GUILayout.BeginVertical();
				if(numberOfPlayers == 4)
				{
					if(GUILayout.Button("3 Players", styleOff))
					{
						numberOfPlayers = 3;
					}
					
					if(GUILayout.Button("4 Players", styleOn))
					{
						numberOfPlayers = 4;
					}
				}
				
				else
				{
					if(GUILayout.Button("3 Players", styleOn))
					{
						numberOfPlayers = 3;
					}
					
					if(GUILayout.Button("4 Players", styleOff))
					{
						numberOfPlayers = 4;
					}
				}
				
				if(GUILayout.Button("-Play-"))
				{
					//Vive Stuff
					Phase2PressX();
					

					//StartCoroutine(PlayButtonPress());
										
					playpress = true;
					timer.GameTimer = 30f * 60f;
				}
			GUILayout.EndVertical();
			GUILayout.EndArea();
		}
	}

	/*IEnumerator PlayButtonPress(){
		PrologueStart(numberOfPlayers);
		while(GetComponent<AudioSource>().volume>0){
			GetComponent<AudioSource>().volume -= 0.01f;
			yield return new WaitForSeconds(0.1f);
		}
		GetComponent<AudioSource>().Stop();
		GetComponent<AudioSource>().loop = false;
		GetComponent<AudioSource>().clip = Prologue;
		GetComponent<AudioSource>().volume = 1f;
		PlayCounter ++;
		GetComponent<AudioSource>().Play();
		yield return new WaitForSeconds(GetComponent<AudioSource>().clip.length-20f);
		PlayCounter --;
		if (PlayCounter ==0) PrologueEnd();
		yield return new WaitForSeconds(5f);
		if (PlayCounter ==0){
			GetComponent<AudioSource>().volume = 0f;
			GetComponent<AudioSource>().clip = IntroLevel;
			GetComponent<AudioSource>().Play();
			Phase2PressX();
			Debug.Log ("Going into phase 2");
			while(GetComponent<AudioSource>().volume<0.75f){
				GetComponent<AudioSource>().volume += 0.05f;
				yield return new WaitForSeconds(0.1f);
			}
		}
	}*/

	protected void PrologueStart(int players){
		signTheRite.SetActive(false);
		timer.numOfPlayers = players;
	}
	
	protected void PrologueEnd(){
		signTheRite.SetActive(true);
	}

	protected void Phase2PressX()
	{
		//Vive Stuff
		Debug.Log ("Phase 2 Start ~Saad");
		vivePlayerMovementScript.disableMovement();
		signTheRite.SetActive(true);


		signTheRitePressX.SetActive(true);
		colliderTitle.enabled = false;
		
		//playerMovementScript.disableMovement();
		
		waitingForPlayerInput = true;
	}

	void Update () {
        //Vive Stuff
        if(phase3Trigger.GetComponent<Phase3Tutorial>().active && !running)
        {
            StartCoroutine(VivePhase3());
        }


        player2Press = (GamePad.GetState(PlayerIndex.Two).Buttons.B == ButtonState.Pressed);
		if (player2Press)GamePad.SetVibration(PlayerIndex.Two,0f,1f);
		else GamePad.SetVibration(PlayerIndex.Two,0f,0f);
		if (numberOfPlayers == 4){
			player3Press = (GamePad.GetState(PlayerIndex.Three).Buttons.B == ButtonState.Pressed);
			if (player3Press)GamePad.SetVibration(PlayerIndex.Three,0f,1f);
			else GamePad.SetVibration(PlayerIndex.Three,0f,0f);
		}
		else player3Press = true;
		if(waitingForPlayerInput)
		{
			//if(vivePlayerMovementScript.triggerPressed)
			{
				waitingForPlayerInput = false;
				StartCoroutine("MovePlayerFromPhase1ToPhase2");
			}
		}
	}

	protected IEnumerator MovePlayerFromPhase1ToPhase2()
	{
		//Vive Stuff
		Debug.Log ("Moving from phase 1 to phase 2 ~Saad");



		player2LocatorVector = locatorPhase1ToPhase2.transform.position - playerBody.transform.position;

		while(player2LocatorVector.magnitude > 0.8f)
		{
			Vector3 displacement = player2LocatorVector.normalized * playerTransitionSpeed;

			//Vive Stuff
			vivePlayerMovementScript.body.SimpleMove(displacement);

			//playerMovementScript.cc.SimpleMove(displacement);
			yield return null;
			player2LocatorVector = locatorPhase1ToPhase2.transform.position - playerBody.transform.position;
		}


		colliderTitle.enabled = true;

		/*if(playerMovementScript != null){
			playerMovementScript.enableMovement();
			colliderTitle.enabled = true;
		}*/
	}

	public void getSignal(Signals signal)
	{
		
		switch(signal)
		{
		case Signals.Phase1PressX:
			Debug.Log("Phase 1");
			signController.SetActive(true);
			//signControllerText.SetActive(true);		
			colliderFlashlight.enabled = false;
			colliderXbox.enabled = true;
			StartCoroutine("ControllerWait20Seconds");			
			break;
		
		
		/*case Signals.Phase3Flashlights:
			Debug.Log("Phase 3");
			Phase3Flashlights();
			break;
		case Signals.Phase5Radio:
			Debug.Log("Phase 5");
			Phase5Radio();
			//LevelTransition();
			break;*/
		}
	}

	protected IEnumerator ControllerWait20Seconds()
	{
		yield return new WaitForSeconds(15f);
		signMoveForwardController.SetActive(true);
		colliderXbox.enabled = false;

		//Vive Stuff
		Debug.Log ("Movement should be enabled ~Saad");
		vivePlayerMovementScript.enableMovement();
		yield return new WaitForSeconds (10f);
	}

	protected IEnumerator VivePhase3() {
        running = true;
		textGrabThings.SetActive (true);
		yield return new WaitForSeconds (genericDleay);
		textGrabThings.SetActive (false);
		Debug.Log("Disable 1st, Enable 2nd ~Saad");

		textLookAtBelt.SetActive (true);
		yield return new WaitForSeconds (genericDleay);
		textLookAtBelt.SetActive (false);
		Debug.Log("Disable 2nd, Enable 3rd ~Saad");

		textDropThings.SetActive (true);
		yield return new WaitForSeconds (genericDleay);
		textDropThings.SetActive (false);
		Debug.Log("Disable 3rd, Enable 4th ~Saad");

        textAttachToGuantlet.SetActive(true);
        yield return new WaitForSeconds(genericDleay);
        textAttachToGuantlet.SetActive(false);
        Debug.Log("Disable 3rd, Enable 4th ~Saad");

        textShake.SetActive(true);
        yield return new WaitForSeconds(genericDleay);
        textShake.SetActive(false);

        textThrow.SetActive (true);
		yield return new WaitForSeconds (genericDleay);
        textThrow.SetActive(false);

        moveForwardAgain.SetActive(true);
        moveForwardAgain.GetComponent<MeshCollider> ().enabled = false;

	}

	protected void Phase3Flashlights()
	{
		
		signBlueFlashlight.SetActive(true);
		signBlueFlashlightText.SetActive(true);
		signYellowFlashlight.SetActive(true);
		signYellowFlashlightText.SetActive(true);
		signFlashlightOffText.SetActive(true);
		colliderFlashlight.enabled = true;
		
		StartCoroutine("FlashlightWait20Seconds");
	}

	protected IEnumerator FlashlightWait20Seconds()
	{
		yield return new WaitForSeconds(20.0f);
		colliderXbox.enabled = true;
		yellowFlashlight.SetActive(false);
		blueFlashlight.SetActive(false);
		signFlashlightOffText.SetActive(false);
		signTurnFlashlightsOnToContinue.SetActive(true);
		if(numberOfPlayers == 3){
			while(!yellowFlashlight.activeSelf)
			{
					toggleYellow(player2Press);
				yield return null;
			}
		}
		else{
			while(!(yellowFlashlight.activeSelf && blueFlashlight.activeSelf))
			{
					toggleYellow(player2Press);
					toggleBlue(player3Press);
				yield return null;
			}
		}

		signMoveForwardFlashlight.SetActive(true);
		colliderFlashlight.enabled = false;
	}

	void toggleBlue(bool state){
		blueFlashlight.SetActive(state);
	}

	void toggleYellow(bool state){
		yellowFlashlight.SetActive(state);
	}

	protected void Phase5Radio()
	{
		signController.SetActive(false);
		//signControllerText.SetActive(false);
		signMoveForwardController.SetActive(false);
		signRadio.SetActive(true);
		
		colliderXbox.enabled = false;
		colliderRadio.enabled = true;
		StartCoroutine("RadioWait20Seconds");
		
	}

	protected IEnumerator RadioWait20Seconds()
	{
		yield return new WaitForSeconds(20f);
		LevelTransition();
	}

	protected void LevelTransition()
	{
		Application.LoadLevel(Application.loadedLevel + 1);
	}

   
}
