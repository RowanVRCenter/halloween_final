﻿using UnityEngine;
using System.Collections;

public class GateDoorTrigger : MonoBehaviour {

	[HideInInspector]
	public bool runOnce = false;
	private bool playerInRange = false;
	private OpenGateDoor gateScript;

	void Start(){
		gateScript = GetComponentInChildren<OpenGateDoor>();
	}

	void OnTriggerEnter(Collider collider){
		if(collider.gameObject.tag == "Player" || collider.gameObject.tag == "FleshMonster"){
			playerInRange = true;
		}
	}

	void OnTriggerExit(Collider collider){
		if(collider.gameObject.tag == "Player" || collider.gameObject.tag == "FleshMonster"){
			playerInRange = false;
		}
	}

	void Update(){
		if(playerInRange ){
			if(!runOnce){
				runOnce = true;
				gateScript.OpenDoor();
			}
		}
	}

}
