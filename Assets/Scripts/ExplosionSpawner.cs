﻿using UnityEngine;
using System.Collections;

public class ExplosionSpawner : MonoBehaviour {

    public GameObject explosion;

    private Transform[] locations;

    public bool test;

	// Use this for initialization
	void Start () {
        locations = this.GetComponentsInChildren<Transform>();
        if (test)
        {
            SpawnExplosions();
        }
	}

    void SpawnExplosions()
    {
        StartCoroutine(ExplosionStart());   
    }

    IEnumerator ExplosionStart()
    {
        Debug.Log("Explosions Starting");
        for (int i = 0; i < locations.Length; i++)
        {
            Instantiate(explosion, locations[i].position, Quaternion.identity);
            yield return new WaitForSeconds(Random.Range(.75f, 1.1f));
        }
    }
}
