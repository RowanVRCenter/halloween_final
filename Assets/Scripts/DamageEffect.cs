﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DamageEffect : MonoBehaviour {

    public Image im;
    public Sprite hitOnce;
    public Sprite hitTwice;
    public Sprite hitThrice;
    public float aTime;
    public bool test;

    public float health = 3;

    void Update()
    {
        if (test)
        {
            Hit(1f);
        }
        test = false;
    }

    public void Hit(float damage)
    {
        Debug.Log("Hit with damage of: " + damage);
        health -= damage;
        if (health >= 2){
            im.sprite = hitOnce;
        }else if (health >= 1 && health < 2){
            im.sprite = hitTwice;
        }else if (health >= 0 && health < 1){
            im.sprite = hitThrice;
        }
        Color newColor = new Color(im.color.r,im.color.g,im.color.b,1f);
        im.color = newColor;
        if (health <= 0)
        {
            SceneManager.LoadScene("Level_Death");
        }
        StopAllCoroutines();
        StartCoroutine(FadeOut());
    }

    IEnumerator FadeOut()
    {
        float r = im.color.r;
        float g = im.color.g;
        float b = im.color.b;
        float i = 0;
        
        while (i < aTime)
        {
            Color newColor = new Color(r, g, b, Mathf.Lerp(1, 0, i / aTime));
            im.color = newColor;
            i += Time.deltaTime;
            yield return null;
        }
    }

    IEnumerator FadeIn()
    {
        float r = im.color.r;
        float g = im.color.g;
        float b = im.color.b;
        float i = 0;

        while (i < aTime)
        {
            Color newColor = new Color(r, g, b, Mathf.Lerp(0, 1, i / aTime));
            im.color = newColor;
            i += Time.deltaTime;
            yield return null;
        }
    }
}
