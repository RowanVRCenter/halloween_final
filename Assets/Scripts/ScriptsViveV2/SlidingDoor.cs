﻿using UnityEngine;
using System.Collections;

public class SlidingDoor : MonoBehaviour {

	public bool doorClosed = true;
	public bool testBool = false;
	public AudioClip[] doorOpenClip, doorCloseClip;

	private float delta = 3.2f;
	private AudioSource sound;
	private Vector3 startPos;
	private Vector3 endPos;
	private float duration = 1.8f;

	// Use this for initialization
	void Start () {
		startPos = transform.localPosition;
		sound = GetComponent<AudioSource> ();
		endPos = new Vector3 (transform.localPosition.x + delta, transform.localPosition.y, transform.localPosition.z);
	}

	IEnumerator OpenDoor() {
		sound.clip = doorOpenClip [Random.Range(1, doorOpenClip.Length)];
		sound.Play ();
		float i = 0;
		while(i < duration) {
			Vector3 temp = Vector3.Lerp (startPos, endPos, i / duration);
			i += Time.deltaTime;
			transform.localPosition = temp;
			yield return null;
		}
		GetComponent<Collider> ().enabled = false;
		doorClosed = false;
	}

	IEnumerator CloseDoor() {
		sound.clip = doorOpenClip [Random.Range(1, doorCloseClip.Length)];
		sound.Play ();
		float i = 0;
		while(i < duration) {
			Vector3 temp = Vector3.Lerp (endPos,startPos, i / duration);
			i += Time.deltaTime;
			transform.localPosition = temp;
			yield return null;
		}
		GetComponent<Collider> ().enabled = true;
		doorClosed = true;
	}

	public void OnTriggerEnter(Collider c) {
		if ((c.tag == "Player" || c.tag == "Monster") && doorClosed) {
			Debug.Log ("Opening door...");
			StartCoroutine (OpenDoor ());
		}
	}

	void Update() {
		if (testBool) {
			testBool = false;
			StartCoroutine (OpenDoor ());
		}
	}
}
