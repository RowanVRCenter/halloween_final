﻿using UnityEngine;
using System.Collections;

public class AmuletQuest : MonoBehaviour {

	private float scalingFactor = 2f;
	private float startingDist;

	public AudioSource heartbeat;
	public ParticleSystem effect;
	public Transform tenticleWall;
	public Light amuletLight;
	public bool withPlayer = false;
	public float currentDist;
	public float minDist;

	public float emissionRate;
	public float lightRange;
	public float testFloat;

	public float heartbeatScaler;
	private float soundDelay;
	private float counter;

	void Start() {
		startingDist = Vector3.Distance (this.transform.position, tenticleWall.position);
		amuletLight.range = 0;
		currentDist = Vector3.Distance (this.transform.position, tenticleWall.position);
	}

	void Update() {
		counter += Time.deltaTime;
		if (this.GetComponent<ConfigurableJoint> () != null) {
			withPlayer = true;
		}

		soundDelay = currentDist / heartbeatScaler;
		if (counter > soundDelay) {
			heartbeat.Play ();
			counter = 0;
		}

		if (withPlayer) {
			currentDist = Vector3.Distance (this.transform.position, tenticleWall.position);

			if (currentDist > minDist) {
				emissionRate = scalingFactor * (startingDist-currentDist);
				lightRange = scalingFactor * (startingDist / (currentDist));
					
				ParticleSystem.EmissionModule em = new ParticleSystem.EmissionModule();
				em = effect.emission;
				ParticleSystem.MinMaxCurve rt = new ParticleSystem.MinMaxCurve();
				rt = em.rate;
				rt.constantMax = emissionRate;
				em.rate = rt;

				amuletLight.range = lightRange;
			}

		}
	}
	
}
