﻿using UnityEngine;
using System.Collections;

public class InteractableObject : MonoBehaviour {

    public bool grabbable = true;
    public bool inventoryCapable = false;
	public bool inBeltRange;

	// Use this for initialization
	void Start () {
	       
	}

	void OnTriggerExit(Collider col)
	{
		//Debug.Log ("Trying to leave belt trigger ~Saad");
		if (col.tag == "Belt")
		{
			inBeltRange = false;
		}
	}

	void OnTriggerEnter(Collider col)
	{
		//Debug.Log ("Collider set ~Saad");
		if (col.tag == "Belt")
		{
			inBeltRange = true;
		}
	}
}
