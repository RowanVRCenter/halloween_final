﻿using UnityEngine;
using System.Collections;

public class GlowstickShocker : MonoBehaviour {

    public GameObject lightningGlowstick;
    public GameObject sparkingToGlowstick;
    public GameObject defaultTrackedObject;
    public GameObject lightningBallEffect;

    public float chargeTimer = 4f;
    public bool chargeReady = true;
    public bool testBool = false;
    public float scaleTime;
    public bool completelyDisabled = false;

    private float origTimer;
    private bool isRunningCharged = false;
    private bool isRunningSummon = false;
    private LightningBoltScript sparkingLightningStrike;
    private Vector3 origScale;
    private float deltaScale = 50f;
    private Transform c;


    // Use this for initialization
    void Start() {
        origTimer = chargeTimer;
        sparkingLightningStrike = sparkingToGlowstick.GetComponent<LightningBoltScript>();
    }

    void Update()
    {
        chargeTimer -= Time.deltaTime;
        if(chargeTimer <= 0)
        {
            chargeTimer = origTimer;
            chargeReady = true;
        }
    }

    void OnTriggerStay(Collider col)
    {
        c = col.transform;
        if(col.tag == "Glowstick" && !isRunningCharged && !isRunningSummon && chargeReady && !completelyDisabled)
        {
            Debug.Log("On Trigger was called");
            StartCoroutine(chargeGlowstick(c));
        }
    }

    IEnumerator chargeGlowstick(Transform c)
    {
        Debug.Log("charge is rdy");

        isRunningCharged = true;

        sparkingLightningStrike.EndObject = c.gameObject;
        yield return new WaitForSeconds(.5f);
        sparkingLightningStrike.EndObject = defaultTrackedObject;

        StartCoroutine(SummonLightningGlowstick(c));

        chargeReady = false;
        chargeTimer = origTimer;

        isRunningCharged = false;
    }

    IEnumerator SummonLightningGlowstick(Transform c)
    {
        Debug.Log("summoning blue stick");

        Vector3 temp = c.position;
        yield return null;
        Destroy(c.gameObject);
        yield return null;
        Instantiate(lightningGlowstick, temp, Quaternion.identity);
        yield return null;

        isRunningSummon = true;

        GameObject currentLightningBallEffect = Instantiate(lightningBallEffect, temp, Quaternion.identity) as GameObject;
        origScale = currentLightningBallEffect.GetComponent<Transform>().localScale;

        for (float i = 0; i < scaleTime; i += Time.deltaTime)
        {
            currentLightningBallEffect.GetComponent<Transform>().localScale = Vector3.Lerp(origScale, origScale * deltaScale, i / scaleTime);
            yield return null;
        }

        

        for (float i = 0; i < scaleTime; i += Time.deltaTime)
        {
            currentLightningBallEffect.GetComponent<Transform>().localScale = Vector3.Lerp(origScale * deltaScale, origScale, i / scaleTime);
            yield return null;
        }

        Destroy(currentLightningBallEffect.gameObject);
        isRunningSummon = false;
    }

    /*IEnumerator ScaleBall(Vector3 glowStickpos, Vector3 glowStickVel)
    {
        GameObject currentLightningBallEffect = Instantiate(lightningBallEffect, glowStickPos, Quaternion.identity) as GameObject;
        origScale = currentLightningBallEffect.GetComponent<Transform>().localScale;

        for (float i = 0; i < scaleTime; i += Time.deltaTime)
        {
            currentLightningBallEffect.GetComponent<Transform>().localScale = Vector3.Lerp(origScale, origScale * deltaScale, i / scaleTime);
            yield return null;
        }

        GameObject currentLightningGlowstick = Instantiate(lightningGlowstick, glowStickpos, Quaternion.identity) as GameObject;
        currentLightningGlowstick.GetComponent<Rigidbody>().velocity = glowStickVel;

        for (float i = 0; i < scaleTime; i += Time.deltaTime)
        {
            currentLightningBallEffect.GetComponent<Transform>().localScale = Vector3.Lerp(origScale * deltaScale, origScale, i / scaleTime);
            yield return null;
        }

        Destroy(currentLightningBallEffect.gameObject);
        isRunning = false;
    }*/

}
