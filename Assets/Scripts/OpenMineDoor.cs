﻿using UnityEngine;
using System.Collections;

public class OpenMineDoor : MonoBehaviour {

	public Animator DoorLeft;
	public Animator DoorRight;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider c) {
		if (c.tag == "Glowstick" || c.tag == "Player") {
			DoorLeft.enabled = true;
			DoorRight.enabled = true;
		}
	}
}
