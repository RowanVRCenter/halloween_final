﻿using UnityEngine;
using System.Collections;

public class Roach : MonoBehaviour {

	public static int roachNumber = 0;

	public float timeToChangeDirection = 2.0f;
	public float activeLength = 3.0f;
	public float pauseLength = 1.0f;
	public float speed = 2.2f;
	protected float currentTime = 0.0f;
	protected bool isPaused = false;
	
	void Awake()
	{
		roachNumber += 5;
		Random.seed = roachNumber;	}
	

	// Use this for initialization
	void Start () {
		SetNewDirection();
	}

	public void SetNewDirection()
	{
		//Get random new direction
		Vector3 newDirection = transform.forward * (Random.value - 0.5f) + transform.right * (Random.value - 0.5f);
		transform.LookAt (transform.position + newDirection,transform.up);
	}

	void HandlePause()
	{
		currentTime += Time.deltaTime;
		if(currentTime >= pauseLength)
		{
			currentTime = 0.0f;
			isPaused = false;
			SetNewDirection();
		}
	}

	void HandleActive()
	{
		currentTime += Time.deltaTime;
		if(currentTime >= activeLength)
		{
			currentTime = 0.0f;
			isPaused = true;
		}
		else
		{
			Move ();
		}
	}

	void Move()
	{
		transform.position += transform.forward * speed * Time.deltaTime;
	}

	// Update is called once per frame
	void Update () {

		if( !isPaused)
		{
			HandleActive();
		}

		else
		{
			HandlePause();
		}

	}
}
