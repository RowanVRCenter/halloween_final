using UnityEngine;
using System.Collections;

public class OpenGateDoor : MonoBehaviour {

	[HideInInspector]
	public bool locked = true;
	public bool openDoor = false;
	public AudioClip[] doorOpenClip,doorLockedClip,doorClosedClip;

	private bool coroutineRunning = false;
	private BoxCollider doorCollider;
	private float startPosition;
	private float doorSize;
	private float waitCloseTimer = 3f;

//	void OnTriggerEnter(Collider collider){
//		if(collider.gameObject.tag == "Player" || collider.gameObject.tag == "FleshMonster"){
//			playerInRange++;
//		}
//	}
//
//	void OnTriggerExit(Collider collider){
//		if(collider.gameObject.tag == "Player" || collider.gameObject.tag == "FleshMonster"){
//			playerInRange--;
//		}
//	}

	// Use this for initialization
	void Start () {
		if(GetComponent<MeshRenderer>().bounds.size.z < GetComponent<MeshRenderer>().bounds.size.x){
			doorSize = GetComponent<MeshRenderer>().bounds.size.x;
		}else{
			doorSize = GetComponent<MeshRenderer>().bounds.size.z;
		}
		startPosition = transform.localPosition.x;
		doorCollider = GetComponent<BoxCollider>();
	}
	
//	// Update is called once per frame
//	void Update () {
//		if (((playerInRange!=0 && getReal3D.Input.GetButtonDown("XBox360Controller_X"))||openDoor)&&doorClosed){
//			if (!locked || openDoor){
//				StartCoroutine(DoorRoutine());
//				openDoor = false;
//			}
//			else{
//				audio.clip = doorLockedClip;
//				audio.Play();
//			}
//		}
//		if((playerInRange && !doorClosing && (getReal3D.Input.GetButtonDown("XBox360Controller_X") || Input.GetKeyDown(KeyCode.E))) || openDoor){
//			if (!locked || openDoor){
//				openDoor = true;
//				OpenDoor();
//				if (!audio.isPlaying){
//					audio.clip = doorOpenClip;
//					audio.Play ();
//					Debug.Log("Open Gate Door");
//				}
//			}
//			else{
//				audio.clip = doorLockedClip;
//				audio.Play();
//			}
//		}
//		if(waitCloseTimer <= 0f && !playerInRange){
//			doorClosing = true;
//			CloseDoor();
//			if (audio.clip != doorClosedClip){
//				audio.clip = doorClosedClip;
//				audio.Play ();
//				Debug.Log("Closing Gate Door");
//			}
//		}
//		else if(!openDoor && !doorClosed){
//			waitCloseTimer -= Time.deltaTime;
//		}
//	}

//	void OpenDoor(){
//		if(transform.localRotation.eulerAngles.y > 270f || transform.localRotation.eulerAngles.y <= 0f){
//			doorClosed = false;
//			transform.RotateAround(transform.position,Vector3.up,-90f*Time.deltaTime/2f);
//		}else{
//			openDoor = false;
//		}
//	}
//
//	void CloseDoor(){
//		//Debug.Log(transform.localRotation.eulerAngles.y >= 270f || transform.localRotation.eulerAngles.y >= 0f);
//		if(transform.localRotation.eulerAngles.y >= 270f || transform.localRotation.eulerAngles.y >= 0f){
//			transform.RotateAround(transform.position,Vector3.up,90f*Time.deltaTime/2f);
//		}else{
//			doorClosed = true;
//			doorClosing = false;
//			waitCloseTimer = waitCloseTimerReset;
//		}
//	}

	public void OpenDoor(){
		//if (((playerInRange!=0 && getReal3D.Input.GetButtonDown("XBox360Controller_X"))||openDoor)&&doorClosed){
		//Debug.Log("Run open Door");
			if ((!locked || openDoor) && !coroutineRunning){
				coroutineRunning = true;
				StartCoroutine(DoorRoutine());
				//openDoor = false;
			}
			else{
				GetComponent<AudioSource>().clip = doorLockedClip[0];
				GetComponent<AudioSource>().Play();
			}
		//}
	}

	IEnumerator DoorRoutine(){
		GetComponent<AudioSource> ().clip = doorOpenClip [Random.Range(0, doorOpenClip.Length)];
		GetComponent<AudioSource>().Play ();
		//transform.RotateAround(transform.position,Vector3.up,-90f*Time.deltaTime/2f);
		yield return null;
		while(transform.localPosition.x <= startPosition + doorSize){
			transform.localPosition += Vector3.right*Time.deltaTime;
			if(transform.localPosition.x >= (startPosition + doorSize*0.5f) && doorCollider.enabled){
				doorCollider.enabled = false;
			}
			yield return null;
		}
		GetComponent<AudioSource>().Stop();

		yield return new WaitForSeconds(waitCloseTimer);
		GetComponent<AudioSource>().Play ();
		while(transform.localPosition.x >= startPosition){
			transform.localPosition -= Vector3.right*Time.deltaTime;
			yield return null;
		}
		GetComponent<AudioSource>().Stop();

		doorCollider.enabled = true;
		GetComponentInParent<GateDoorTrigger>().runOnce = false;
		coroutineRunning = false;
	}
}
