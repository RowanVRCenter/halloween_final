﻿using UnityEngine;
using System.Collections;

public class Glowstick : MonoBehaviour
{

    public bool testGlow = false;
    private Rigidbody rb;
    public ParticleSystem ps;
    public Transform lightTrans;
    public float shakeThresh = 3;
    public float lightOffset = .2f;
	public FixedJoint seal;
	public bool inBelt;

    private Vector3 lightPosWithOffset;
    private Light glowLight;
    public float glowLife = 60;
    private bool glowDead;
    public bool activated;
    private float breakForce;
    public int shakeCount;
    public int velMeasures;
    public float glowDelay = 5; //[seconds] Time it takes for the glowstick to reach full intensity
    public float shakeTimer = .5f;
	public Transform lightningBall;
	private Vector3 origScale;
	private Vector3 currScale;
	public float deltaScale;
	public float maxIntensity;
	public float scaleSpeed;
	private AudioSource sound;
	public float volumeSpeed;

    // Use this for initialization
    void Start()
    {
		if (tag == "Lightstick") {
			origScale = lightningBall.localScale;
			currScale = lightningBall.localScale;
			sound = GetComponent<AudioSource> ();
			sound.Play ();
			sound.volume = 0;
		}
        rb = GetComponent<Rigidbody>();
        glowLight = GetComponentInChildren<Light>();
        shakeCount = 0;
        shakeTimer = 0;
        activated = false;
        glowLight.enabled = true;
        ps.enableEmission = false;
        velMeasures = 0;
        glowLight.intensity = 0;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
		if (activated && !glowDead)
        {
            glowLife -= Time.deltaTime;
            if (glowLife <= 0)
            {
                glowDead = true;
                Debug.Log("Glowstick died");
                StartCoroutine(FadeOff());
				if (tag == "Lightstick") {
					StartCoroutine (LightningBallShrink ());
				}
            }
        }

		if (glowDead && !inBelt && this.GetComponent<FixedJoint>() == null) {
			Debug.Log ("Trying to destroy stick ~Saad");
			Destroy (this.gameObject);
		}

        if (activated && GetComponent<ConfigurableJoint>() == null)
        {
            //Debug.Log("Should be running");
            lightPosWithOffset = new Vector3(lightTrans.position.x, lightTrans.position.y + lightOffset, lightTrans.position.z);
            glowLight.transform.position = lightPosWithOffset;
        }

		if (!activated) {
			if (GetComponent<ConfigurableJoint> () == null) {
				if (seal == null) {
					activated = true;
					StartCoroutine (FadeOn ());
					if (tag == "Lightstick") {
						StartCoroutine (LightningBallEnlarge ());
					}
					Debug.Log ("Glowstick activated");
					//Destroy (seal.gameObject);
				}
			}
		}

        //Scaffolding below (just for update)

        if (testGlow)
        {
            StartCoroutine(FadeOn());
			if (tag == "Lightstick") {
				StartCoroutine (LightningBallEnlarge ());
			}
            activated = true;
            testGlow = false;
        }
    }

    IEnumerator FadeOn()
    {
        if(ps.enableEmission != null)
        {
            ps.enableEmission = true;
        }
        while (glowLight.intensity < maxIntensity)
        {
            glowLight.intensity += Time.deltaTime / glowDelay;
            yield return null;
        }
    }

    IEnumerator FadeOff()
    {
        ps.enableEmission = false;
        while (glowLight.intensity > 0)
        {
            glowLight.intensity -= Time.deltaTime / glowDelay;
            yield return null;
        }
    }

	IEnumerator LightningBallEnlarge()
	{
		Debug.Log ("Enlarging");
		while (currScale.magnitude < deltaScale * origScale.magnitude) {
			lightningBall.localScale = new Vector3 (currScale.x + Time.deltaTime*scaleSpeed, currScale.y + Time.deltaTime*scaleSpeed, currScale.z + Time.deltaTime*scaleSpeed);
			sound.volume += Time.deltaTime*volumeSpeed;
			currScale = lightningBall.localScale;
			yield return null;
		}
		StartCoroutine (VaryLight ());
	}

	IEnumerator LightningBallShrink()
	{
		while (currScale.magnitude > origScale.magnitude/5) {
			lightningBall.localScale = new Vector3 (currScale.x - Time.deltaTime*scaleSpeed, currScale.y - Time.deltaTime*scaleSpeed, currScale.z - Time.deltaTime*scaleSpeed);
			sound.volume -= Time.deltaTime*volumeSpeed;
			currScale = lightningBall.localScale;
			yield return null;
		}
		Destroy (lightningBall.gameObject);
	}

	IEnumerator VaryLight()
	{
		while (!glowDead && activated) {
			glowLight.intensity = maxIntensity + .1f*Mathf.Sin (Time.time+Random.Range(-Time.time,.5f*Time.time));
			yield return null;
		}
	}
}
