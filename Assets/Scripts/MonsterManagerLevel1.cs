﻿using UnityEngine;
using System.Collections;

public class MonsterManagerLevel1 : MonoBehaviour {
	
	//public GameObject highIntensityFlashlight;
	//public GameObject lowIntensityFlashlight;

	public enum MonsterState
	{
		_1_hallway,
		_2_deadend,
		_3_end,
		_4_hide,
		_5_killYou
	}

	protected MonsterState monsterState;

	protected SkinnyMonsterHashIDs skinnyMonsterHashIDs;
	protected Animator skinnyMonsterAnimator;
	public GameObject skinnyMonster;
	public GameObject player;
	public float strikeDistanceFromPlayer;

	public GameObject hallwayRightLeanPoint;
	public GameObject deadendLeftLeanPoint;
	public GameObject endRightLeanPoint;
	public GameObject hidePoint;

	public GameObject trigger1;
	public GameObject trigger2;
	public GameObject trigger3;

	public float playerProximityRadius = 8.0f;	//How close the player can get before triggering regardless of lights
	//public float highIntensityProximityRadius;	//How close you need to be to trigger high intensity flashlight 
	//public float lowIntensityProximityRadius;	//How close you need to be to trigger low intensitsy flashlight

	public float highIntensityDotProductFactor = 0.866f;
	public float lowInensityDotProductFactor = 0.866f;

	protected bool animationLock = false;

	void Awake()
	{
		skinnyMonsterHashIDs = skinnyMonster.GetComponent<SkinnyMonsterHashIDs>();
		skinnyMonsterAnimator = skinnyMonster.GetComponent<Animator>();
	}

	// Use this for initialization
	void Start () {
		hallwayRightLeanPoint.SetActive(false);
		deadendLeftLeanPoint.SetActive(false);
		endRightLeanPoint.SetActive(false);

		StartCoroutine("SkinnyMonsterHallway");
	}
	
	// Update is called once per frame
	void Update () {


		if(!animationLock)
		{
			MoveToNextState();
		}
	}

	protected void MoveToNextState()
	{
		switch( monsterState )
		{
		case MonsterState._1_hallway:
			if(trigger1.GetComponent<MonsterMoveTrigger1>().moveToNextSpot){
				StartCoroutine("SkinnyMonsterDeadend");
			}
			break;
		case MonsterState._2_deadend:
			if(trigger2.GetComponent<MonsterMoveTrigger2>().moveToNextSpot){
				StartCoroutine("SkinnyMonsterEnd");
			}
			break;
		case MonsterState._3_end:
			if(trigger3.GetComponent<MonsterMoveTrigger3>().moveToNextSpot){
				StartCoroutine("SkinnyMonsterHide");
			}
			break;
		}
	}

	IEnumerator WaitForTime( float time)
	{
		yield return new WaitForSeconds(time);
	}

	IEnumerator SkinnyMonsterHallway()
	{
		animationLock = true;
		monsterState = MonsterState._1_hallway;
		skinnyMonster.transform.position = hallwayRightLeanPoint.transform.position;
		skinnyMonster.transform.rotation = hallwayRightLeanPoint.transform.rotation;
		skinnyMonsterAnimator.SetBool(skinnyMonsterHashIDs.peekRightBool, true);
		yield return StartCoroutine("WaitForTime", 0.6f);
		animationLock = false;
	}

	IEnumerator SkinnyMonsterDeadend()
	{
		animationLock = true;
		monsterState = MonsterState._2_deadend;
		skinnyMonsterAnimator.SetBool(skinnyMonsterHashIDs.peekRightBool, false);
		yield return StartCoroutine("WaitForTime", 1.0f);
		skinnyMonster.transform.position = deadendLeftLeanPoint.transform.position;
		skinnyMonster.transform.rotation = deadendLeftLeanPoint.transform.rotation;
		skinnyMonsterAnimator.SetBool(skinnyMonsterHashIDs.peekLeftBool, true);
		yield return StartCoroutine("WaitForTime", 1.5f);
		animationLock = false;
	}

	IEnumerator SkinnyMonsterEnd()
	{
		animationLock = true;
		monsterState = MonsterState._3_end;
		skinnyMonsterAnimator.SetBool(skinnyMonsterHashIDs.peekLeftBool, false);
		yield return StartCoroutine("WaitForTime", 1.5f);
		skinnyMonster.transform.position = endRightLeanPoint.transform.position;
		skinnyMonster.transform.rotation = endRightLeanPoint.transform.rotation;
		skinnyMonsterAnimator.SetBool(skinnyMonsterHashIDs.peekRightBool, true);
		yield return StartCoroutine("WaitForTime", 1.5f);
		animationLock = false;
	}

	IEnumerator SkinnyMonsterHide()
	{
		animationLock = true;
		monsterState = MonsterState._4_hide;
		skinnyMonsterAnimator.SetBool(skinnyMonsterHashIDs.peekRightBool, false);
		yield return new WaitForEndOfFrame();
		skinnyMonster.transform.position = hidePoint.transform.position;
		skinnyMonster.transform.rotation = hidePoint.transform.rotation;
		animationLock = false;
	}

	IEnumerator PopInFrontOfPlayer()
	{
		monsterState = MonsterState._5_killYou;

		Vector3 spawnPoint = player.transform.position + player.transform.forward * strikeDistanceFromPlayer;

		skinnyMonster.transform.position = spawnPoint;
		skinnyMonster.transform.rotation = Quaternion.FromToRotation(skinnyMonster.transform.forward, player.transform.forward * -1.0f);
		yield return StartCoroutine("WaitForTime", 1.5f);
		skinnyMonsterAnimator.SetBool(skinnyMonsterHashIDs.strikeLeftHash, true);

	}
}
