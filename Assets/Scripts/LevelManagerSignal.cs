﻿using UnityEngine;
using System.Collections;

public class LevelManagerSignal : MonoBehaviour {

	public Level0Manager manager;
	public Level0Manager.Signals signal;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider collider)
	{
		if(collider.tag == "Player")
		{
			Debug.Log("Trigger Enter");
			manager.getSignal(signal);
		}
	}

}
