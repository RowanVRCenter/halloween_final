﻿using UnityEngine;
using System.Collections;

public class EyeballProjectile : MonoBehaviour {

	public float speed;
	public float lifetime;
	private Rigidbody rb;
	private float counter;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		rb.MovePosition ((transform.forward * Time.deltaTime * speed) + transform.position);
		counter += Time.deltaTime;
		if (counter > lifetime)
			Destroy (this.gameObject);
	}

    void OnTriggerEnter(Collider c)
    {
        Debug.Log("Hit something");
        if (c.tag == "Head")
        {
            Debug.Log("Hit head");
            c.gameObject.SendMessage("Hit", .5f, SendMessageOptions.DontRequireReceiver);
        }
    }
    void OnCollisionEnter()
    {
        //Destroy(this);
    }
}
