﻿using UnityEngine;
using System.Collections;

public class Belt : MonoBehaviour {

    public Transform head;

	// Use this for initialization
	void Start () {
        //head = GetComponentInParent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 m_Position = new Vector3(head.transform.position.x, head.transform.position.y - 1.2f, head.transform.position.z);
        transform.position = m_Position;
    }
}
