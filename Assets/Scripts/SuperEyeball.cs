﻿using UnityEngine;
using System.Collections;

public class SuperEyeball : MonoBehaviour {

    public GameObject projectilePrefab;

    private RaycastHit hit;
    private GameObject projectile;

    public float attackDelay;
    private float counter;


    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        Vector3 fwd = transform.TransformDirection(Vector3.forward); //the direction the player is facing
        Debug.DrawRay(transform.position, fwd * 100, Color.green);
        if (Physics.Raycast(transform.position, fwd, out hit, 100))
        {
            if (hit.collider.tag == "Head")
            {
                if (counter > attackDelay)
                {
                    Attack();
                    counter = 0;
                }
            }
        }
        counter += Time.deltaTime;
    }

    void Attack()
    {
        projectile = Instantiate(projectilePrefab, this.transform.position+transform.forward*2, this.transform.rotation) as GameObject;
    }
}
