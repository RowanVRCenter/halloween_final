﻿#define usingGetReal3D

using UnityEngine;
using System.Collections;

//If you query the button state, it drops all states back to false.
public class XBoxDPad : MonoBehaviour {

	public float queryDelay = 0.75f;
	protected float queryTimer = 0.0f;

	protected bool up = false;
	protected bool down = false;
	protected bool left = false;
	protected bool right = false;

	protected bool stateQueried = false;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

		queryTimer += Time.deltaTime;

		if(queryTimer >= queryDelay)
		{
			queryTimer = 0.0f;
			stateQueried = false;
		}

		up = false;
		down = false;
		left = false;
		right = false;

		#if usingGetReal3D
		if(Input.GetAxis("XBox360Controller_DPad_Horizontal") >= 0.9) //Right
		{
			right = true;
		}
		
		else if(Input.GetAxis("XBox360Controller_DPad_Horizontal") <= -0.9) //Left
		{
			left = true;
		}
		
		else if(Input.GetAxis("XBox360Controller_DPad_Vertical") >= 0.9) //Up
		{
			up = true;
		}
		
		else if(Input.GetAxis("XBox360Controller_DPad_Vertical") <= -0.9) //Down
		{
			down = true;
		}

		#else
		//Update True Visits
		if(Input.GetAxis("DPadHorizontal") >= 0.9) //Right
		{
			right = true;
		}
		
		else if(Input.GetAxis("DPadHorizontal") <= -0.9) //Left
		{
			left = true;
		}
		
		else if(Input.GetAxis("DPadVertical") >= 0.9) //Up
		{
			up = true;
		}
		
		else if(Input.GetAxis("DPadVertical") <= -0.9) //Down
		{
			down = true;
		}
		#endif
	}

	public bool Up {
		get {
			if(stateQueried)
				return false;
			if(this.up)
				stateQueried = true;
			return this.up;
		}
	}

	public bool Down {
		get {
			if(stateQueried)
				return false;
			if(this.down)
				stateQueried = true;
			return this.down;
		}
	}

	public bool Left {
		get {
			if(stateQueried)
				return false;
			if(this.left)
				stateQueried = true;
			return this.left;
		}
	}

	public bool Right {
		get {
			if(stateQueried)
				return false;
			if(this.right)
				stateQueried = true;
			return this.right;
		}
	}
}
