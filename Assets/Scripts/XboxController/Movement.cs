﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
public class Movement : MonoBehaviour {

	public CharacterController cc;
	public GameObject xboxcontroller;

	public float forwardSpeed = 15.0f;
	//public float backwardSpeed = 10.0f;
	//public float strafeSpeed = 1.0f;

	public float rotationSpeed = 18.0f;
	//public float lowSpeed = 4.0f;
	//public float highSpeed = 120.0f;

	//public float lowElevation = 2.0f;
	//public float highElevation = 500.0f;

	public AudioSource WalkingAudioSource;
	public AudioClip DryWalking,WetWalking,FleshWalking,FloorWalking,SewerWalking;

	public bool canMove = true;

	private RaycastHit hit;

	void Awake(){
		GameObject WAS = (GameObject)Instantiate(WalkingAudioSource.gameObject);
		WalkingAudioSource = WAS.GetComponent<AudioSource>();
	}

	void Start () {
	
		cc = GetComponent<CharacterController> ();
		WalkingAudioSource.loop = true;
		WalkingAudioSource.bypassEffects = true;
		WalkingAudioSource.bypassListenerEffects = true;
		WalkingAudioSource.bypassReverbZones = true;
		WalkingAudioSource.volume = 1f;
		switch(Application.loadedLevel){
		case 1:
			WalkingAudioSource.volume = 1f;
			WalkingAudioSource.clip = DryWalking;
			break;
		case 2:
			WalkingAudioSource.volume =1f;
			WalkingAudioSource.clip = SewerWalking;
			break;
		case 3:
			WalkingAudioSource.volume = 0.75f;
			WalkingAudioSource.clip = FleshWalking;
			break;
		default:
			WalkingAudioSource.volume = 1f;
			WalkingAudioSource.clip = FloorWalking;
			break;
		}
		StartCoroutine(SurfaceCheck());
	}

	// Update is called once per frame
	void Update () {

		if(canMove)
		{
			Vector3 cameraRotationGlobal = Vector3.zero;
			//cameraRotationGlobal.y = Input.GetAxis("XBox360Controller_Right_Stick_Horizontal") * Time.deltaTime * rotationSpeed;
			gameObject.transform.Rotate (cameraRotationGlobal, Space.World);

			Vector3 deltaPositionLocal = xboxcontroller.transform.forward * Input.GetAxis("XBox360Controller_Left_Stick_Vertical") * forwardSpeed;
			deltaPositionLocal += xboxcontroller.transform.right * Input.GetAxis ("XBox360Controller_Left_Stick_Horizontal") * forwardSpeed * 2f;
			cc.SimpleMove (deltaPositionLocal);

			if (deltaPositionLocal.magnitude >= 0.25f*forwardSpeed){
				if (!WalkingAudioSource.isPlaying){
					WalkingAudioSource.Play();
				}
			}
			else{
				if (WalkingAudioSource.isPlaying){
					WalkingAudioSource.Stop();
				}
			}
		}
		else
		{
			cc.SimpleMove(Vector3.zero);
		}

	}

	public void disableMovement()
	{
		canMove = false;
	}

	public void enableMovement()
	{
		canMove = true;
	}

	IEnumerator SurfaceCheck(){
		while (true){
			if (Physics.Raycast(transform.position+Vector3.up*1.67f,Vector3.down,out hit)){
				switch (hit.collider.gameObject.tag){
				case "Water":
					if (WalkingAudioSource.clip != WetWalking){
						WalkingAudioSource.volume = 1f;
						WalkingAudioSource.clip = WetWalking;
					}
					break;
				case "Flesh":
					if (WalkingAudioSource.clip != FleshWalking){
						WalkingAudioSource.volume = 0.75f;
						WalkingAudioSource.clip = FleshWalking;
					}
					break;
				case "Sewer":
					if (WalkingAudioSource.clip != SewerWalking){
						WalkingAudioSource.volume = 1f;
						WalkingAudioSource.clip = SewerWalking;
					}
					break;
				case "Gravel":
					if (WalkingAudioSource.clip != DryWalking){
						WalkingAudioSource.volume = 1f;
						WalkingAudioSource.clip = DryWalking;
					}
					break;
				case "Floor":
					if (WalkingAudioSource.clip != FloorWalking){
						WalkingAudioSource.volume = 1f;
						WalkingAudioSource.clip = FloorWalking;
					}
					break;
				}
				//if (hit.distance > 2f)WalkingAudioSource.volume = 0;
				//else WalkingAudioSource.volume = 1f;
			}
			yield return new WaitForSeconds(0.2f);
		}
	}
}
