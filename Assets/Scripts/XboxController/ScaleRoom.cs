﻿#define usingGetReal3D

using UnityEngine;
using System.Collections;

public class ScaleRoom : MonoBehaviour {

	public TextMesh xScaleText;
	private GameObject[] scaleObjects;
	protected XBoxDPad xBoxDPad;
	// Use this for initialization
	void Start () {
		xBoxDPad = GetComponent<XBoxDPad>();
		scaleObjects = GameObject.FindGameObjectsWithTag("Interactable");
	}
	
	// Update is called once per frame
	void Update () {

		xScaleText.text = "X scale: " + scaleObjects[0].transform.localScale.x.ToString() + " Y scale: " + scaleObjects[0].transform.localScale.y.ToString();

		#if usingGetReal3D
		if(xBoxDPad.Left){
			foreach(GameObject obj in scaleObjects){
				Vector3 tmpScale = obj.transform.localScale;
				tmpScale.x = tmpScale.x*0.99f;
				obj.transform.localScale = tmpScale;
			}
		}
		if(xBoxDPad.Right){
			foreach(GameObject obj in scaleObjects){
				Vector3 tmpScale = obj.transform.localScale;
				tmpScale.x = tmpScale.x*1.01f;
				obj.transform.localScale = tmpScale;
			}
		}
		if(xBoxDPad.Down){
			foreach(GameObject obj in scaleObjects){
				Vector3 tmpScale = obj.transform.localScale;
				tmpScale.y = tmpScale.y*0.99f;
				obj.transform.localScale = tmpScale;
			}
		}
		if(xBoxDPad.Up){
			foreach(GameObject obj in scaleObjects){
				Vector3 tmpScale = obj.transform.localScale;
				tmpScale.y = tmpScale.y*1.01f;
				obj.transform.localScale = tmpScale;
			}
		}
		#else
		if(Input.GetKey(KeyCode.LeftArrow)){
			foreach(GameObject obj in scaleObjects){
				Vector3 tmpScale = obj.transform.localScale;
				tmpScale.x = tmpScale.x*0.99f;
				obj.transform.localScale = tmpScale;
			}
		}
		if(Input.GetKey(KeyCode.RightArrow)){
			foreach(GameObject obj in scaleObjects){
				Vector3 tmpScale = obj.transform.localScale;
				tmpScale.x = tmpScale.x*1.01f;
				obj.transform.localScale = tmpScale;
			}
		}
		if(Input.GetKey(KeyCode.DownArrow)){
			foreach(GameObject obj in scaleObjects){
				Vector3 tmpScale = obj.transform.localScale;
				tmpScale.y = tmpScale.y*0.99f;
				obj.transform.localScale = tmpScale;
			}
		}
		if(Input.GetKey(KeyCode.UpArrow)){
			foreach(GameObject obj in scaleObjects){
				Vector3 tmpScale = obj.transform.localScale;
				tmpScale.y = tmpScale.y*1.01f;
				obj.transform.localScale = tmpScale;
			}
		}
		#endif
	}
}
