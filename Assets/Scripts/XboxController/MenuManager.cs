﻿#define usingGetReal3D

using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour {

	public GameObject mainMenu;
	public GameObject walkingRouteMenu;
	public GameObject boundariesMenu;
	public GameObject securityMenu;
	public GameObject crowdsMenu;
	public GameObject crowdDensityMenu;

	public GameObject routesPublic = null;
	public GameObject routesParkway = null;
	public GameObject routesIndpendenceMall = null;

	public GameObject boundariesParkway = null;
	public GameObject boundariesIndependenceMall = null;
	public GameObject boundariesClosed = null;
	public GameObject boundariesPress = null;

	public GameObject medicalStations = null;
	public GameObject securityStations = null;

	protected bool menuActive = false;
	protected GameObject currentMenu = null;

	protected bool dPadPressed = false;

	protected XBoxDPad xBoxDPad;

	// Use this for initialization
	void Start () {
		xBoxDPad = GetComponent<XBoxDPad>();

		currentMenu = mainMenu;

		mainMenu.SetActive(false);
		walkingRouteMenu.SetActive(false);
		boundariesMenu.SetActive(false);
		securityMenu.SetActive(false);
		crowdsMenu.SetActive(false);
	}

	#if usingGetReal3D

	// Update is called once per frame
	void Update () {
	
		if(!menuActive)
		{
			//Has the start button been pressed?
			if (Input.GetButtonDown("XBox360Controller_Start"))
			{
				menuActive = true;
				currentMenu.SetActive(menuActive);
			}

			return;
		}

		else
		{

			//Has the start button been pressed?
			if (Input.GetButtonDown("XBox360Controller_Start"))
			{
				menuActive = false;
				currentMenu.SetActive(menuActive);
				return;
			}

			if(currentMenu == mainMenu)
			{
				if(xBoxDPad.Up)
				{
					currentMenu.SetActive(false);
					currentMenu = walkingRouteMenu;
					currentMenu.SetActive(true);
				}
			
				else if(xBoxDPad.Down)
				{
					currentMenu.SetActive(false);
					currentMenu = securityMenu;
					currentMenu.SetActive(true);
				}

				else if(xBoxDPad.Left)
				{
					currentMenu.SetActive(false);
					currentMenu = boundariesMenu;
					currentMenu.SetActive(true);
				}


				else if(xBoxDPad.Right)
				{
					currentMenu.SetActive(false);
					currentMenu = crowdsMenu;
					currentMenu.SetActive(true);
				}

				else if( Input.GetButtonDown("XBox360Controller_B") )
				{
					//Return to main menu
					currentMenu.SetActive(false);
					currentMenu = mainMenu;
					currentMenu.SetActive(true);
					//Back out of menu
					menuActive = false;
					currentMenu.SetActive(menuActive);
					return;

				}
			}

			else if(currentMenu == walkingRouteMenu)
			{
				if(xBoxDPad.Up)
				{
					routesPublic.SetActive(!routesPublic.activeSelf);
				}
				
				else if(xBoxDPad.Down)
				{
					//Remove?
				}
				
				else if(xBoxDPad.Left)
				{
					routesParkway.SetActive(!routesParkway.activeSelf);
				}
				
				else if(xBoxDPad.Right)
				{
					routesIndpendenceMall.SetActive(!routesIndpendenceMall.activeSelf);
				}
				
				else if( Input.GetButtonDown("XBox360Controller_B") )
				{
					//Return to main menu
					currentMenu.SetActive(false);
					currentMenu = mainMenu;
					currentMenu.SetActive(true);
				}
			}

			else if(currentMenu == boundariesMenu)
			{
				if(xBoxDPad.Up)
				{
					boundariesClosed.SetActive(!boundariesClosed.activeSelf);
				}
				
				else if(xBoxDPad.Down)
				{
					boundariesPress.SetActive(!boundariesPress.activeSelf);
				}
				
				else if(xBoxDPad.Left)
				{
					boundariesParkway.SetActive(!boundariesParkway.activeSelf);
				}
				
				
				else if(xBoxDPad.Right)
				{
					boundariesIndependenceMall.SetActive(!boundariesIndependenceMall.activeSelf);
				}
				
				else if( Input.GetButtonDown("XBox360Controller_B") )
				{
					//Return to main menu
					currentMenu.SetActive(false);
					currentMenu = mainMenu;
					currentMenu.SetActive(true);
				}
			}

			else if(currentMenu == securityMenu)
			{
				if(xBoxDPad.Up)
				{
					securityStations.SetActive(!securityStations.activeSelf);
				}
				
				else if(xBoxDPad.Down)
				{
					//Toggle visibility of ?
				}
				
				else if(xBoxDPad.Left)
				{
					medicalStations.SetActive(!medicalStations.activeSelf);
				}
				
				
				else if(xBoxDPad.Right)
				{
					//Toggle visibility of independece mall tickets
				}
				
				else if( Input.GetButtonDown("XBox360Controller_B") )
				{
					//Return to main menu
					currentMenu.SetActive(false);
					currentMenu = mainMenu;
					currentMenu.SetActive(true);
				}
			}

			else if(currentMenu == crowdsMenu)
			{
				if(xBoxDPad.Up)
				{
					//Toggle visibility of public paths
				}
				
				else if(xBoxDPad.Down)
				{
					//Toggle visibility of ?
				}
				
				else if(xBoxDPad.Left)
				{
					//Aerial 
				}
				
				
				else if(xBoxDPad.Right)
				{
					//Aerial Density Menu
					currentMenu.SetActive(false);
					currentMenu = crowdDensityMenu;
					currentMenu.SetActive(true);
				}
				
				else if( Input.GetButtonDown("XBox360Controller_B") )
				{
					//Return to main menu
					currentMenu.SetActive(false);
					currentMenu = mainMenu;
					currentMenu.SetActive(true);
				}
			}
		
			else if(currentMenu == crowdDensityMenu)
			{
				if(xBoxDPad.Up)
				{
					//Toggle visibility of public paths
				}
				
				else if(xBoxDPad.Down)
				{
					//Toggle visibility of ?
				}
				
				else if(xBoxDPad.Left)
				{
					//Toggle visibility of Parkway tickets
				}
				
				
				else if(xBoxDPad.Right)
				{
					//Toggle visibility of independece mall tickets
				}
				
				else if( Input.GetButtonDown("XBox360Controller_B") )
				{
					//Return to main menu
					currentMenu.SetActive(false);
					currentMenu = crowdsMenu;
					currentMenu.SetActive(true);
				}
			}

		}

	}
	#else
	// Update is called once per frame
	void Update () {
		
		if(!menuActive)
		{
			//Has the start button been pressed?
			if (Input.GetKeyDown(KeyCode.JoystickButton7))
			{
				menuActive = true;
				currentMenu.SetActive(menuActive);
			}
			
			return;
		}
		
		else
		{
			
			//Has the start button been pressed?
			if (Input.GetKeyDown(KeyCode.JoystickButton7))
			{
				menuActive = false;
				currentMenu.SetActive(menuActive);
				return;
			}
			
			if(currentMenu == mainMenu)
			{
				if(xBoxDPad.Up)
				{
					currentMenu.SetActive(false);
					currentMenu = walkingRouteMenu;
					currentMenu.SetActive(true);
				}
				
				else if(xBoxDPad.Down)
				{
					currentMenu.SetActive(false);
					currentMenu = securityMenu;
					currentMenu.SetActive(true);
				}
				
				else if(xBoxDPad.Left)
				{
					currentMenu.SetActive(false);
					currentMenu = boundariesMenu;
					currentMenu.SetActive(true);
				}
				
				
				else if(xBoxDPad.Right)
				{
					currentMenu.SetActive(false);
					currentMenu = crowdsMenu;
					currentMenu.SetActive(true);
				}
				
				else if( Input.GetKeyDown(KeyCode.JoystickButton1) )
				{
					//Return to main menu
					currentMenu.SetActive(false);
					currentMenu = mainMenu;
					currentMenu.SetActive(true);
				}
			}
			
			else if(currentMenu == walkingRouteMenu)
			{
				if(xBoxDPad.Up)
				{
					routesPublic.SetActive(!routesPublic.activeSelf);
				}
				
				else if(xBoxDPad.Down)
				{
					//Remove?
				}
				
				else if(xBoxDPad.Left)
				{
					routesParkway.SetActive(!routesParkway.activeSelf);
				}
				
				else if(xBoxDPad.Right)
				{
					routesIndpendenceMall.SetActive(!routesIndpendenceMall.activeSelf);
				}
				
				else if( Input.GetKeyDown(KeyCode.JoystickButton1) )
				{
					//Return to main menu
					currentMenu.SetActive(false);
					currentMenu = mainMenu;
					currentMenu.SetActive(true);
				}
			}
			
			else if(currentMenu == boundariesMenu)
			{
				if(xBoxDPad.Up)
				{
					boundariesClosed.SetActive(!boundariesClosed.activeSelf);
				}
				
				else if(xBoxDPad.Down)
				{
					boundariesPress.SetActive(!boundariesPress.activeSelf);
				}
				
				else if(xBoxDPad.Left)
				{
					boundariesParkway.SetActive(!boundariesParkway.activeSelf);
				}
				
				
				else if(xBoxDPad.Right)
				{
					boundariesIndependenceMall.SetActive(!boundariesIndependenceMall.activeSelf);
				}
				
				else if( Input.GetKeyDown(KeyCode.JoystickButton1) )
				{
					//Return to main menu
					currentMenu.SetActive(false);
					currentMenu = mainMenu;
					currentMenu.SetActive(true);
				}
			}
			
			else if(currentMenu == securityMenu)
			{
				if(xBoxDPad.Up)
				{
					securityStations.SetActive(!securityStations.activeSelf);
				}
				
				else if(xBoxDPad.Down)
				{
					//Toggle visibility of ?
				}
				
				else if(xBoxDPad.Left)
				{
					medicalStations.SetActive(!medicalStations.activeSelf);
				}
				
				
				else if(xBoxDPad.Right)
				{
					//Toggle visibility of independece mall tickets
				}
				
				else if( Input.GetKeyDown(KeyCode.JoystickButton1) )
				{
					//Return to main menu
					currentMenu.SetActive(false);
					currentMenu = mainMenu;
					currentMenu.SetActive(true);
				}
			}
			
			else if(currentMenu == crowdsMenu)
			{
				if(xBoxDPad.Up)
				{
					//Toggle visibility of public paths
				}
				
				else if(xBoxDPad.Down)
				{
					//Toggle visibility of ?
				}
				
				else if(xBoxDPad.Left)
				{
					//Aerial 
				}
				
				
				else if(xBoxDPad.Right)
				{
					//Aerial Density Menu
					currentMenu.SetActive(false);
					currentMenu = crowdDensityMenu;
					currentMenu.SetActive(true);
				}
				
				else if( Input.GetKeyDown(KeyCode.JoystickButton1) )
				{
					//Return to main menu
					currentMenu.SetActive(false);
					currentMenu = mainMenu;
					currentMenu.SetActive(true);
				}
			}
			
			else if(currentMenu == crowdDensityMenu)
			{
				if(xBoxDPad.Up)
				{
					//Toggle visibility of public paths
				}
				
				else if(xBoxDPad.Down)
				{
					//Toggle visibility of ?
				}
				
				else if(xBoxDPad.Left)
				{
					//Toggle visibility of Parkway tickets
				}
				
				
				else if(xBoxDPad.Right)
				{
					//Toggle visibility of independece mall tickets
				}
				
				else if( Input.GetKeyDown(KeyCode.JoystickButton1) )
				{
					//Return to main menu
					currentMenu.SetActive(false);
					currentMenu = crowdsMenu;
					currentMenu.SetActive(true);
				}
			}
			
		}
		
	}
	#endif
}
