﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CreepyDollAvatarController))]
public class DeadEndKill : MonoBehaviour {

	private GameObject player;
	private CreepyDollAvatarController creepyDoll;
	private bool playerInRange = false;
	private AudioSource killAudio;
	private Death deathScript;

	void Awake()
	{
		player = GameObject.FindGameObjectWithTag("Player");
		creepyDoll = GetComponent<CreepyDollAvatarController>();
		killAudio = GetComponent<AudioSource>();
	}

	void Start()
	{
		creepyDoll.SetMood(CreepyDollAvatarController.Moods.crazy);
		creepyDoll.SetState(CreepyDollAvatarController.States.stand);
		deathScript = GameObject.FindGameObjectWithTag("Timer").GetComponent<Death>();
	}

	void OnTriggerEnter(Collider collider){
		if(collider.gameObject.tag == "Player"){
			playerInRange = true;
		}
	}
	
	void OnTriggerExit(Collider collider){
		if(collider.gameObject.tag == "Player"){
			playerInRange = false;
		}
	}

	// Update is called once per frame
	void Update () {
	
		if(playerInRange && (Input.GetKeyDown(KeyCode.E)))
		{
			creepyDoll.KillPlayer( player);
			StartCoroutine("setDeath");
		}

	}

	IEnumerator setDeath(){
		killAudio.Play();
		yield return new WaitForSeconds(2.5f);
		deathScript.KillPlayer(Application.loadedLevelName);
	}
}
