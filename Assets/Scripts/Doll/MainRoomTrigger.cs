﻿using UnityEngine;
using System.Collections;

public class MainRoomTrigger : MonoBehaviour {

	public DollPositionManager dollState;

	void OnTriggerEnter(Collider collider){
		if(collider.gameObject.tag == "Player"){
			dollState.dollCurrentState = DollPositionManager.PlayerPositionStates.mainRoom;
		}
	}
}
