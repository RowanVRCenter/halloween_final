﻿using UnityEngine;
using System.Collections;

public class DollPositionManager : MonoBehaviour {

	public Transform[] MainRoomPoints;
	public Transform[] tHallwayPoints;
	public Transform[] inclineTunnelPoints;
	public Transform[] waterTunnelPoints;
	public Transform[] deadEndPoints;
	public Transform doll;
	public Transform yellowLight;
	public Transform redLight;
	public GameObject rightSeal;

	private Transform player;
	private int mainRoomIndex = 0;
	private int inclineTunnelIndex = 0;
	private int waterTunnelIndex = 0;
	private bool inclineTunnelset = false;
	private bool waterTunnelset = false;
	private bool isSeen = false;

	public enum PlayerPositionStates{
		mainRoom,
		tHallway,
		inclineTunnel,
		deadEnd,
		waterTunnel
	}

	public PlayerPositionStates dollCurrentState;

	// Use this for initialization
	void Start () {
		dollCurrentState = PlayerPositionStates.mainRoom;
		player = GameObject.FindGameObjectWithTag("Player").transform;
		foreach(Transform point in MainRoomPoints){
			point.gameObject.SetActive(false);
		}
		foreach(Transform point in tHallwayPoints){
			point.gameObject.SetActive(false);
		}
		foreach(Transform point in inclineTunnelPoints){
			point.gameObject.SetActive(false);
		}
		foreach(Transform point in waterTunnelPoints){
			point.gameObject.SetActive(false);
		}
		foreach(Transform point in deadEndPoints){
			point.gameObject.SetActive(false);
		}
		doll.position = MainRoomPoints[0].position;
		doll.rotation = MainRoomPoints[0].rotation;
	}

	// Update is called once per frame
	void Update () {

		switch(dollCurrentState){
		case PlayerPositionStates.mainRoom:
			MainRoom();
			break;
		case PlayerPositionStates.tHallway:
			THallway();
			break;
		case PlayerPositionStates.inclineTunnel:
			InclineTunnel();
			break;
		case PlayerPositionStates.waterTunnel:
			WaterTunnel();
			break;
		case PlayerPositionStates.deadEnd:
			break;
		}
	}

	void MainRoom(){
		Vector3 dollDistance = Vector3.Normalize(doll.position - player.position);
		float dot = Vector3.Dot(player.forward,dollDistance);
		//float dotRed = Vector3.Dot(redLight.forward,dollDistance);

		if(dot < 0.05f && isSeen){
			
			if(mainRoomIndex < MainRoomPoints.Length - 1){
				mainRoomIndex++;
			}else{
				mainRoomIndex = 0;
			}
			
			doll.position = MainRoomPoints[mainRoomIndex].position;
			doll.rotation = MainRoomPoints[mainRoomIndex].rotation;
			isSeen = false;
		}
		
		if(dot > 0.9f){
			isSeen = true;
		}
	}

	void THallway(){
		doll.position = tHallwayPoints[0].position;
		doll.rotation = tHallwayPoints[0].rotation;
	}

	void InclineTunnel(){

		if(!inclineTunnelset){
			doll.position = inclineTunnelPoints[0].position;
			doll.rotation = inclineTunnelPoints[0].rotation;
			inclineTunnelset = true;
		}

		Vector3 dollDistance = Vector3.Normalize(doll.position - player.position);
		float dot = Vector3.Dot(player.forward,dollDistance);
		//float dotRed = Vector3.Dot(redLight.forward,dollDistance);

		if(dot < 0.05f && isSeen){
			
			if(inclineTunnelIndex < inclineTunnelPoints.Length - 1){
				inclineTunnelIndex++;
			}else{
				inclineTunnelIndex = 0;
			}

			doll.position = inclineTunnelPoints[inclineTunnelIndex].position;
			doll.rotation = inclineTunnelPoints[inclineTunnelIndex].rotation;

			isSeen = false;
		}
		
		if(dot > 0.9f){
			isSeen = true;
		}
	}

	void WaterTunnel(){
		if(!waterTunnelset){
			doll.position = waterTunnelPoints[0].position;
			doll.rotation = waterTunnelPoints[0].rotation;
			waterTunnelset = true;
		}
		
		Vector3 dollDistance = Vector3.Normalize(doll.position - player.position);
		float dot = Vector3.Dot(player.forward,dollDistance);
		//float dotRed = Vector3.Dot(redLight.forward,dollDistance);
		
		if(dot < 0.05f && isSeen){
			
			if(waterTunnelIndex < waterTunnelPoints.Length - 1){
				waterTunnelIndex++;
			}else{
				waterTunnelIndex = 0;
			}
			
			doll.position = waterTunnelPoints[waterTunnelIndex].position;
			doll.rotation = waterTunnelPoints[waterTunnelIndex].rotation;
			
			isSeen = false;
		}
		
		if(dot > 0.9f){
			isSeen = true;
		}
	}
}
