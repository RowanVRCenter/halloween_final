﻿using UnityEngine;
using System.Collections;

public class THallwayTrigger : MonoBehaviour {

	public DollPositionManager dollState;
	
	void OnTriggerEnter(Collider collider){
		if(collider.gameObject.tag == "Player"){
			dollState.dollCurrentState = DollPositionManager.PlayerPositionStates.tHallway;
		}
	}
}
