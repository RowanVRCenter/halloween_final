﻿using UnityEngine;
using System.Collections;

public class DollPickupKill : MonoBehaviour {

	public GameObject killerDoll;
	private CreepyDollAvatarController creepyDoll;
	private bool playerInRange = false;
	private Transform player;
	private GameObject doll;
	private AudioSource killAudio;
	private Death deathScript;
	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag("Player").transform;
		creepyDoll = killerDoll.GetComponent<CreepyDollAvatarController>();
		creepyDoll.SetMood(CreepyDollAvatarController.Moods.crazy);
		killAudio = GetComponent<AudioSource>();
		//deathScript = GameObject.FindGameObjectWithTag("Timer").GetComponent<Death>();
	}

	void OnTriggerEnter(Collider collider){
		if(collider.tag == "Player"){
			playerInRange = true;
		}
	}

	void OnTriggerExit(Collider collider){
		if(collider.tag == "Player"){
			playerInRange = false;
		}
	}

	// Update is called once per frame
	void Update () {
		if(playerInRange && (Input.GetKeyDown(KeyCode.E)))
		{
			gameObject.GetComponent<MeshRenderer>().enabled = false;
			creepyDoll.KillPlayer( player.gameObject);
			StartCoroutine(setDeath());
		}
	}

	IEnumerator setDeath(){
		killAudio.Play();
		yield return new WaitForSeconds(2.5f);
		//deathScript.KillPlayer(Application.loadedLevelName);
	}
}
