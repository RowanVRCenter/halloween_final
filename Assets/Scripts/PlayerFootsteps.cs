﻿using UnityEngine;
using System.Collections;

public class PlayerFootsteps : MonoBehaviour {

	private AudioSource footsteps;
	public AudioClip[] footstepSound;

	public float stepDist;
	private float dist;
	private Vector3 lastPos;

	// Use this for initialization
	void Start () {
		footsteps = GetComponent<AudioSource> ();
		lastPos = transform.position;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		dist += Vector3.Distance (transform.position, lastPos);
		if (dist > stepDist) {
			footsteps.PlayOneShot (footstepSound [Random.Range (1, footstepSound.Length)]);
			dist = 0;
		}
		lastPos = transform.position;
	}
}
