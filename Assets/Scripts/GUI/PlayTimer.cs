﻿using UnityEngine;
using System.Collections;

public class PlayTimer : MonoBehaviour {

	[HideInInspector]
	public string GameTimeElapsed;
	public int numOfPlayers;
	[HideInInspector]
	public float GameTimer;

	float mins = 0f;
	float secs = 0f;

	void Awake() {
		DontDestroyOnLoad(transform.gameObject);
		GameTimer = 30f * 60f;
	}

	void Update(){
		GameTimer -= Time.deltaTime;
		
		mins = Mathf.Floor(GameTimer/60f);
		secs = GameTimer%60f;
		
		GameTimeElapsed = mins.ToString() + " : " + secs.ToString("#.00");
	}

	void OnGUI(){
			GUI.Label(new Rect(Screen.width*.8f,10f,100f,20f),GameTimeElapsed);
			GUI.Label(new Rect(Screen.width*.8f,40f,100f,20f),GameTimer.ToString());
			GUI.Label(new Rect(Screen.width*.6f,10f,300f,20f),"Number of Players: "+numOfPlayers.ToString());
	}
}
