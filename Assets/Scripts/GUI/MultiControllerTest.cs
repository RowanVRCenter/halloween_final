﻿using UnityEngine;
using System.Collections;

public class MultiControllerTest : MonoBehaviour {

	private bool pressed1;
	private bool pressed2;
	private bool pressed3;
	private bool pressed4;

	void Start(){
		for(int i = 0; i < Input.GetJoystickNames().Length; i++){
			Debug.Log(Input.GetJoystickNames()[i]);

		}
	}

	// Update is called once per frame
	void Update () {
		if(Input.GetButton("XBox360Controller1_B")){
			Debug.Log("Controller 1 B Pressed");
			pressed1 = true;
		}else{
			pressed1 = false;
		}
		if(Input.GetButton("XBox360Controller2_B")){
			Debug.Log("Controller 2 B Pressed");
			pressed2 = true;
		}else{
			pressed2 = false;
		}
		if(Input.GetButton("XBox360Controller3_B")){
			Debug.Log("Controller 3 B Pressed");
			pressed3 = true;
		}else{
			pressed3 = false;
		}
		if(Input.GetButton("XBox360Controller4_B")){
			Debug.Log("Controller 4 B Pressed");
			pressed4 = true;
		}else{
			pressed4 = false;
		}

	}

	void OnGUI(){
		GUI.Label(new Rect(10f,10f,100f,30f),"Controller 1");
		if(pressed1){
			GUI.Label(new Rect(110f,10f,100f,30f),"Being Pressed");
		}
		GUI.Label(new Rect(10f,40f,100f,30f),"Controller 2");
		if(pressed2){
			GUI.Label(new Rect(110f,40f,100f,30f),"Being Pressed");
		}
		GUI.Label(new Rect(10f,70f,100f,30f),"Controller 3");
		if(pressed3){
			GUI.Label(new Rect(110f,70f,100f,30f),"Being Pressed");
		}
		GUI.Label(new Rect(10f,100f,100f,30f),"Controller 4");
		if(pressed4){
			GUI.Label(new Rect(110f,100f,100f,30f),"Being Pressed");
		}

	}
}
