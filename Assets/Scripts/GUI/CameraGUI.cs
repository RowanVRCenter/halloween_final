﻿using UnityEngine;
using System.Collections;

public class CameraGUI : MonoBehaviour {

	public GameObject Timer;
	private float mins;
	private float secs;
	private GameObject timer;
	private GameObject player;
	private Vector3[] playerPositions;
	private Quaternion[] playerRotations;
	private Vector3[] playerScales;
	private string[] playerNames;
	// Use this for initialization
	void Awake () {
		timer = GameObject.FindWithTag("Timer");
		if(timer == null){
			Instantiate(Timer,new Vector3(0f,0f,0f),Quaternion.identity);
			timer = GameObject.FindWithTag("Timer");
		}
	}

	void Start(){
		player = GameObject.FindGameObjectWithTag("Player");
	}

	void OnGUI(){
			if(GUI.Button(new Rect(10f,10f,150f,30f),"Load Level " + (Application.loadedLevel + 1).ToString())){
				if(Application.loadedLevel == 0){
					timer.GetComponent<PlayTimer>().numOfPlayers = GameObject.FindObjectOfType<Level0Manager>().numberOfPlayers;
				}
				nextLevelRPC(Application.loadedLevel+1);
			}
			if(GUI.Button(new Rect(10f,50f,150f,30f),"Reload Level")){
				ReloadLevelRPC(Application.loadedLevel);
			}

	}
	

	void nextLevelRPC(int scene){
		Application.LoadLevel(scene);
	}

	void ReloadLevelRPC(int scene){
		Application.LoadLevel(scene);
	}
	

}
