﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class MonsterSpawner : MonoBehaviour {

	public bool testBool = false;

	public GameObject skinnyMonster;
	public GameObject amulet;
	public Transform target;
	public AmbientSound ambience;

	private Transform[] spawnPoints;
	private int totalSpawn;
	private bool spawned;

	// Use this for initialization
	void Start () {
		spawnPoints = this.GetComponentsInChildren<Transform> ();	
		//Debug.Log (spawnPoints.Length);
	}
	
	// Update is called once per frame
	void Update () {
		if (!spawned) {
			if (amulet.GetComponent<ConfigurableJoint> () == null || testBool) {
				StartCoroutine (SpawnTheDemons ());
				ambience.Enclosing ();
				spawned = true;
			}
			testBool = false;
		}
	
	}

	public IEnumerator SpawnTheDemons() {
		yield return new WaitForSeconds (2f);

		for (int i = 1; i < spawnPoints.Length; i++) {
			GameObject currentMonster = Instantiate (skinnyMonster, spawnPoints [i].position, Quaternion.identity) as GameObject;
			currentMonster.GetComponent<SkinnyMonster> ().setTarget (target);
			currentMonster.GetComponent<NavMeshAgent> ().destination = target.position;
			currentMonster.GetComponent<Animator> ().SetBool ("Lumber", true);
			yield return null;
		}
	}
}
