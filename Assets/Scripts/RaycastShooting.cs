﻿using UnityEngine;
using System.Collections;

public class RaycastShooting : MonoBehaviour
{
    RaycastHit hit;
	private float watchTime;
	public float timeUntilDeath;
	public GameObject glowstick;
	public int rayCastDist;

    void FixedUpdate()
    {
        
        Vector3 fwd = transform.TransformDirection(Vector3.forward); //the direction the player is facing
        Debug.DrawRay(transform.position, fwd*rayCastDist, Color.green);
        if (Physics.Raycast(transform.position, fwd, out hit, rayCastDist)) //fire a beam in front of the object up to 100 units away
        {
            //Debug.Log(hit.collider.tag);    
			if (hit.transform.tag == "Doll") {
				watchTime += Time.deltaTime;
				if (watchTime >= timeUntilDeath) {
					//Debug.Log ("Looking at doll...");
					hit.transform.SendMessage("DollAttack",this.GetComponent<Transform>(), SendMessageOptions.DontRequireReceiver); //Call the method WatchBird in the gameobject that is hit
					watchTime = 0;
				}
			}
			else if (hit.transform.tag == "Eyeball") {
				watchTime += Time.deltaTime;
				if (watchTime >= timeUntilDeath) {
					//Debug.Log ("Looking at doll...");
					hit.transform.SendMessage("EyeActivate",this.GetComponent<Transform>(), SendMessageOptions.DontRequireReceiver); //Call the method WatchBird in the gameobject that is hit
					watchTime = 0;
				}
			}else {
				watchTime = 0;
			}
        }

    }
}