using UnityEngine;
using System.Collections;

public class SealVerticalTube : MonoBehaviour {

	public GameObject rightSeal;
	
	private bool playerInRange = false;
	private float startPosition;
	private Vector3 tmpPosition;
	private Quaternion tmpRotation;
	private CameraShake camShake;
	private GameObject player;
	private Vector3 rightSealStartPoint;
	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag("Player");
		camShake = player.GetComponentInChildren<CameraShake>();
		startPosition = transform.position.x;
		rightSealStartPoint = rightSeal.transform.position;
	}
	
	void OnTriggerEnter(Collider collider){
		if(collider.gameObject.tag == "Player"){
			playerInRange = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(playerInRange){
			CloseDoor();
		}

		if(playerInRange && !GetComponent<AudioSource>().isPlaying){
			StartCoroutine(PlayAudio());
		}
	}
	
	void CloseDoor(){
		//Debug.Log("Run Close");
		if(transform.position.x > (startPosition - 5f)){
			tmpPosition = transform.position;
			tmpPosition.x -= Time.deltaTime*4f;
			transform.position = tmpPosition;
			
			transform.RotateAround(transform.position,new Vector3(0f,0f,1f),Time.deltaTime*100f);

			//camShake.enabled = true;
		}else{
			rightSeal.transform.position = rightSealStartPoint + new Vector3(5f,0f,0f);
			rightSeal.GetComponent<IntersectionRightSeal>().leftSealClosed = true;
			//camShake.enabled = false;
			playerInRange = false;
		}
	}

	IEnumerator PlayAudio(){
		GetComponent<AudioSource>().Play ();
		yield return new WaitForSeconds(GetComponent<AudioSource>().clip.length);
		Destroy(GetComponent<AudioSource>());
	}
}
