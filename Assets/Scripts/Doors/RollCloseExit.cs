using UnityEngine;
using System.Collections;

public class RollCloseExit : MonoBehaviour {

	private bool playerInRange = false;
	private Vector3 tmpPosition;
	private Quaternion tmpRotation;
	private CameraShake camShake;
	private GameObject player;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag("Player");
		//camShake = player.GetComponentInChildren<CameraShake>();
	}

	void OnTriggerEnter(Collider collider){
		if(collider.gameObject.tag == "Player"){
			playerInRange = true;
		}
	}

	// Update is called once per frame
	void Update () {
		if(playerInRange){
			CloseDoor();
		}

		if(playerInRange && !GetComponent<AudioSource>().isPlaying){
			StartCoroutine(PlayAudio());
		}
	}

	void CloseDoor(){
		if(transform.position.z < (-5f)){
			tmpPosition = transform.position;
			tmpPosition.z += Time.deltaTime*4f;
			transform.position = tmpPosition;

			transform.RotateAround(transform.position,new Vector3(1f,0f,0f),Time.deltaTime*20f);
			//camShake.enabled = true;
		}else{
			//camShake.enabled = false;
			playerInRange = false;
		}
	}

	IEnumerator PlayAudio(){
		GetComponent<AudioSource>().Play ();
		yield return new WaitForSeconds(GetComponent<AudioSource>().clip.length);
		Destroy(GetComponent<AudioSource>());
	}
}
