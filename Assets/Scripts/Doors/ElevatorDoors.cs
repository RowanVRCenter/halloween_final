using UnityEngine;
using System.Collections;

public class ElevatorDoors : MonoBehaviour {

	public Transform leftElevatorDoor;
	public Transform rightElevatorDoor;

	// Update is called once per frame
	void Start (){
		Debug.Log("Open Elevator Doors");
		GetComponent<AudioSource>().Play();
	}
	void Update () {

		if(Time.timeSinceLevelLoad > 2.0f){
			if(leftElevatorDoor.rotation.eulerAngles.y > 270f || leftElevatorDoor.rotation.eulerAngles.y < 0f){
				leftElevatorDoor.RotateAround(leftElevatorDoor.position,Vector3.up,-90f*Time.deltaTime/4f);
				rightElevatorDoor.RotateAround(rightElevatorDoor.position,Vector3.up,90f*Time.deltaTime/4f);
			}
			else if ((GetComponent<AudioSource>() != null && !GetComponent<AudioSource>().isPlaying)){
				Destroy(GetComponent<AudioSource>());
			}
		}
	}
}
