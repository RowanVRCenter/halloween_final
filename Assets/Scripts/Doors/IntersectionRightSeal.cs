using UnityEngine;
using System.Collections;

public class IntersectionRightSeal : MonoBehaviour {

	public bool leftSealClosed = false;

	private bool playerInRange = false;
	private float startPosition;
	private Vector3 tmpPosition;
	private Quaternion tmpRotation;
	//private CameraShake camShake;
	private GameObject player;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag("Player");
		//camShake = player.GetComponentInChildren<CameraShake>();
		startPosition = transform.position.x;
	}
	
	void OnTriggerEnter(Collider collider){
		if(collider.gameObject.tag == "Player"){
			playerInRange = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(playerInRange && leftSealClosed){
			CloseDoor();
		}
		
		if(playerInRange && !GetComponent<AudioSource>().isPlaying){
			Debug.Log("Close Right Seal");
			StartCoroutine(PlayAudio());
		}
	}

	IEnumerator PlayAudio(){
		GetComponent<AudioSource>().Play ();
		yield return new WaitForSeconds(GetComponent<AudioSource>().clip.length);
		Destroy(GetComponent<AudioSource>());
	}
	
	void CloseDoor(){
		if(transform.position.x > (startPosition)){
			tmpPosition = transform.position;
			tmpPosition.x -= Time.deltaTime*4f;
			transform.position = tmpPosition;
			
			transform.RotateAround(transform.position,new Vector3(0f,0f,1f),Time.deltaTime*100f);
			
			//camShake.enabled = true;
		}else{
			//camShake.enabled = false;
			playerInRange = false;
		}
	}
}
