using UnityEngine;
using System.Collections;

public class SecretDoorOpen : MonoBehaviour {

	private GameObject player;
	public Transform door;
	[HideInInspector]
	public bool playerHasKey = false;
	private float doorHeight;
	private Vector3 tmpPosition;
	private float doorStart;
	//private CameraShake camShake;
	private bool doorsOpening = false;


	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag("Player");
		//camShake = player.GetComponentInChildren<CameraShake>();
		doorHeight = door.gameObject.GetComponent<MeshFilter>().sharedMesh.bounds.size.y;
		doorStart = transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {

		if(playerHasKey && GetComponent<AudioSource>() != null && !GetComponent<AudioSource>().isPlaying){
			StartCoroutine(PlayAudio());
			doorsOpening = true;
			Debug.Log ("open secret door");
		}

		if(doorsOpening){
			OpenDoor();
		}


	}

	void OpenDoor(){
		if(transform.position.y < (doorStart + (doorHeight*.85f))){
			doorsOpening = true;
			tmpPosition = transform.position;
			tmpPosition.y += Time.deltaTime*0.9f;
			transform.position = tmpPosition;

			//camShake.enabled = true;
		}else{
			//camShake.enabled = false;
			doorsOpening = false;
		}
	}

	IEnumerator PlayAudio(){
		GetComponent<AudioSource>().Play ();
		yield return new WaitForSeconds(GetComponent<AudioSource>().clip.length);
		Destroy(GetComponent<AudioSource>());
	}
}
