﻿using UnityEngine;
using System.Collections;

public class TurnValve : MonoBehaviour {

	public GameObject SewerDoor;
	public GameObject valve;
	public BoxCollider doorCollider;
	private Vector3 sewerDoorStart;
	private float pauseTimer;
	private float pauseTimerReset = 1.0f;
	public AudioClip open, close;
	private float doorheight;
	private bool coroutineRunning;



     

	// Use this for initialization
	void Start () {
        doorheight = SewerDoor.GetComponent<MeshRenderer>().bounds.size.y;
		pauseTimer = pauseTimerReset;
		sewerDoorStart = SewerDoor.transform.position;
	}
	
	void Update () {

        if (!coroutineRunning)
        {
			coroutineRunning = true;
			StartCoroutine(doorRoutine());
		}
	}

	IEnumerator doorRoutine(){
		GetComponent<AudioSource>().clip = open;
		GetComponent<AudioSource>().Play();

		while(SewerDoor.transform.position.y < sewerDoorStart.y+doorheight){
			valve.transform.RotateAround(valve.transform.position,valve.transform.up,100f*Time.deltaTime);
			SewerDoor.transform.position += Vector3.up*Time.deltaTime*0.5f;
			if ((SewerDoor.transform.position.y > sewerDoorStart.y+(doorheight*0.75f))&&!doorCollider.isTrigger){
				doorCollider.isTrigger = true;
			}
			yield return null;
		}
		GetComponent<AudioSource>().Stop();
		yield return new WaitForSeconds(0.5f);
		GetComponent<AudioSource>().clip = close;

		while(SewerDoor.transform.position.y > sewerDoorStart.y){
			if (!GetComponent<AudioSource>().isPlaying)GetComponent<AudioSource>().Play();
				if(doorCollider.isTrigger)doorCollider.isTrigger = false;
				valve.transform.RotateAround(valve.transform.position,valve.transform.up,-100f*Time.deltaTime);
				SewerDoor.transform.position -= Vector3.up*Time.deltaTime*0.2f;
			yield return null;
		}
		GetComponent<AudioSource>().Stop ();
		coroutineRunning = false;
	}
}
