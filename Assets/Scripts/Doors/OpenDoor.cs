﻿using UnityEngine;
using System.Collections;

public class OpenDoor : MonoBehaviour {

	public Transform leftDoor;
	public Transform rightDoor;
	public GameObject doorLock;
	public AudioSource unlockAudio;
	public Transform unlockPos;

	[HideInInspector]
	public bool openDoor = false;

	private bool runDoorOpen = false;
	private float leftDoorStart;
	private float leftDoorSize;
	private Mesh leftDoorMesh;
	private GameObject key;
	private Mesh rightDoorMesh;
	private Vector3 tmpPosition;
	private float startTime;
	private bool keyInRange = false;
	public AudioClip lockedDoorClip, openDoorClip;

	// Use this for initialization
	void Start () {
		unlockAudio.clip = lockedDoorClip;
		leftDoorStart = leftDoor.position.z;
		leftDoorSize = leftDoor.GetComponent<MeshFilter>().sharedMesh.bounds.size.x;
	}

	void OnTriggerStay(Collider collider){
		if (collider.tag == "Floor1Key") {
			//Debug.Log ("Key Enter Lock     ~oG Kane");
			keyInRange = true;
			key = collider.gameObject;
		} else {
			keyInRange = false;
		}
	}

	// Update is called once per frame
	void Update () {
		if(keyInRange && !runDoorOpen){
			runDoorOpen = true;
			TurnKey ();
			startTime = Time.time+2;
			unlockAudio.clip = openDoorClip;
			Debug.Log("Open Big Doors");
		}

	}

	void TurnKey()
	{
		if(key.GetComponent<Joint>() != null)
		{
			Destroy (key.GetComponent<FixedJoint> ());
		}
		key.GetComponent<Rigidbody> ().isKinematic = true;
		key.layer = 0;
		StartCoroutine (KeyAnimation ());
	}

	IEnumerator KeyAnimation()
	{
		float i = 8;
		while (i > 6) {
			key.transform.rotation = Quaternion.Lerp (key.transform.rotation, Quaternion.Euler (-90f, 270f, 0), 4*Time.deltaTime);
			key.transform.position = Vector3.Lerp (key.transform.position,unlockPos.position, 4*Time.deltaTime);
			i -= Time.deltaTime;
			yield return null;
		}
		while (i > 3 && i < 6) {
			key.transform.position = Vector3.Lerp (key.transform.position, new Vector3 (-2.4f, 1.632f, -12.174f), 2*Time.deltaTime);
			i -= Time.deltaTime;
			yield return null;
		}
		while (i < 3 && i > 0) {
			key.transform.rotation = Quaternion.Lerp (key.transform.rotation, Quaternion.Euler (0, 270f, 0), 2*Time.deltaTime);
			i -= Time.deltaTime;
			if (unlockAudio !=null && !unlockAudio.isPlaying){
				//Key Unlock Animation
				StartCoroutine(PlayAudio());
			}
			yield return null;
		}
		Destroy(doorLock);
		Destroy (key);
		StartCoroutine(OpenDoors());
		yield return null;
	}

	IEnumerator OpenDoors(){
	
		while(leftDoor.position.x > (leftDoorStart - leftDoor.position.x) && Time.time>startTime){
			tmpPosition = leftDoor.position;
			tmpPosition.x -= Time.deltaTime;
			leftDoor.position = tmpPosition;
			
			tmpPosition = rightDoor.position;
			tmpPosition.x += Time.deltaTime;
			rightDoor.position = tmpPosition;
			yield return null;
		}
	}

	IEnumerator PlayAudio(){
		unlockAudio.Play ();
		yield return new WaitForSeconds(unlockAudio.clip.length);
		Destroy(unlockAudio);
	}
}
