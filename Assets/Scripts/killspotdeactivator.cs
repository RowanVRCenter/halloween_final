﻿using UnityEngine;
using System.Collections;

public class killspotdeactivator : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if (Application.loadedLevel != 1){
			gameObject.SetActive(false);
		}
	}
}
