﻿using UnityEngine;
using System.Collections;

public class highTriggerLanding : MonoBehaviour {

	public bool triggered;


	void OnTriggerEnter(Collider collider){
		if (collider.gameObject.tag == "Player"){
			triggered = true;
		}
	}
}