﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SteamVR_TrackedObject))]
public class PickupObject : MonoBehaviour
{
    //Currently uses a parenting technique to grab an object. Can be switched to joints if necessary.

    SteamVR_TrackedObject trackedObj;
    SteamVR_Controller.Device device;
    FixedJoint joint;
    FixedJoint itemJoint;

    public GameObject heldObj;
    
    private Collider currentCol;
    private Rigidbody attachPoint;
	public Rigidbody beltAttachPoint;
	private ConfigurableJoint configJoint;

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
        currentCol = null;
        attachPoint = GetComponent<Rigidbody>();
    }

    void Update()
    {
        device = SteamVR_Controller.Input((int)trackedObj.index);
        //For dropping an object (doesn't have to be interactable as if you have something in your hand, you should def be able to drop it)
		if (device.GetPressUp(SteamVR_Controller.ButtonMask.Trigger) && heldObj != null)
        {
            Destroy(joint);
            joint = null;
            Debug.Log(heldObj.name);
            //heldObj.transform.SetParent(null);
            transform.GetChild(0).gameObject.SetActive(true);
			if (heldObj.GetComponent<InteractableObject>().inBeltRange && heldObj.GetComponent<InteractableObject>().inventoryCapable )
            {
				if (heldObj.tag == "Glowstick" || heldObj.tag == "LightStick") {
					heldObj.GetComponent<Glowstick>().inBelt = true;
				}
				Debug.Log ("Putting in belt ~Saad");
				configJoint = heldObj.gameObject.AddComponent<ConfigurableJoint>();
				configJoint.connectedBody = beltAttachPoint;
				configJoint.axis = new Vector3 (0, 0, 0);
				configJoint.xMotion = ConfigurableJointMotion.Locked;
				configJoint.yMotion = ConfigurableJointMotion.Locked;
				configJoint.zMotion = ConfigurableJointMotion.Locked;
				configJoint.angularXMotion = ConfigurableJointMotion.Free;
				configJoint.angularYMotion = ConfigurableJointMotion.Locked;
				configJoint.angularZMotion = ConfigurableJointMotion.Free;
				configJoint.anchor = new Vector3 (0f, .115f, 0f);
            }
            else
            {
				Debug.Log ("Dropping ~Saad");
				//currentCol.enabled = true;
                tossObject(heldObj.GetComponent<Rigidbody>());
            }
            heldObj = null;
        }
        //For picking up an interactable object
        else if (currentCol != null && device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger) && currentCol.GetComponent<InteractableObject>().grabbable && heldObj == null)
        {
            //transform.GetChild(0).gameObject.SetActive(false);
			if (currentCol.GetComponent<ConfigurableJoint>() != null)
            {
                //currentCol.transform.SetParent(null);
				Destroy(currentCol.GetComponent<ConfigurableJoint>());
            }
            heldObj = currentCol.gameObject;
            //currentCol.attachedRigidbody.isKinematic = true;
			if (currentCol.tag == "Glowstick" || currentCol.tag == "Lightstick") {
				currentCol.transform.rotation = this.transform.rotation * Quaternion.Euler(90, 0, 0);
				currentCol.GetComponent<Glowstick> ().inBelt = false;
				//currentCol.enabled = false;
			}
			if (currentCol.tag == "Floor1Key") {
				currentCol.GetComponent<PickUpKey> ().pickedUp = true;
			}
            currentCol.transform.position = transform.position;
            joint = currentCol.gameObject.AddComponent<FixedJoint>();
            joint.connectedBody = attachPoint;
            //currentCol.gameObject.transform.SetParent(gameObject.transform);
        }
    }

    //length is how long the vibration should go for (seconds)
    //strength is vibration strength from 0-1 (weak to sensual to dangerous)
    IEnumerator LongVibration(float length, float strength)
    {
        for (float i = 0; i < length; i += Time.deltaTime)
        {
            device.TriggerHapticPulse((ushort)Mathf.Lerp(0, 3999, strength));
            yield return null;
        }
    }

    void OnTriggerExit(Collider col)
    {
        currentCol = null;
		//Debug.Log ("Collider null ~Saad");
    }

    void OnTriggerEnter(Collider col)
    {
        currentCol = col;
		//Debug.Log ("Collider set ~Saad");
    }

    void tossObject(Rigidbody rigidBody)
    {

		Debug.Log ("Throwing ~Saad");
        Transform origin = trackedObj.origin ? trackedObj.origin : trackedObj.transform.parent;
        if (origin != null)
        {
            rigidBody.velocity = origin.TransformVector(device.velocity);
            rigidBody.angularVelocity = origin.TransformVector(device.angularVelocity);
        }
        else
        {
            rigidBody.velocity = device.velocity;
            rigidBody.angularVelocity = device.angularVelocity;
        }
        rigidBody.maxAngularVelocity = rigidBody.angularVelocity.magnitude;
    }
}
