﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ManualLoadLevel : MonoBehaviour {

	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            SceneManager.LoadSceneAsync("Level_00_v_01", LoadSceneMode.Single);
        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SceneManager.LoadSceneAsync("Level_01_v_02", LoadSceneMode.Single);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SceneManager.LoadSceneAsync("Level_02_v_02", LoadSceneMode.Single);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            SceneManager.LoadSceneAsync("Level_03_v_01", LoadSceneMode.Single);

        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            SceneManager.LoadSceneAsync("Level_END", LoadSceneMode.Single);

        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            SceneManager.LoadSceneAsync("Level_Death", LoadSceneMode.Single);

        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);

        }
        
    }
}
