﻿using UnityEngine;
using System.Collections;

public class DisableBushing : MonoBehaviour {

    public GameObject lightningBallEffect;
    public LightningBoltScript[] lightning;
    private BushingTriSystem system;
    public bool disabled;
    private Animator socketAnim;
    private SocketLookAt socketAim;
    private AudioSource sound;
    public CapsuleCollider trigger;

    public bool partOfSystem;
    private Vector3 origScale;
    public float scaleTime;
    public float deltaScale;
    public float scaleDownSpeed;

	// Use this for initialization
	void Start () {
        sound = GetComponent<AudioSource>();
        socketAim = GetComponentInChildren<SocketLookAt>();
        socketAnim = GetComponentInChildren<Animator>();
        if (partOfSystem)
        {
            system = GetComponentInParent<BushingTriSystem>();
            lightning = GetComponentsInChildren<LightningBoltScript>();
        }
	}

    void OnCollisionEnter(Collision c)
    {
        if (c.gameObject.tag == "Lightstick" && !disabled)
        {
            if(c.gameObject.GetComponent<Glowstick>().activated)
            {
                sound.Stop();
                socketAim.enabled = false;
                socketAnim.SetTrigger("Close");
                trigger.enabled = false;
                disabled = true;

                if (partOfSystem)
                {
                    system.DisableBushing();
                    lightning[0].gameObject.SetActive(false);
                    lightning[1].gameObject.SetActive(false);
                    StartCoroutine(SpawnLightningEffect(c.gameObject.transform.position));
                }
            }            
        }
    }

    IEnumerator SpawnLightningEffect(Vector3 c)
    {
        GameObject currentLightningBallEffect = Instantiate(lightningBallEffect, c, Quaternion.identity) as GameObject;
        origScale = currentLightningBallEffect.GetComponent<Transform>().localScale;

        for (float i = 0; i < scaleTime; i += Time.deltaTime)
        {
            currentLightningBallEffect.GetComponent<Transform>().localScale = Vector3.Lerp(origScale, origScale * deltaScale, i / scaleTime);
            yield return null;
        }
        for (float i = 0; i < scaleTime / scaleDownSpeed; i += Time.deltaTime)
        {
            currentLightningBallEffect.GetComponent<Transform>().localScale = Vector3.Lerp(origScale * deltaScale, origScale, i / (scaleTime / scaleDownSpeed));
            yield return null;
        }

        Destroy(currentLightningBallEffect.gameObject);

        yield return null;
    }
}
