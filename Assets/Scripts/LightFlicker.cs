﻿using UnityEngine;
using System.Collections;

public class LightFlicker : MonoBehaviour {

	public float delay = 0.1f;
	private float countDownTime;

	// Use this for initialization
	void Start () {
		GetComponent<Light>().enabled = false;
		Random.seed = 0;
		countDownTime = delay;
	}
	
	// Update is called once per frame
	void Update () {

		countDownTime -= Time.deltaTime;

		if(countDownTime <= 0.0f)
		{
			countDownTime = delay;
			float value = Random.value;

			if(value > 0.5f)
			{
				GetComponent<Light>().enabled = true;
			}
			else
			{
				GetComponent<Light>().enabled  = false;
			}
		}
	}
}
