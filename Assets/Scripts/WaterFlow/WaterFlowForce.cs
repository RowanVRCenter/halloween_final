﻿using UnityEngine;
using System.Collections;

public class WaterFlowForce : MonoBehaviour {

	public float flowForce = 0.25f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerStay(Collider collider){
		if(collider.gameObject.tag == "Player"){
//			Rigidbody playerRB = collider.gameObject.GetComponent<Rigidbody>();
//			playerRB.AddForce(new Vector3(flowForce,0,0),ForceMode.VelocityChange);
			CharacterController playerCC = collider.gameObject.GetComponent<CharacterController>();
			playerCC.Move(new Vector3(flowForce*Time.deltaTime,0f,0f));
		}
	}

//	void OnTriggerExit(Collider collider){
//		if(collider.gameObject.tag == "Player"){
//			collider.gameObject.rigidbody.velocity = Vector3.zero;
//		}
//	}
}
