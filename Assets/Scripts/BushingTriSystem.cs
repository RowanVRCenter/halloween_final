﻿using UnityEngine;
using System.Collections;

public class BushingTriSystem : MonoBehaviour
{
    public ShieldMovement shield;
    public GameObject eyeSocket;

    private int bushingsDisabled;

    void Start()
    {
        shield.Initialize(true);
        Debug.Log("Rising");
    }

    public void DisableBushing()
    {
        bushingsDisabled++;
        if (bushingsDisabled > 1)
        {
            shield.MoveBelow();
            eyeSocket.GetComponent<Animator>().SetTrigger("Close");
            eyeSocket.GetComponent<SocketLookAt>().enabled = false;

        }
    }
}
