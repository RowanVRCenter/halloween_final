﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class BossFight : MonoBehaviour {

    public Transform heartShields;
    public Transform bushingParent;
    public Transform bushingPositions;
    public Transform explosionManager;
    public LineRenderer[] lightningStrikes;
    public AmbientSound ambience;
    public float raiseUpTime;
    public GameObject topEye;
    public SuperEyeball MYULTIMATEATTACK;
    public Image black;
    public float aTime = 3;

    public int bushingsDestroyed;
    private Vector3 startPos;
    private bool fightStarted = false;

	// Use this for initialization
	void Start () {
        bushingsDestroyed = 0;
        startPos = bushingParent.position;
        lightningStrikes = bushingParent.gameObject.GetComponentsInChildren<LineRenderer>();

        for(int i = 0; i < lightningStrikes.Length; i++)
        {
            lightningStrikes[i].enabled = false;
            
        }
	}

    public void BushingKilled()
    {
        bushingsDestroyed++;
        if (bushingsDestroyed > 7)
        {
            ambience.StopAllSound();
            EndScene();
        }
    }

    void EndScene()
    {
        topEye.transform.SetParent(null);
        topEye.GetComponent<Rigidbody>().isKinematic = false;
        topEye.GetComponent<Rigidbody>().AddTorque(-1*transform.up * 5);
        explosionManager.SendMessage("SpawnExplosions", SendMessageOptions.DontRequireReceiver);
        StartCoroutine(FadeIn());
    }
    
    void OnTriggerEnter(Collider c)
    {
        if(c.tag == "Player" && !fightStarted)
        {
            MYULTIMATEATTACK.enabled = true;
            heartShields.SendMessage("StartFight", SendMessageOptions.DontRequireReceiver);
            ambience.Enclosing();
            StartCoroutine(RaiseBushingParent());
            fightStarted = true;
        }
    }

    IEnumerator RaiseBushingParent()
    {
        for(float j = 0; j < raiseUpTime; j += Time.deltaTime)
        {
            bushingParent.position = Vector3.Lerp(startPos, bushingPositions.position, j / raiseUpTime);
            yield return null;
        }
        StartLightning();
    }

    void StartLightning()
    {
        for (int i = 0; i < lightningStrikes.Length; i++)
        {
            //if(lightningStrikes[i].gameObject.transform.parent.parent == this.GetComponent<Transform>())
            //{
                lightningStrikes[i].enabled = true;
            //}
        }
    }

    IEnumerator FadeIn()
    {
        float r = black.color.r;
        float g = black.color.g;
        float b = black.color.b;
        float i = 0;

        while (i < aTime)
        {
            Color newColor = new Color(r, g, b, Mathf.Lerp(0, 1, i / aTime));
            black.color = newColor;
            i += Time.deltaTime;
            yield return null;
        }
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1, LoadSceneMode.Single);
    }
}
