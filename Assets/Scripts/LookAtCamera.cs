﻿using UnityEngine;
using System.Collections;

public class LookAtCamera : MonoBehaviour {

    private Vector3 zeroedPos;

    public Camera cam;
    public bool lockYPos;

	
	// Update is called once per frame
	void Update () {
        if (!lockYPos)
            transform.LookAt(cam.transform.position);
        else
        {
            zeroedPos = new Vector3(cam.transform.position.x, this.transform.position.y, cam.transform.position.z);
            transform.LookAt(zeroedPos);
        }
	}
}
