﻿using UnityEngine;
using System.Collections;

public class ViveFlashlight : MonoBehaviour
{
    //References
    public Light flashlight;
    
    //Public variables
    public bool testBlink = false;

    //Private variables
    private bool blinking = false;
    private float batteryLife;
    private bool batteryDead;

    void Start()
    {
        batteryLife = 100000000;
    }
    

    void Update()
    {
        if(flashlight.isActiveAndEnabled && !batteryDead)
        {
            batteryLife -= Time.deltaTime;
            if(batteryLife <= 0)
            {
                batteryDead = true;
                LightDying();
            }
        }

        
        //Scaffolding below (just for update)

        if(testBlink)
        {
            StartCoroutine(LightFlicker(true));
            testBlink = false;
        }
    }

    public void LightDying()
    //When flashlight batteries die, make the light flicker and turn off
    {
        if (!flashlight.isActiveAndEnabled) //check if light is even running first
        {
            return;
        }
        else
        {
            StartCoroutine(LightFlicker(true));
        }
    }

    IEnumerator LightFlicker(bool toggle)
    //Flashes the light in a fading scheme with the ability to leave the flashlight in the opposite state that it started in using the toggle bool
    //toggle = true -> inverses original state
    //toggle = false -> keeps original state
    {
        blinking = true;
        for (float i = .1f; i < .5f; i *= 2)
        {
            flashlight.enabled = !flashlight.isActiveAndEnabled;
            yield return new WaitForSeconds(i);
            flashlight.enabled = !flashlight.isActiveAndEnabled;
            yield return new WaitForSeconds(.5f-i);
        }
        if (toggle)
            flashlight.enabled = !flashlight.isActiveAndEnabled;
        blinking = false;
    }//End of FadingBlink
}