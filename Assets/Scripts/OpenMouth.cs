﻿using UnityEngine;
using System.Collections;

public class OpenMouth : MonoBehaviour {

	public GameObject fleshMouthEntrance;
	Vector3 newposition;
	Vector3 startPosition;

	void Start(){
		startPosition = fleshMouthEntrance.transform.position + new Vector3(3,0,0);
		fleshMouthEntrance.transform.position = startPosition;
	}


	void OnTriggerEnter( Collider other)
	{
		StartCoroutine(MoveForward());
	}

	IEnumerator MoveForward(){
		if (!fleshMouthEntrance.GetComponent<AudioSource>().isPlaying)fleshMouthEntrance.GetComponent<AudioSource>().Play ();
		while (fleshMouthEntrance.transform.position.x>(startPosition.x-3f)){
			newposition = fleshMouthEntrance.transform.position + new Vector3(-Time.deltaTime/3f,0,0);
			fleshMouthEntrance.transform.position = newposition;
			yield return null;
		}
	}
}
