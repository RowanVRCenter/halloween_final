﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LightFizzle : MonoBehaviour {

	public AudioSource generator;
	public List<Light> lights;

	protected bool triggered = false;

	protected IEnumerator FlickerLights()
	{



		for( int i = 0; i < 5; i++ )
		{
			generator.GetComponent<AudioSource>().volume -= 0.2f;
			yield return new WaitForSeconds(0.1f);		}

		generator.GetComponent<AudioSource>().Stop ();


		Random.seed=0;
		for( int i = 0; i < lights.Count; i++ )
		{
			lights[i].intensity = Random.value;
		}

		yield return new WaitForSeconds(0.1f);

		for( int i = 0; i < lights.Count; i++ )
		{
			lights[i].intensity = Random.value;

		}

		yield return new WaitForSeconds(0.1f);

		for( int i = 0; i < lights.Count; i++ )
		{
			lights[i].intensity = Random.value;

		}

		yield return new WaitForSeconds(0.1f);

		for( int i = 0; i < lights.Count; i++ )
		{
			lights[i].intensity = 0.0f;
			yield return new WaitForSeconds(0.2f);
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if(!triggered)
		{
			triggered = true;
			StartCoroutine("FlickerLights");
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
