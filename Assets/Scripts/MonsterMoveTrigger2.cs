﻿using UnityEngine;
using System.Collections;

public class MonsterMoveTrigger2 : MonoBehaviour {

	public bool moveToNextSpot = false;

	void OnTriggerEnter(Collider collider)
	{
		Debug.Log("Run Trigger");
		if(collider.tag == "Player")
		{
			moveToNextSpot = true;
		}
	}
}
