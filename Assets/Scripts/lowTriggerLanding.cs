using UnityEngine;
using System.Collections;

public class lowTriggerLanding : MonoBehaviour {

	private highTriggerLanding highTrigger;

	// Use this for initialization
	void Start () {
		highTrigger = gameObject.transform.parent.GetComponentInChildren<highTriggerLanding>();
	}
	
	void OnTriggerEnter(Collider collider){
		if (collider.gameObject.tag == "Player" && highTrigger.triggered){
			StartCoroutine(PlayAudio());
		}
	}

	IEnumerator PlayAudio(){
		GetComponent<AudioSource>().Play();
		highTrigger.triggered = false;
		yield return new WaitForSeconds(GetComponent<AudioSource>().clip.length);
		Destroy(GetComponent<AudioSource>());
	}
}
