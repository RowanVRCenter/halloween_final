﻿using UnityEngine;
using System.Collections;

public class HeartCollision : MonoBehaviour {

    public Rigidbody stem1;
    public Rigidbody stem2;
    public Rigidbody heart;

	// Use this for initialization
	void Start () {
	
	}
	
	void OnCollisionEnter(Collision c)
    {
        if (c.gameObject.tag == "Eyeball")
        {
            stem1.isKinematic = false;
            stem2.isKinematic = false;
            heart.isKinematic = false;
        }
    }
}
