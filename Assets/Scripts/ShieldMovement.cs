﻿using UnityEngine;
using System.Collections;

public class ShieldMovement : MonoBehaviour {

	public Transform upT;
	public Transform downT;
	public float actionTime = 3.5f; //Time to complete movement in seconds
	public bool isUp;
	public bool isDown;
    public bool collisionDestroy = false;

    private AudioSource sound;
    public bool test;

	// Use this for initialization
	void Start () {
        sound = this.GetComponent<AudioSource>();
        Debug.Log(sound);
	}

    void Update()
    {
        if (test)
        {
            MoveBelow();
            test = false;
        }
    }

	public void Initialize(bool Up)
	{
		if (Up) {
			transform.position = upT.position;
			transform.rotation = upT.rotation;
			isUp = true;
			isDown = false;
		} else {
			transform.position = downT.position;
			transform.rotation = downT.rotation;
			isDown = true;
			isUp = false;
		}
	}

	public void MoveBelow()
	{
		isUp = false;
		StartCoroutine (Descend ());
	}

	IEnumerator Descend()
	{
        sound.Play();
		for (float i = 0; i < actionTime; i += Time.deltaTime) {
			transform.position = Vector3.Lerp (upT.position, downT.position, i / actionTime);
			transform.rotation = Quaternion.Lerp (upT.rotation, downT.rotation, i / actionTime);
			yield return null;
		}
		isDown = true;
	}

	public void MoveAbove()
	{
		isDown = false;
		StartCoroutine (Ascend ());
	}

	IEnumerator Ascend()
	{
        sound.Play();
		for (float i = 0; i < actionTime; i += Time.deltaTime) {
			transform.position = Vector3.Lerp (downT.position, upT.position, i / actionTime);
			transform.rotation = Quaternion.Lerp (downT.rotation, upT.rotation, i / actionTime);
			yield return null;
		}
		isUp = true;
	}

	void OnCollisionEnter(Collision c)
	{
        if (collisionDestroy && c.gameObject.tag == "Lightstick")
		    MoveBelow ();
	}
}
