﻿using UnityEngine;
using System.Collections;

public class HeartBeatLight : MonoBehaviour {

	public Light heartGlow;
	public float pulseRate = 1.05f;
	private bool fade = true;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if(heartGlow.intensity <= 1.5f){
			fade = false;
		}else if(heartGlow.intensity >= 2f){
			fade = true;
		}

		if(fade){
			heartGlow.intensity -= Time.deltaTime*pulseRate;
		}else{
			heartGlow.intensity += Time.deltaTime*pulseRate;
		}
	}

}
