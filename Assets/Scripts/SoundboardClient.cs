﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof (NetworkView))]
public class SoundboardClient : MonoBehaviour {
	private int clips = 0;
	private List<string> clipnames = new List<string>();
	private int wide;
	private int high;
	private int buttonwidth;
	private int buttonheight;
	private string ip = "192.168.11.12";
	bool connect = false;

	void StartNetworking () {
		Network.Connect(ip, 38759, "TheRite");
	}
	
	void OnConnectedToServer(){
		connect = true;
	}
	
	void OnDisconnectedFromServer(){
		connect = false;
	}
	
	void Update () {
	}
	
	void OnGUI (){
        if (connect)
        {
            //				clips = clipnames.Count;
            //				if (clips > 0){
            //					wide = (int)Mathf.Sqrt(clips);
            //					high = (clips+wide-1)/wide;
            //					buttonwidth = (Screen.width / wide) - 10;
            //					buttonheight = (Screen.height / (2*high)) - 10;
            //					int x = 0;
            //					int y = 0;
            //					for(int i = 0;i<clips;i++){
            //						if (x < wide){
            //							if (GUI.Button(new Rect((x*(buttonwidth+5))+5,(y*(buttonheight+5))+5+Screen.height/2,buttonwidth,buttonheight),clipnames[i]))networkView.RPC("PlaySound",RPCMode.Server,i);
            //							x++;
            //						}
            //						else{
            //							y++;
            //							x=0;
            //							if (GUI.Button(new Rect((x*(buttonwidth+5))+5,(y*(buttonheight+5))+5+Screen.height/2,buttonwidth,buttonheight),clipnames[i]))networkView.RPC("PlaySound",RPCMode.Server,i);
            //							x++;
            //						}
            //					}
            //				}
        }
        else
        {
            GUI.Label(new Rect((Screen.width / 2) - 50, Screen.height / 2 - 20, 150, 30), "Enter Server ip");
            ip = GUI.TextField(new Rect((Screen.width / 2) - 50, Screen.height / 2, 150, 20), ip);
            if (GUI.Button(new Rect((Screen.width / 2) - 50, Screen.height / 2 + 20, 150, 20), "Connect"))
            {
                StartNetworking();
            }
        }
	}
	[RPC]
	void NameClips(int size, int i, string name){
		if (clipnames.Count < size){
			clipnames.Add("");
			clipnames[i] = name;
		}
		else if (clipnames.Count > size){
			GetComponent<NetworkView>().RPC ("ResendNames",RPCMode.Server);
			clipnames = new List<string>();
		}
		else{
			clipnames[i] = name;
		}
	}
	
	[RPC]
	void PlaySound(int i){}
	
	[RPC]
	void ResendNames(){}
	
}
