﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoachSpawner : MonoBehaviour {


	public GameObject roachPrefab;
	public float timeToSpawn = 5f;
	public int maxRoaches = 10;
	
	protected int currentRoach = 0;
	protected List<GameObject> roaches;

	protected float roachSpawnTime = 0.0f;
	
	void Awake()
	{
		roaches = new List<GameObject> ();

		for( int i = 0; i < maxRoaches; i++)
		{
			GameObject roach = (GameObject)Instantiate(roachPrefab);
			InitializeRoach(roach);
			roach.SetActive(false);
			roaches.Add(roach);
		}
	}

	void InitializeRoach(GameObject roach)
	{
		roach.SetActive(true);
		roach.transform.parent = this.transform;
		roach.transform.localPosition = Vector3.zero;
		roach.transform.localRotation = new Quaternion ();
		roach.GetComponent<Roach> ().SetNewDirection();
	}

	GameObject GetNextRoach()
	{
		currentRoach++;
		if (currentRoach >= roaches.Count)
		{
			currentRoach = 0;
		}
		return roaches [currentRoach];
	}

	// Update is called once per frame
	void Update () {
		roachSpawnTime += Time.deltaTime;

		if(roachSpawnTime >= timeToSpawn)
		{
			roachSpawnTime = 0.0f;
			InitializeRoach( GetNextRoach());
		}
	}
}
