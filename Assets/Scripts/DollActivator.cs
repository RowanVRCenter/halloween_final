﻿using UnityEngine;
using System.Collections;

public class DollActivator : MonoBehaviour {

	public ScaryClothDoll[] dolls;

	// Use this for initialization
	void Start () {
	
	}

	void OnTriggerEnter(Collider c)
	{
		if (c.tag == "Player") {
			for (int i = 0; i < dolls.Length; i++) {
				dolls [i].DollAttack (c.transform);
			}
		}
	}
}
