﻿using UnityEngine;
using System.Collections;

public class TentacleInteraction : MonoBehaviour {

	public TentacleDeath[] tentacles;
	public Transform amuletPos;
	public GameObject explosionPrefab;
	public BoxCollider playerWall;
	public FleshEntrance fleshScript;

	private GameObject explosionEffect;
	public Explosion explosionScript;
	private GameObject amulet;
	private Light light;
	private bool killing;

	// Use this for initialization
	void Start () {
		
	}
	
	void OnTriggerEnter(Collider c)
	{
		if (c.tag == "Key") {
			if (!killing) {
				playerWall.enabled = false;
				amulet = c.gameObject;
				c.attachedRigidbody.isKinematic = true;
				light = amulet.GetComponentInChildren<Light> ();
				amulet.layer = 0;
				StartCoroutine (KillTentacles ());
			}
		}
	}

	IEnumerator KillTentacles()
	{
		killing = true;
		float i = 8;
		while (i > 6) {
			amulet.transform.position = Vector3.Lerp (amulet.transform.position,amuletPos.position, (8-i)/2);
			i -= Time.deltaTime;
			yield return null;
		}
		while (i < 6 && i > 1) {
			light.intensity = 4 * Mathf.Cos (3*i-3) + 4;
			i -= Time.deltaTime;
			yield return null;
		}
		while (i < 1 && i > 0) {
			light.range += 10*Time.deltaTime;
			i -= Time.deltaTime;
			yield return null;
		}
		explosionEffect = Instantiate (explosionPrefab, amulet.transform.position, Quaternion.identity) as GameObject;
		yield return null;
		explosionEffect.transform.GetComponent<Explosion> ().Explode ();

		yield return new WaitForSeconds (1.7f);
		Destroy (amulet);
		for (int j = 0; j < tentacles.Length; j++)
		{
			tentacles [j].MakeRagdoll ();
			yield return null;
		}
		fleshScript.OpenMouth ();
		GetComponent<AudioSource> ().Stop ();
		killing = false;


	}
}
