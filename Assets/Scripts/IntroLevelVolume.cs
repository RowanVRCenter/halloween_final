﻿using UnityEngine;
using System.Collections;

public class IntroLevelVolume : MonoBehaviour {

	private float volume;
	public float volumeStart;
	public float volumeEnd;
	public AudioSource audio1;

	// Use this for initialization
	void Start () {
		volume = volumeStart;
		audio1.volume = volumeStart;
	}
	
	// Update is called once per frame
	void Update () {
		if ((Time.timeSinceLevelLoad > 35)&&(volume<=volumeEnd)){
			volume += (Time.deltaTime*volumeEnd/3f);
			audio1.volume = volume;
		}
	}
}
