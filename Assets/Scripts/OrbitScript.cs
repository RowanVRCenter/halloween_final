﻿using UnityEngine;
using System.Collections;

public class OrbitScript : MonoBehaviour {

	public Transform rotatePoint;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.RotateAround(rotatePoint.position,Vector3.up,1f);
	}
}
