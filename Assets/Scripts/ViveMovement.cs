﻿using UnityEngine;
using System.Collections;

public class ViveMovement : MonoBehaviour {

	//Reference to the tracked object AKA controller
	private SteamVR_TrackedObject trackedObj;
	private Vector2 axisNumber;
	private SteamVR_Controller.Device device;

	public CharacterController body;
	public float moveSpeed;
	public Transform head;

	//Tracking button presses
	public bool triggerPressed = false;
	public bool canMove = true;

	//Simulating Gravity
	private float gravity;

	public bool developerMode = false;
	public GameObject glowStick;


	// Use this for initialization
	void Start () {
		trackedObj = this.GetComponent<SteamVR_TrackedObject> ();
		device = SteamVR_Controller.Input ((int)trackedObj.index);
		Random.seed = 48914561;

	}//End of Starts

	
	// Update is called once per frame
	void FixedUpdate () {		

		device = SteamVR_Controller.Input ((int)trackedObj.index);
		axisNumber = device.GetAxis (Valve.VR.EVRButtonId.k_EButton_Axis0);

		if (device.GetPress(SteamVR_Controller.ButtonMask.Touchpad) && canMove) {
			//Debug.Log (axisNumber[1]);
			body.SimpleMove (transform.forward*moveSpeed*axisNumber[1]);
		}








		if (device.GetPressDown (SteamVR_Controller.ButtonMask.Trigger)) {
			triggerPressed = true;
			//Debug.Log ("Trigger button pressed");
		} 
		else if (device.GetPressUp (SteamVR_Controller.ButtonMask.Trigger)) {
			triggerPressed = false;
			//Debug.Log ("Trigger button unpressed");
		}
		if (!body.isGrounded) {
			gravity -= 9.81f * Time.deltaTime;
			body.Move (new Vector3 (0, gravity*Time.deltaTime, 0));
		} else {
			gravity = 0;
		}

		if (developerMode) {
			if (device.GetPressUp (SteamVR_Controller.ButtonMask.Grip)) {
				Instantiate (glowStick, new Vector3 (this.transform.position.x, this.transform.position.y + 2f, this.transform.position.z), Quaternion.identity);
			}
		}

	}//End of Update

	public void disableMovement() {
		canMove = false;
	}

	public void enableMovement() {
		canMove = true;
	}
}
