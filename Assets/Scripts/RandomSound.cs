﻿using UnityEngine;
using System.Collections;

public class RandomSound : MonoBehaviour {

    private AudioSource sound;
    public AudioClip[] explosionSound;

    // Use this for initialization
    void Start () {
        sound = GetComponent<AudioSource>();
        sound.clip = explosionSound[Random.Range(0, explosionSound.Length)];
        sound.Play();
	}
	
	
}
