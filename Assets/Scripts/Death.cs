﻿using UnityEngine;
using System.Collections;

public class Death : MonoBehaviour {
	
	public int level2Loop = 1;
	private float reloadTimer = 3f;
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.O)){
            //DeathRoomLoad.reloadLevel = Application.loadedLevelName;
            UnityEngine.SceneManagement.SceneManager.LoadScene("Level Death");
            //StartCoroutine(reloadLevel(0));
        }

//		if(death && !triggeredOnce){
//			triggeredOnce = true;
//			DeathRoomLoad.reloadLevel = Application.loadedLevelName;
//			if(Application.loadedLevel == 2){
//				level2Loop = GameObject.Find("TimeTransitionManager").GetComponent<TimeTransitionManager>().trigger2Hit;
//			}
//			Application.LoadLevel("Level_Death");
//		}
	}

	public void KillPlayer(string level){
		//DeathRoomLoad.reloadLevel = level;
        /*
		if(Application.loadedLevel == 2){
			level2Loop = GameObject.Find("TimeTransitionManager").GetComponent<TimeTransitionManager>().trigger2Hit;
		}
        */
        UnityEngine.SceneManagement.SceneManager.LoadScene("Level_Death");
		StartCoroutine(reloadLevel(level));
	}

	IEnumerator reloadLevel(string reloadLevel){
		yield return new WaitForSeconds(reloadTimer);
        UnityEngine.SceneManagement.SceneManager.LoadScene(reloadLevel);
	}
}
