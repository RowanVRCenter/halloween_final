﻿using UnityEngine;
using System.Collections;

public class PickUpKey : MonoBehaviour {
	

	private bool playerInRange = false;
	public bool pickedUp = false;
	private bool activated = false;
	public GameObject ExitTorches;
	public GameObject fountainBlood;
	public GameObject fountain;
	public SecretDoorOpen secretDoor;
	public OpenDoor openLevel2Door;
	public AudioClip pickupkey;

	/*
	void OnTriggerEnter(Collider collider){
		if(collider.gameObject.tag == "GrabbingHand"){
			playerInRange = true;
		}
	}
	
	void OnTriggerExit(Collider collider){
		if(collider.gameObject.tag == "GrabbingHand"){
			playerInRange = false;
		}
	}
	*/
	
	// Update is called once per frame
	void Update () {
		if (!activated) {
			if (pickedUp) {
				secretDoor.playerHasKey = true;
				openLevel2Door.openDoor = true;
				Destroy (fountainBlood);
				Destroy (fountain);
				ExitTorches.SetActive (true);
				AudioSource.PlayClipAtPoint (pickupkey, transform.position);
				activated = true;
			}
		}
	}
}
