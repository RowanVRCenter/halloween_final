﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {

	private Transform cam;
	private Vector3 camStartPosition;
	float delayTimer;
	float delayTimerReset = 0.0001f;

	void Awake(){
		Random.seed = 12345;
	}

	// Use this for initialization
	void Start () {
		delayTimer = delayTimerReset;
		cam = transform;
		camStartPosition = cam.localPosition;
	}
	
	// Update is called once per frame
	void Update () {

		if(delayTimer > 0){
			delayTimer -= Time.deltaTime;
		}else{
			cam.localPosition = camStartPosition;
			ShakeCam();
			delayTimer = delayTimerReset;
		}


	}

	void ShakeCam(){
		Vector3 offsetVector = new Vector3(Random.Range(1f,25f)/100f,Random.Range(1f,25f)/100f,Random.Range(1f,25f)/100f);
		cam.localPosition = camStartPosition + offsetVector;
	}
}
