﻿using UnityEngine;
using System.Collections;

public class CoffinShake : MonoBehaviour {

	public Transform player;
	private Transform coffin;
	private Vector3 coffinStartPosition;
	float delayTimer;
	float delayTimerReset = 0.005f;
	float shakeTimer;
	float shakeTimerReset = 1f;
	bool playerInRange = false;
	
	void Awake(){
		Random.seed = 12345;
	}
	
	// Use this for initialization
	void Start () {
		delayTimer = delayTimerReset;
		shakeTimer = shakeTimerReset;
		coffin = transform;
		coffinStartPosition = coffin.position;
	}

	void OnTriggerEnter(Collider collider){
		if(collider.gameObject.tag == "Player"){
			playerInRange = true;
		}
	}

	// Update is called once per frame
	void Update () {

			if(playerInRange && delayTimer > 0){
				delayTimer -= Time.deltaTime;
				shakeTimer -= Time.deltaTime;
			}else if(playerInRange && shakeTimer > 0){
				coffin.position = coffinStartPosition;
				ShakeCam();
				delayTimer = delayTimerReset;
			}
		
	}
	
	void ShakeCam(){
		Vector3 offsetVector = new Vector3(Random.Range(-30f,30f)/100f,Random.Range(1f,10f)/100f,Random.Range(1f,20f)/100f);
		coffin.position = coffinStartPosition + offsetVector;
	}
}
