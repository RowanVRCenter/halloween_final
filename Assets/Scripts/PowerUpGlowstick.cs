﻿using UnityEngine;
using System.Collections;

public class PowerUpGlowstick : MonoBehaviour {

    public GameObject lightningGlowstick;
    public GameObject lightningBallEffect;
    public float deltaScale;
    public float scaleTime;

    private Vector3 origScale;
    private Vector3 glowStickPos;
    private Vector3 glowStickVel;
    private Vector3 glowStickAngularVel;
    private bool isRunning = false;



	// Use this for initialization
	void Start () {
	
	}

	

    void OnTriggerEnter(Collider c)
    {
        if(c.tag == "Glowstick")
        {
            if(!isRunning )
            {
                glowStickPos = c.transform.position;
                glowStickVel = c.GetComponent<Rigidbody>().velocity;
                Debug.Log(glowStickVel);
                c.enabled = false;
                Destroy(c.gameObject);
                StartCoroutine(ScaleBall(glowStickPos, glowStickVel));
                isRunning = true;
            }
        }
    }

    IEnumerator ScaleBall(Vector3 glowStickpos, Vector3 glowStickVel)
    {
        GameObject currentLightningBallEffect = Instantiate(lightningBallEffect, glowStickPos, Quaternion.identity) as GameObject;
        origScale = currentLightningBallEffect.GetComponent<Transform>().localScale;

        for (float i = 0; i < scaleTime; i += Time.deltaTime)
        {
            currentLightningBallEffect.GetComponent<Transform>().localScale = Vector3.Lerp(origScale, origScale * deltaScale, i / scaleTime);
            yield return null;
        }

        GameObject currentLightningGlowstick = Instantiate(lightningGlowstick, glowStickpos, Quaternion.identity) as GameObject;
        currentLightningGlowstick.GetComponent<Rigidbody>().velocity = glowStickVel;

        for (float i = 0; i < scaleTime; i += Time.deltaTime)
        {
            currentLightningBallEffect.GetComponent<Transform>().localScale = Vector3.Lerp(origScale * deltaScale, origScale, i / scaleTime);
            yield return null;
        }

        Destroy(currentLightningBallEffect.gameObject);
        isRunning = false;
    }


}
