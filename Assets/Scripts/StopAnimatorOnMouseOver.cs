﻿using UnityEngine;
using System.Collections;

public class StopAnimatorOnMouseOver : MonoBehaviour {
	
	public Animator anim;
	public Collider collider;

	// Use this for initialization
	void Start () {
	
		anim = GetComponent<Animator>();
		anim.Play("Rotate");
		collider = GetComponent<Collider>();

	}
	
	// Update is called once per frame
	void Update () {
	
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if(collider.Raycast( ray,out hit, 100.0f))
		{
			anim.enabled = false;
		}
		else
			anim.enabled = true;

	}
}
