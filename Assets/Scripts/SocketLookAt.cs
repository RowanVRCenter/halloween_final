﻿using UnityEngine;
using System.Collections;

public class SocketLookAt : MonoBehaviour
{

    public float timeLag;
    public Camera cam;
    private float counter;
    private Vector3 currentPos;
    private Vector3 lastPos;
    private Vector3 lookPos;

    // Use this for initialization
    void Start()
    {
        currentPos = cam.transform.position;
        lastPos = currentPos;
    }

    // Update is called once per frame
    void Update()
    {



        lookPos = Vector3.Lerp(currentPos, cam.transform.position, Time.deltaTime / timeLag);




        transform.LookAt(lookPos);
        currentPos = lookPos;
    }
}
