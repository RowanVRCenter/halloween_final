﻿using UnityEngine;
using System.Collections;

public class AttackBushing : MonoBehaviour {

    public GameObject lightningBallEffect;
    public GameObject explosion;
    public ShieldMovement shield;
    public GameObject lightning1;
    public GameObject lightning2;
    public Collider triggerZone;

    public float scaleTime;
    public float scaleDownSpeed;
    public float deltaScale;

    private Vector3 origScale;
    private bool destroyed = false;
    private BossFight boss;

    // Use this for initialization
    void Start () {
        boss = GetComponentInParent<Transform>().GetComponentInParent<BossFight>();
	}
	
    
    void OnCollisionEnter(Collision c)
    {
        if(c.transform.tag == "Lightstick" && !destroyed)
        {
            if (c.gameObject.GetComponent<Glowstick>().activated)
            {
                ContactPoint contact = c.contacts[0];
                Vector3 contactPos = contact.point;
                StartCoroutine(ExplodeWithLightning(contactPos));
                Destroy(c.gameObject);
            }
        }

    }

    IEnumerator ExplodeWithLightning(Vector3 c)
    {
        Debug.Log("exploding with lightning");

        Instantiate(explosion, c, Quaternion.identity);

        GameObject currentLightningBallEffect = Instantiate(lightningBallEffect, c, Quaternion.identity) as GameObject;
        origScale = currentLightningBallEffect.GetComponent<Transform>().localScale;


        for (float i = 0; i < scaleTime; i += Time.deltaTime)
        {
            currentLightningBallEffect.GetComponent<Transform>().localScale = Vector3.Lerp(origScale, origScale * deltaScale, i / scaleTime);
            yield return null;
        }

        for (float i = 0; i < scaleTime/scaleDownSpeed; i += Time.deltaTime)
        {
            currentLightningBallEffect.GetComponent<Transform>().localScale = Vector3.Lerp(origScale * deltaScale, origScale, i / (scaleTime/scaleDownSpeed));
            yield return null;
        }

        Destroy(currentLightningBallEffect.gameObject);
        this.GetComponent<GlowstickShocker>().completelyDisabled = true;
        triggerZone.enabled = false;
        lightning1.SetActive(false);
        lightning2.SetActive(false);
        destroyed = true;

        yield return new WaitForSeconds(2f);
        shield.MoveBelow();
        yield return new WaitForSeconds(5f);
        boss.BushingKilled();
    }
}
