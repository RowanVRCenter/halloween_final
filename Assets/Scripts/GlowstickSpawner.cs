﻿using UnityEngine;
using System.Collections;

public class GlowstickSpawner : MonoBehaviour {

    public GameObject glowstick;
    public GameObject[] bossRoomGlowsticks;
    public float respawnTimer = 4f;

    private Vector3[] origGlowstickPositions;
    private float origTimer;

	// Use this for initialization
	void Start () {
        origTimer = respawnTimer;
        origGlowstickPositions = new Vector3[bossRoomGlowsticks.Length];

        for(int i = 0; i < bossRoomGlowsticks.Length; i++)
        {
            origGlowstickPositions[i] = bossRoomGlowsticks[i].transform.position;
        }
	}
	
	// Update is called once per frame
	void Update () {
        respawnTimer -= Time.deltaTime;
        if(respawnTimer <= 0)
        {
            respawnTimer = origTimer;
            StartCoroutine(SpawnGlowsticks());
        }
	}

    IEnumerator SpawnGlowsticks()
    {
        for (int i = 0; i < bossRoomGlowsticks.Length; i++)
        {
            if(bossRoomGlowsticks[i] == null)
            {
                GameObject temp = Instantiate(glowstick, origGlowstickPositions[i], Quaternion.identity) as GameObject;
                bossRoomGlowsticks[i] = temp;
            }
            yield return null;
        }     
    }
}
