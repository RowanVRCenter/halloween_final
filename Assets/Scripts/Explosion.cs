﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

    public bool explode;  

    private ParticleSystem[] effects;
    private AudioSource sounds;
    private Light flare;

	private bool exploded;
    
    void Start () {
        effects = GetComponentsInChildren<ParticleSystem>();
        sounds = GetComponent<AudioSource>();
        flare = GetComponentInChildren<Light>();

        if (!explode)
        {
            Explode();
        }
    }
	

    public void Explode()
    {
		if(!exploded)
        	StartCoroutine(ExplosionEffects());
    }

    IEnumerator ExplosionEffects()
    {
        sounds.Play();
        yield return new WaitForSeconds(1.7f);
        flare.intensity = 4;
        for (int i = 0; i < effects.Length; i++)
        {
            effects[i].Play();
        }
        yield return new WaitForSeconds(.07f);
        flare.intensity = 0;
		exploded = true;

        yield return new WaitForSeconds(10f);
        Destroy(this.gameObject);
    }
}
