﻿using UnityEngine;
using System.Collections;

public class AmbientSound : MonoBehaviour {

    public AudioSource ambientSource;
    public AudioSource chaseSource;
    public AudioClip ambient;
    public AudioClip chase;
    public float fadeTime = 2f;
    public float maxVolume = 1f;

    private bool fadingIn = false;
    private bool fadingOut = false;
    private bool beingChased;

	// Use this for initialization
	void Start () {
        ambientSource = GetComponents<AudioSource>()[0];
        ambientSource.clip = ambient;
        chaseSource = GetComponents<AudioSource>()[1];
        chaseSource.clip = chase;
        
        ambientSource.Play();
	}

    public void StopAllSound()
    {
        ambientSource.Stop();
        chaseSource.Stop();
    }

    IEnumerator ChaseBegin()
    {
        while (!beingChased)
        {
            if (fadingOut == false && fadingIn == false)
            {
                StartCoroutine(FadeOut(ambientSource, fadeTime));
                StartCoroutine(FadeIn(chaseSource, fadeTime));
                beingChased = true;
            }
            yield return null;
        }
    }

    IEnumerator ChaseEnd()
    {
        while (beingChased)
        {
            if (fadingOut == false && fadingIn == false)
            {
                StartCoroutine(FadeIn(ambientSource, fadeTime));
                StartCoroutine(FadeOut(chaseSource, fadeTime));
                beingChased = false;
            }
            yield return null;
        }
    }

	public void Enclosing()
	{
		StopCoroutine(ChaseBegin());
		StopCoroutine(ChaseEnd());
		StartCoroutine(ChaseBegin());
	}

	public void Slacking()
	{
		StopCoroutine(ChaseEnd());
		StopCoroutine(ChaseBegin());
		StartCoroutine(ChaseEnd());
	}

    IEnumerator FadeIn(AudioSource audioSource, float FadeTime)
    {
        fadingIn = true;
        audioSource.volume = 0;
        audioSource.Play();
        while (audioSource.volume < maxVolume)
        {
            audioSource.volume += Time.deltaTime / FadeTime;
            yield return null;
        }
        audioSource.volume = maxVolume;
        fadingIn = false;
    }
    IEnumerator FadeOut(AudioSource audioSource, float FadeTime)
    {
        fadingOut = true;
        while (audioSource.volume > 0)
        {
            audioSource.volume -= Time.deltaTime / FadeTime;
            yield return null;
        }
        audioSource.Stop();
        audioSource.volume = maxVolume;
        fadingOut = false;
    }
}
