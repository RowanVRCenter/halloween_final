﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class ScaryClothDoll : MonoBehaviour {

	public Transform target;
	private AudioSource sound;

	//Controller Parameters
	public static int paramBoolWalk = Animator.StringToHash("WalkBool");
	public static int paramBoolSway = Animator.StringToHash("SwayBool");
	public static int paramBoolDead = Animator.StringToHash("DeadBool");
	public static int paramBoolSit = Animator.StringToHash("SitBool");
	public static int paramTriggerStrike = Animator.StringToHash("StrikeTrigger");
	public static int paramTriggerStand = Animator.StringToHash("StandTrigger");

	public float animIdleLength;
	public float animIdleLookLength;
	public float animSitLength;
	public float animStandLength;
	public float animStrikeLength;
	public float animWalkLength;
	public float animSwayLength;
	public float animDieLength;

	public Texture2D happyTex;
	public Texture2D angryTex;
	public Texture2D deadTex;

	protected Animator anim;
	protected Material mat;
	protected NavMeshAgent agent;

	//Animation States
	protected static int stateIdle = Animator.StringToHash("Idle");
	protected static int stateIdleLook = Animator.StringToHash("IdleLook");
	protected static int stateSit = Animator.StringToHash("Sit");
	protected static int stateStand = Animator.StringToHash("Stand");
	protected static int stateStrike = Animator.StringToHash("Strike");
	protected static int stateWalk = Animator.StringToHash("Walk");
	protected static int stateSway = Animator.StringToHash("Sway");
	protected static int stateDie = Animator.StringToHash("Die");


	public bool testBool = false;
	public bool testBool2 = false;

	private OffMeshLinkData link;
	public float testValue;
	public float targetDistance;
	private bool attacking;
	private bool jumping;
	public Transform testTransform;
	public Transform head;
	private Rigidbody rb;
	public AmbientSound ambience;
	private float gravity;
	private bool chasing;
	public Transform playerHead;
	public AudioClip killScream;
    private DamageEffect playerHealth;

	void Awake()
	{
		anim = GetComponent<Animator>();
		mat =  transform.FindChild("Doll_Head").GetComponent<SkinnedMeshRenderer>().material;
		mat.mainTexture = happyTex;
		agent = GetComponent<NavMeshAgent> ();
		agent.stoppingDistance = 0.5f;
		Init();
	}

	void Start()
	{
		sound = GetComponent<AudioSource> ();
		rb = GetComponent<Rigidbody> ();

		RuntimeAnimatorController ac = anim.runtimeAnimatorController;
		for (int i = 0; i < ac.animationClips.Length; i++) {
			if (ac.animationClips [i].name == "Sit") {
				animSitLength = ac.animationClips [i].length;
			}
			if (ac.animationClips [i].name == "Walk") {
				animWalkLength = ac.animationClips [i].length;
			}
			if (ac.animationClips [i].name == "Strike") {
				animStrikeLength = ac.animationClips [i].length;
			}
			if (ac.animationClips [i].name == "Stand") {
				animStandLength = ac.animationClips [i].length;
			}
		}
	}

	public void Update()
	{

		if (testBool) {
			DollAttack (testTransform);
		}
		if (testBool2) {
			StartCoroutine(AttackSequence());
		}
		/*
		if (target != null) {
			if (target.tag == "Head") {
				targetDistance = agent.remainingDistance;
				if (agent.remainingDistance < 30 && !chasing) {
					ambience.Enclosing ();
					chasing = true;
				} else if (chasing) {
					ambience.Slacking ();
					chasing = false;
				}
			}
		}*/

		if (agent.isOnOffMeshLink && !jumping) {
			Debug.Log ("Jump");
			StartCoroutine (Jump ());
		}

		anim.SetFloat ("Speed", agent.velocity.magnitude);
		if (target != null && !anim.GetBool(paramBoolSit) && agent.isOnNavMesh) {
			agent.destination = target.position;
			head.LookAt (target.position);

		}
	}

	public void DollAttack(Transform player)
	{
		if (!chasing) {
			StartCoroutine (ChaseSequence (player));
			chasing = true;
		}
	}

	IEnumerator ChaseSequence(Transform player) {
		//Debug.Log ("Doll is following      ~Saad");
		anim.SetBool(paramBoolSit, false);
		mat.mainTexture = angryTex;
		yield return new WaitForSeconds (animStandLength);
		target = player;
		ambience.Enclosing ();
		sound.Play ();
	}

	IEnumerator AttackSequence() {
		//Debug.Log ("Doll attacking...");
		attacking = true;
		agent.Stop();
		anim.SetTrigger (paramTriggerStrike);
		yield return new WaitForSeconds (animStrikeLength);
		if (agent.remainingDistance < 2) {
            playerHealth = playerHead.gameObject.GetComponent<DamageEffect>();
            playerHealth.Hit(1f);
            if (playerHealth.health <= 0)
            {
                StartCoroutine(GameOver());
            }
            else
            {
                agent.Resume();
            }
        } else {
			agent.Resume ();
		}
		attacking = false;
	}

	public void SetSit(bool value)
	{
		anim.SetBool(paramBoolSit, value);
	}

	public void Die()
	{
		mat.mainTexture = deadTex;
		anim.SetBool(paramBoolDead, true);
	}

	public void Init()
	{
		mat.mainTexture = happyTex;
		anim.SetBool(paramBoolWalk, false);
		anim.SetBool(paramBoolSway, false);
		anim.SetBool(paramBoolSit, true);
		anim.SetBool(paramBoolDead, false);
	}

	public void Strike()
	{
		mat.mainTexture = angryTex;
		anim.SetTrigger(paramTriggerStrike);
	}

	public void Stand()
	{
		anim.SetTrigger (paramTriggerStand);
	}
	
	protected IEnumerator StepThroughAnimations()
	{
		yield return new WaitForSeconds (3.0f);
		anim.SetBool(paramBoolWalk, true);
		yield return new WaitForSeconds (3.0f);
		anim.SetBool(paramBoolSway, true);
		yield return new WaitForSeconds (3.0f);
		anim.SetBool(paramBoolSway, false);
		anim.SetBool(paramBoolSit, true);
		yield return new WaitForSeconds (3.0f);
		anim.SetBool(paramBoolSit, false);
		yield return new WaitForSeconds (3.0f);
		anim.SetTrigger(paramTriggerStrike);
		yield return new WaitForSeconds (3.0f);
		Die();
	}

	public void OnTriggerStay(Collider c) {
		if (c.tag == "Head" && !attacking && agent.isActiveAndEnabled) {
			StartCoroutine (AttackSequence ());
		}
	}

	IEnumerator GameOver()
	{
		ambience.chaseSource.Stop ();
		ambience.ambientSource.Stop ();

		sound.clip = killScream;
		this.GetComponent<Rigidbody> ().isKinematic = true;
		this.GetComponent<CapsuleCollider> ().enabled = false;
		agent.enabled = false;

		transform.SetParent (playerHead);

		this.transform.localPosition = new Vector3 (0, -.83f, .3f);

		this.transform.localRotation = Quaternion.Euler (0, 180, 0);
		yield return null;
		sound.Play ();
	}

	IEnumerator Jump()
	{
		rb.useGravity = true;
		jumping = true;
		link = agent.currentOffMeshLinkData;
		Vector3 mid = (link.endPos - link.startPos) / 2 + link.startPos;
		Debug.Log (mid);
		mid = new Vector3(mid.x, transform.position.y + 2, mid.z);
		float t=0;
		while (t < .5f) {
			Vector3 newPos = Vector3.Slerp (link.startPos, mid, t*2);
			transform.position = newPos;
			t += Time.deltaTime;
			yield return null;
		}
		while (t > 0.5f && t < 2) {
			gravity -= 9.81f * Time.deltaTime;
			rb.AddForce (new Vector3 (0, gravity*Time.deltaTime, 0));
			t += Time.deltaTime;
			yield return null;
		}
		agent.CompleteOffMeshLink ();
		agent.Resume ();
		jumping = false;
		rb.useGravity = false;
	}


}
