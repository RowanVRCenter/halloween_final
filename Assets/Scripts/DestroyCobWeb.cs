using UnityEngine;
using System.Collections;

public class DestroyCobWeb : MonoBehaviour {

	void Start(){
	}

	void OnTriggerEnter(Collider collider){
		if(collider.gameObject.tag == "Player"){
			StartCoroutine("destroyWeb");
		}
	}

	IEnumerator destroyWeb(){
		GetComponent<AudioSource>().Play();
		yield return new WaitForSeconds(GetComponent<AudioSource>().clip.length);
		Destroy(gameObject);
	}
}
