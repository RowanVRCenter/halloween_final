﻿using UnityEngine;
using System.Collections;

public class HeartShield : MonoBehaviour {

	public enum ShieldDeployment
	{
		DeployUpwards,
		DestroyDownwards,
	}

	private GameObject[] shields;
	private ShieldMovement shieldMove;
	private int shieldsDeployed;
	public bool allShieldsDeployed;
    public float rotateSpeed = 2f;

	public ShieldDeployment function = ShieldDeployment.DeployUpwards;

	// Use this for initialization
	void Start () {
		shields = GameObject.FindGameObjectsWithTag ("HeartShield");
		foreach (GameObject shield in shields) {
			shieldMove = shield.GetComponent<ShieldMovement> ();
            shieldMove.Initialize(function != ShieldDeployment.DestroyDownwards);
        }
    }

    void Update()
    {
        transform.Rotate(Vector3.up * Time.deltaTime * rotateSpeed);
    }
	
	void OnTriggerEnter(Collider c)
	{
		Debug.Log (c);
        if (function == ShieldDeployment.DeployUpwards)
        {
            for (int i = 0; i < shields.Length; i++)
            {
                if (shieldsDeployed == shields.Length)
                {
                    allShieldsDeployed = true;
                    Debug.Log("All shields up");
                    break;
                }
                if (shields[i].GetComponent<ShieldMovement>().isDown)
                {
                    shields[i].GetComponent<ShieldMovement>().MoveAbove();
                    shieldsDeployed++;
                    Debug.Log("Moving shield up");
                    break;
                }
            }
        }
	}

    public void StartFight()
    {
        foreach (GameObject shield in shields)
        {
            shieldMove = shield.GetComponent<ShieldMovement>();
            shieldMove.MoveAbove();
        }
    }
}
