﻿using UnityEngine;
using System.Collections;

public class CameraRenderSetting : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<Camera>().renderingPath = RenderingPath.DeferredLighting;
	}
	
	// Update is called once per frame
	void Update () {
		if (GetComponent<Camera>().actualRenderingPath != RenderingPath.DeferredLighting) GetComponent<Camera>().renderingPath = RenderingPath.DeferredLighting;
	}
}
