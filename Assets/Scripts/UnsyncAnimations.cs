﻿using UnityEngine;
using System.Collections;

public class UnsyncAnimations : MonoBehaviour {

	private Animator[] childrenAnimators;

	// Use this for initialization
	void Start () {
		if (this.transform.GetComponentsInChildren<Animator> () != null) {
			childrenAnimators = this.transform.GetComponentsInChildren<Animator> ();
			for (int i = 0; i < childrenAnimators.Length; i++) {
				childrenAnimators [i].enabled = false;
			}

			StartCoroutine (Unsync ());
		} else {
			this.transform.GetComponent<Animator> ().enabled = false;
			StartCoroutine (Unsync());
		}
	}

	IEnumerator Unsync() {
		if (childrenAnimators.Length > 0) {	
			for (int i = 0; i < childrenAnimators.Length; i++) {
				float rand = Random.Range (0, 2f);
				yield return new WaitForSeconds (rand);
				childrenAnimators [i].enabled = true;
			}
		} else {
			float rand = Random.Range (0, 2f);
			yield return new WaitForSeconds (rand);
			Debug.Log (rand);
			this.transform.GetComponent<Animator> ().enabled = true;
		}
	}

}
