﻿using UnityEngine;
using System.Collections;

public class EyeballManager : MonoBehaviour {

	protected Transform[] eyes;
	public GameObject player;
	protected Quaternion[] eyeStart;

	protected bool trackPlayer = false;

	void Awake()
	{
		
		//eyes = GameObject.FindGameObjectsWithTag ("Eyeball");
	}

	void Start () {
		//player = GameObject.FindGameObjectWithTag("Player");
		eyes = GetComponentsInChildren<Transform> ();
		eyeStart = new Quaternion[eyes.Length];
		for (int i = 1; i < eyes.Length; i++)
		{
			eyeStart [i] = eyes [i].rotation;
		}
		StartCoroutine (RandomLook ());
	}

	public void TrackPlayer(bool option)
	{
		if (option != trackPlayer) {
			trackPlayer = option;
			if (trackPlayer) {
				Debug.Log ("Switching to tracking");
				StopAllCoroutines();
				StartCoroutine (TrackPlayer ());
			} else {
				Debug.Log ("Switching to random");
				StopAllCoroutines();
				StartCoroutine (RandomLook ());
			}
		}
	}

	IEnumerator RandomLook()
	{
		Debug.Log ("tracking start");
		float j = 0;
		while (j < 2)
		{
			for (int i = 1; i < eyes.Length; i++) {
				eyes [i].rotation = Quaternion.Slerp (eyes[i].rotation, eyeStart[i], j/2);
			}
			j += Time.deltaTime;
			yield return null;
		}
		while (true) {
			//Debug.Log ("Looking randomly...");
			for (int i = 1; i < eyes.Length; i++) {
				eyes [i].transform.Rotate (new Vector3 (.1f*Mathf.Sin (Time.time+i*2), .2f*Mathf.Sin (Time.time+i*4), 0), Space.Self);
			}
			yield return null;
		}

	}

	IEnumerator TrackPlayer()
	{
		Debug.Log ("tracking start");
		float j = 0;
		while (j < 2)
		{
			for (int i = 1; i < eyes.Length; i++) {
				Vector3 relativePos = player.transform.position - eyes[i].position;
				Quaternion rotation = Quaternion.LookRotation (relativePos);
				eyes [i].rotation = Quaternion.Slerp (eyes[i].rotation, rotation, j/2);
			}
			j += Time.deltaTime;
			yield return null;
		}
		while (true) {
			//Debug.Log ("Looking at player...");
			for (int i = 1; i < eyes.Length; i++) {
				eyes [i].LookAt (player.transform.position);
			}
			yield return null;
		}
	}
}
