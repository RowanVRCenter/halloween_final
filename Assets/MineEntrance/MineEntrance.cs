﻿using UnityEngine;
using System.Collections;

public class MineEntrance : MonoBehaviour {

    public Animator leftDoor;
    public Animator rightDoor;

    public void OpenDoors()
    {
        leftDoor.enabled = true;
        rightDoor.enabled = true;
    }


    void Update()
    {
        OpenDoors();
    }

}
