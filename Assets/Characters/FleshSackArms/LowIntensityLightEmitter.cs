﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Light))]
public class LowIntensityLightEmitter : MonoBehaviour {

	public float castDistance = 20.0f;

	protected LightReaction currentlyStriking = null;

	void Awake()
	{
	}

	// Use this for initialization
	void Start ()
	{
	}
	
	// Update is called once per frame
	void LateUpdate () {
	
		Ray ray = new Ray(transform.position, transform.forward);
		RaycastHit rayHit = new RaycastHit ();

		if( Physics.Raycast(ray, out rayHit,  castDistance))
		{
			LightReaction lr = rayHit.collider.GetComponent<LightReaction>();
			if(lr != null)
			{
				if(lr != currentlyStriking)
				{
					if(currentlyStriking != null)
					{
						currentlyStriking.EndReaction(LightReaction.LightReactionType.lowIntensity);
					}
					currentlyStriking = lr;
					currentlyStriking.Reaction(LightReaction.LightReactionType.lowIntensity);
				}
				else
				{
					currentlyStriking.Reaction(LightReaction.LightReactionType.lowIntensity);
				}
			}
			else
			{
				if(currentlyStriking != null)
				{
					currentlyStriking.EndReaction(LightReaction.LightReactionType.lowIntensity);
					currentlyStriking = null;
				}
			}
		}
		//When we don't hit any colliders
		else
		{
			if(currentlyStriking != null)
			{
				currentlyStriking.EndReaction(LightReaction.LightReactionType.lowIntensity);
				currentlyStriking = null;
			}
		}

	}
}
