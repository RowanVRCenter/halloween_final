﻿using UnityEngine;
using System.Collections;

public class LightReaction : MonoBehaviour {

	public enum LightReactionType
	{
		highIntensity, 
		lowIntensity
	}

	virtual public void Reaction( LightReactionType type)
	{
	}

	virtual public void EndReaction( LightReactionType type)
	{
	}

}
