﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Light))]
public class HighIntensityLightEmitter : MonoBehaviour {

	public float castDistance = 20.0f;

	protected LightReaction currentlyStriking = null;

	void Awake()
	{
	}

	// Use this for initialization
	void Start ()
	{
	}

	// Update is called once per frame
	void LateUpdate () {

		Ray ray = new Ray(transform.position, transform.forward);

		RaycastHit rayHit = new RaycastHit();
		
		if( Physics.Raycast(ray, out rayHit,  castDistance))
		{
			LightReaction lr = rayHit.collider.GetComponent<LightReaction>();
			if(lr != null)
			{
				if(lr != currentlyStriking)
				{
					if(currentlyStriking != null)
					{
						currentlyStriking.EndReaction(LightReaction.LightReactionType.highIntensity);
					}
					currentlyStriking = lr;
					currentlyStriking.Reaction(LightReaction.LightReactionType.highIntensity);
				}
				else
				{
					currentlyStriking.Reaction(LightReaction.LightReactionType.highIntensity);
				}
			}
			else
			{
				if(currentlyStriking != null)
				{
					currentlyStriking.EndReaction(LightReaction.LightReactionType.highIntensity);
					currentlyStriking = null;
				}
			}
		}
		//When we don't hit any colliders
		else
		{
			if(currentlyStriking != null)
			{
				currentlyStriking.EndReaction(LightReaction.LightReactionType.highIntensity);
				currentlyStriking = null;
			}
		}
		
	}
}
