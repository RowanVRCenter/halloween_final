﻿using UnityEngine;
using System.Collections;

public class FleshSackArms : LightReaction {

	protected Animator animator;
	private float delay;
	private bool triggered = false;
	int isSwayingBoolParam = Animator.StringToHash("isSwaying");
	int isViolentBoolParam = Animator.StringToHash("isViolent");

	void Awake()
	{
		animator = GetComponent<Animator> ();
		Random.seed = 5;
	}

	// Use this for initialization
	void Start () {
		delay = Random.value*10f;
		//animator.SetBool(isSwayingBoolParam,true);
	}

	void OnTriggerEnter(Collider collider){
		if(collider.tag == "Player"){
			animator.SetBool(isSwayingBoolParam, false);
			animator.SetBool(isViolentBoolParam,true);
			GetComponent<AudioSource>().Play ();
		}
	}

	void OnTriggerExit(Collider collider){
		if(collider.tag == "Player"){
			animator.SetBool(isViolentBoolParam,false);
			animator.SetBool(isSwayingBoolParam, true);
			GetComponent<AudioSource>().Stop ();
		}
	}

	// Update is called once per frame
//	void Update () {
//		if(delay <= 0f && !triggered){
//			triggered = true;
//			animator.SetBool(isSwayingBoolParam, true);
//		}else{
//			delay -= Time.deltaTime;
//		}
//	}

//	public override void Reaction (LightReactionType type)
//	{
//		switch (type)
//		{
//			case LightReactionType.lowIntensity:
//				animator.SetBool(isSwayingBoolParam, true);
//				break;
//			case LightReactionType.highIntensity:
//				animator.SetBool(isViolentBoolParam, true);
//				break;
//		}
//	}
//
//	public override void EndReaction (LightReactionType type)
//	{
//		Debug.Log ("End Reaction");
//		switch (type)
//		{
//			case LightReactionType.lowIntensity:
//				animator.SetBool(isSwayingBoolParam, false);
//				break;
//			case LightReactionType.highIntensity:
//				animator.SetBool(isViolentBoolParam, false);
//				break;
//		}
//	}
}
