﻿using UnityEngine;
using System.Collections;

public class HeartManager : MonoBehaviour {

	public GameObject heart;
	public GameObject bloodSpear;
	public GameObject bloodIdle;
	public GameObject bloodDeath;
	public GameObject bloodExplosion;
	public HeartBeatLight light1;
	public HeartBeatLight light2;
	//public GameObject spear;
	public GameObject spearEndPoint;
	public AudioSource levelaudio;

	public GameObject player;

	protected Animation anim;
	protected AnimationState animationState;

	protected bool startDeath = false;

	public GameObject playerLight;

	void Awake()
	{
		anim = heart.GetComponent<Animation>();
		animationState = anim["C4D Animation Take"];
	}

	// Use this for initialization
	void Start () {
		//spear.SetActive(false);
		bloodSpear.SetActive(false);
		bloodIdle.SetActive(true);
		bloodDeath.SetActive(false);
		bloodExplosion.SetActive(false);
	}

	//Shove rod in heart
	//Speed up heart over 
	//Blood geysers
	//Blood explosion

	IEnumerator HeartDeath()
	{
		//0.5 seconds spear throw
		//15.469 seconds heart speeds up 1x to 4.5x
		//

		GetComponent<AudioSource>().Play();
		float spearThrustTime = 0.5f;
		//spear.SetActive(true);
		//spear.transform.LookAt(spearEndPoint.transform.position);
		float currentTime = 0.0f;

		//Vector3 spearDirection = spearEndPoint.transform.position - spear.transform.position;
		//Vector3 spearToHeartDistance = spearDirection;
		//Shove model in heart
		//for(  ;  spearDirection.magnitude > 0.5f;  ){
			//levelaudio.volume -= Time.deltaTime/spearThrustTime;
			//spear.transform.position = spear.transform.position + spearToHeartDistance * Time.deltaTime / spearThrustTime ;
			yield return null;
			//spearDirection = spearEndPoint.transform.position - spear.transform.position;
		//}
		levelaudio.Stop ();
		bloodSpear.SetActive(true);

		//Speed Up Heart
		currentTime = 0.0f;
		float duration = 14.9696f; // seconds
		float maxRate = 4.5f;

		float currentRate = maxRate / duration;
		float heartRate = 1.0f;

		for( ; currentTime < duration; currentTime += Time.deltaTime)
		{
			yield return null;
			light1.pulseRate += Time.deltaTime;
			light2.pulseRate += Time.deltaTime;
			heartRate += currentRate * Time.deltaTime;
			animationState.speed = heartRate;
		}
		//Shoot out gysers
		bloodDeath.SetActive(true);
		//Explode
		yield return new WaitForSeconds(0.86f);
		bloodExplosion.SetActive(true);
		yield return new WaitForSeconds(0.5f);

		float flyingThroughTubeTime = 8f;
		float speed = 18f;
		float maxSpeed = 40f;
		float distance = speed * flyingThroughTubeTime;
		float currentDistance = 0.0f;

		playerLight.SetActive(true);

		for( ; currentDistance < distance; currentDistance += speed * Time.deltaTime)
		{
			player.transform.position += new Vector3( 0.0f, speed * Time.deltaTime, 0.0f);
			if(speed < maxSpeed)
				speed += Time.deltaTime;

			yield return null;
		}

		Application.LoadLevel(Application.loadedLevel + 1);
	}

}
