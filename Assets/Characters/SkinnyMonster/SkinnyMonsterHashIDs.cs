﻿using UnityEngine;
using System.Collections;

public class SkinnyMonsterHashIDs : MonoBehaviour {
	//States
	public int crouchState = Animator.StringToHash("Base Layer.Crouch");
	public int idleBreathState = Animator.StringToHash("Base Layer.Idle_Breath");
	public int idleTurnHeadState = Animator.StringToHash("Base Layer.Idle_TurnHead");
	public int peekToLeftState = Animator.StringToHash("Base Layer.Peek_ToLeft");
	public int peekLeftState = Animator.StringToHash("Base Layer.Peek_Left");
	public int peekToLeftReturnState = Animator.StringToHash("Base Layer.Peek_ToLeft_Return");
	public int peekToRightState = Animator.StringToHash("Base Layer.Peek_ToRight");
	public int peekRightState = Animator.StringToHash("Base Layer.Peek_Right");
	public int peekToRightReturnState = Animator.StringToHash("Base Layer.Peek_ToRight_Return");
	
	public int strikeLeftState = Animator.StringToHash("Base Layer.Strike_Left");
	public int screamState = Animator.StringToHash("Base Layer.Scream");
	public int screamLongState = Animator.StringToHash("Base Layer.Scream_Long");
	public int screamWaveState = Animator.StringToHash("Base Layer.Scream_Wave");

	public int lumberState = Animator.StringToHash("Base Layer.Lumber");

	public int scaredState = Animator.StringToHash("Base Layer.Scared");

	//Parameters
	public int crouchBool = Animator.StringToHash("Crouch");
	public int peekLeftBool = Animator.StringToHash("PeekLeft");
	public int peekRightBool = Animator.StringToHash("PeekRight");

	public int strikeLeftHash = Animator.StringToHash("StrikeLeft");
	public int screamHash = Animator.StringToHash("Scream");
	public int screamLongHash = Animator.StringToHash("ScreamLong");
	public int screamWaveHash = Animator.StringToHash("ScreamWave");

	public int lumber = Animator.StringToHash("Lumber");

	public int scaredEnter = Animator.StringToHash("Scared_Enter");
	public int scared = Animator.StringToHash("Scared");
	public int scaredExit = Animator.StringToHash("Scared_Exit");

}
