﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class CreepyDollAvatarController : MonoBehaviour {
	
	public enum States
	{
		stand = 0,
		stepLeft = 1,
		stepRight = 2,
		stepLeftArmRaised = 3,
		stepRightArmRaised = 4,
		standArmsRaised = 5,
		kill = 6
	}

	public enum Moods
	{
		happy,
		crazy
	}

	protected Animator anim;
	protected int stateParam = Animator.StringToHash("State");

	public Texture2D happyTex;
	public Texture2D crazyTex;
	public GameObject dollspot;

	void Awake()
	{
		anim = GetComponent<Animator>();
	}

	void Start()
	{
		dollspot.SetActive(false);
		SetState(States.stand);
	}

	public void SetState( States state)
	{
		switch( state )
		{
			case States.stand:
				anim.SetInteger(stateParam,0);
				break;
			case States.stepLeft:
				anim.SetInteger(stateParam,1);
				break;
			case States.stepRight:
				anim.SetInteger(stateParam,2);
				break;
			case States.stepLeftArmRaised:
				anim.SetInteger(stateParam,3);
				break;
			case States.stepRightArmRaised:
				anim.SetInteger(stateParam,4);
				break;
			case States.standArmsRaised:
				anim.SetInteger(stateParam,5);
				break;
			case States.kill:
				anim.SetInteger(stateParam,6);
				break;	
		}
	}

	public void KillPlayer(GameObject player)
	{
		//transform.position = player.transform.position;
		//transform.rotation = player.transform.rotation;

		transform.position = player.transform.position + new Vector3(0,Camera.main.transform.position.y-1,0) + player.transform.forward;// + player.transform.right*0.4f;
		transform.rotation = player.transform.rotation;
		transform.RotateAround(transform.position,Vector3.up,193.4f);
		transform.parent = player.transform;
		dollspot.SetActive(true);
		//Vector3 tempPosition = transform.localPosition;
		//tempPosition.x = 0f;
		//transform.localPosition = tempPosition;
		//transform.LookAt(player.transform.position+getReal3D.Input.GetSensor("Head").position-(Vector3.up*0.9f));
		//Transform killSpotTransform = GameObject.Find("DollKillSpot").transform;
		//transform.position = killSpotTransform.position;
		//transform.parent = killSpotTransform;
		//transform.LookAt(GameObject.FindGameObjectWithTag("MainCamera").transform.position-new Vector3(0f,.75f,0f));
		SetMood(Moods.crazy);
		SetState(States.kill);
	}

	public void SetMood( Moods mood)
	{
		switch(mood)
			{
			case Moods.happy:
				gameObject.GetComponentInChildren<SkinnedMeshRenderer>().GetComponent<Renderer>().material.mainTexture = happyTex;
				break;
			case Moods.crazy:
				gameObject.GetComponentInChildren<SkinnedMeshRenderer>().GetComponent<Renderer>().material.mainTexture = crazyTex;
				break;
			}
	}
}
