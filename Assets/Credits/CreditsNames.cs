﻿using UnityEngine;
using System.Collections;

public class CreditsNames : MonoBehaviour {

	public TextMesh textMesh;
	public float textScrollSpeed = 1.0f;
	public float endHeightOffset = 10.0f;
	protected float textHeight;

	
	protected string credits = 
		"Created By:\n\n\n\n" + 
		"Saad Sohail\n\n" +
		"Kane Heaning\n\n\n\n" +
		"Michael Bucceroni\n\n" +
		"George Lecakes\n\n" +
		"Matthew Montagnino\n\n" + 
		"\n\n\n\n\n\n\n\n" +
        "Songs by Kevin MacLeod (incompetech.com):\n\n\n\n" +
        "Chase Pulse Faster\n\n" +
        "Controlled Chaos\n\n" +
        "Echoes of Time\n\n" +
        "Minima\n\n" +
        "Phantom from Space\n\n" +
        "Stormfront\n\n" +
        "The Descent\n\n\n" +
        "Licensed under Creative Commons:\n\n" + 
        "By Attribution 3.0 License\n\n" +
        "http://creativecommons.org/licenses/by/3.0/\n\n\n" +
		"This game used the following\n" +
		"audio clips from FreeSound.org:\n" +
		"\n\n\n\n" + 
		"Swamp_at_dusk - MeffyEllis\n" +
		"Malum - CadereSounds\n" +
		"Water Wading - Motion_S\n" +
		"Cave_ambient - xDimebagx\n" +
		"Metal Atmosphere 4 - Dan Oberbauer\n" +
		"Das Artefakt 5 - Fantozzi\n" +
		"Porte pierre 2 - Slanesh\n" +
		"horror cinema 5 2014 - CadereSounds\n" +
		"Doorslam - HerbertBoland\n" +
		"Keys-chaves04-drop - andre.rocha.nacimento\n" +
		"Landing on ground - celebration\n" +
		"Swishes with feather - tobbarebble\n" +
		"open rolling door 150328_0543 MS - klankbeeld\n" +
		"horror gate - Tomlija\n" +
		"engine2_down - ReadeOnly\n" +
		"Draw Bridge Lower - hykenfreak\n" +
		"Prison,jail,a gard opens and closes doors and locks \n of prison cells with keys in an echoing corridor - YleArkisto\n" +
		"Cave_ambient - xDimebagx\n" +
		"subaudible thumper - parabolix\n" +
		"Substrate 1 - the_odds\n" +
		"ambienttest2 - Vitouliss\n" +
		"ScaryViolins - lennyboy\n" +
		"DenseCrunchRe-Rated_CaveIn - zimbot\n" +
		"Water splash - ceberation\n" +
		"ChainLodingBayDoor01 - mmaruska\n" +
		"Water_drip-echo2 - roscoetoon\n" +
		"Acid_movement_E2_115BPM - Snapper4298\n" +
		"Genuine Heartbeat Bass - Speedenza\n" +
		"Human Heartbeat - Speedenza\n" +
		"horror monster 01 140804_0339 - klankbeeld\n" +
		"DroneBreath - Zoltoks\n" +
		"Chicken Meat Slime - qubodup\n" +
		"meat<CsG> - csaszi\n" +
		"Wet Meat - RICHERlandTV\n" +
		"Cinematic Rise -  PatrickLieberkind\n" +
		"CringeScare_3Hit_fast - TheGoliath\n" +
		"Impact Explosion - CGEffex\n" +
		"00001NoisevaultAlleyWater - cAPSLOCK7\n" +
		"woosh_02 - Glaneur de sons\n" +
		"SFX_HIT_BRASS_C# - tiagusilva37\n" +
		"\n\n\n" +
		"All other sounds not in the public domain \n" +
		"were recorded by Michael Bucceroni\n" + 
		"\n\n\n\n\n\n\n\n" +
		"Rowan Virtual Reality Center\n" + 
		"\n\n\n\n\n\n\n\n" +
		"Thank you for playing\n";



	protected bool isScrolling = false;

	IEnumerator ScrollText()
	{

		float scrollAmount = 0.0f;

		while( scrollAmount < textHeight + endHeightOffset)
		{
			float displacement = textScrollSpeed * Time.deltaTime;
			scrollAmount += displacement;
			textMesh.gameObject.transform.position += Vector3.up * displacement;
			yield return null;
		}

	}

	// Use this for initialization
	void Start () {
	
		textMesh.text = credits;
		textHeight = textMesh.GetComponent<Renderer>().bounds.size.y;
	}
	
	// Update is called once per frame
	void Update () {
	
		if (!isScrolling)
		{
			isScrolling = true;
			StartCoroutine ("ScrollText");
		}

	}
}
