﻿Shader "Custom/UndulatingUVs" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_NormalTex ("Normal Map", 2D) = "bump" {}
		_ScrollXSpeed ("X Scroll Speed", float) = 0.1
		_ScrollYSpeed ("Y Scroll Speed", float) = 0.1
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _MainTex;
		sampler2D _NormalTex;
		fixed _ScrollXSpeed;
		fixed _ScrollYSpeed;

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
		
			float2 scrolledUV = IN.uv_MainTex;
			float yScrollValue = sin( _Time  * 60)/80.0;
			float xScrollValue =  0;
			scrolledUV += float2( xScrollValue, yScrollValue );
			
			float3 normalMap = UnpackNormal(tex2D(_NormalTex, scrolledUV));
		
			half4 c = tex2D (_MainTex, scrolledUV);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
			o.Normal = normalMap.rgb;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
