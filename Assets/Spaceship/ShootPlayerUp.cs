﻿using UnityEngine;
using System.Collections;

public class ShootPlayerUp : MonoBehaviour {

	public GameObject player;
	public Rigidbody rigidBody;

	public GameObject playerStartPosition;

	protected bool hasFlown = false;

	public GameObject spacefleet;
	public GameObject waypointManager;
	public AudioClip birds,ship,another;
	public Light sun1,sun2;
	private float sun1val,sun2val;
	public Skybox sky;
	private Color skycolor;

	// Use this for initialization
	void Start () {
		sky.material.SetColor("_Tint",Color.grey);
		rigidBody.useGravity = false;
		spacefleet.SetActive(false) ;
		waypointManager.SetActive(false);
		GetComponent<AudioSource>().clip = birds;
		GetComponent<AudioSource>().volume = 1f;
		sun1val = sun1.intensity;
		sun2val =sun2.intensity;
	}


	IEnumerator ThrowPlayerUp()
	{
		//Length in air 5.416 seconds

		//Down 6.513 seconds
		GetComponent<AudioSource>().Play();

		float a2 = -0.91828f;
		float a1 = 10.501f;
		float duration = 11.435f;
		float currentTime = 0.0f;
		float startTime = Time.time;
		for(  ; currentTime < duration ;  )
		{
			currentTime = Time.time - startTime;
			player.transform.position = new Vector3(playerStartPosition.transform.position.x, currentTime * a1 + currentTime * currentTime * a2 - 110f , playerStartPosition.transform.position.z);
			yield return null;
		}
		playerStartPosition.GetComponent<AudioSource>().Play ();
		yield return new WaitForSeconds(5f);
		rigidBody.useGravity = true;
		spacefleet.SetActive(true);
		waypointManager.SetActive(true);
		yield return new WaitForSeconds(5);
		while(GetComponent<AudioSource>().volume>0){
			GetComponent<AudioSource>().volume -= Time.deltaTime/2;
			sun1.intensity -= sun1val*Time.deltaTime/2.01f;
			sun2.intensity -= sun2val*Time.deltaTime/2.01f;
			sky.material.SetColor("_Tint",Color.Lerp(sky.material.GetColor("_Tint"),Color.black,Time.deltaTime));
			yield return null;
		}
		GetComponent<AudioSource>().clip = ship;
		GetComponent<AudioSource>().volume = 1f;
		GetComponent<AudioSource>().Play();
		yield return new WaitForSeconds(ship.length-3f);
		GetComponent<AudioSource>().clip = another;
		GetComponent<AudioSource>().Play ();
		yield return new WaitForSeconds(another.length);
		Application.LoadLevel(5);
	}

	// Update is called once per frame
	void Update () {

		if(!hasFlown)
			StartCoroutine(ThrowPlayerUp());
			hasFlown = true;


	}
}
