﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spaceship : MonoBehaviour {

	public Light topLight;
	public Light bottomLight;
	
	public Light blink1;
	public Light blink2;
	public Light blink3;
	public Light blink4;

	public float rotationRate = 0.5f;

	IEnumerator PulseLight( Light light, float pulseInSeconds)
	{
		float waitTimeInSeconds = 0.1f;

		float pulseIncPerTick = waitTimeInSeconds / light.intensity;

		float maxLightValue = light.intensity;
		float currentPulseValue = 0.0f;

		while(true)
		{
			for( ; currentPulseValue < maxLightValue ; )
			{
				currentPulseValue += pulseIncPerTick;
				light.intensity = currentPulseValue;
				yield return new WaitForSeconds(waitTimeInSeconds);
			}

			for( ; currentPulseValue > 0.0f ; )
			{
				currentPulseValue -= pulseIncPerTick;
				light.intensity = currentPulseValue;
				yield return new WaitForSeconds(waitTimeInSeconds);
			}

		}
	}

	// Use this for initialization
	void Start () {
		StartCoroutine (PulseLight(topLight, 3.0f));
		StartCoroutine (PulseLight(bottomLight, 3.0f));

		StartCoroutine (PulseLight(blink1, 0.5f));
		StartCoroutine (PulseLight(blink2, 0.5f));
		StartCoroutine (PulseLight(blink3, 0.5f));
		StartCoroutine (PulseLight(blink4, 0.5f));

	}
	
	// Update is called once per frame
	void Update () {
	
		transform.Rotate (0f, rotationRate, 0f);

	}
}
