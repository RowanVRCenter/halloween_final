﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//[ExecuteInEditMode]
public class SpaceWaypointManager : MonoBehaviour 
{
	public enum SpaceWaypointActions
	{
		none,
		lightsConverge,
		scream,
		idle
	}

	public Transform waypointsInGroup;
	public GameObject spaceshipGroup;
	public float speed = 20.0f;
	public SpaceshipTrackedLights spaceshipTrackedLights;

	protected bool isNavigating = false;
	protected List<Transform> wayPoints;
	protected int currentWaypoint = 0;

	protected Vector3 currentDirection;
	public bool isLooping = true;	//Bridges the start and end of waypoints
	private GameObject player;
	
	private static int CompareTransform( Transform a, Transform b)
	{
		return a.name.CompareTo(b.name);
	}

	// Use this for initialization
	void Start ()
	{
		wayPoints = new List<Transform>();
		player = GameObject.FindGameObjectWithTag("Player");
		ExtractWaypoints();
		PlaceAtStart();
		IncWaypoint();
		StartNavigating();
	}

	public void IsLooping(bool value)
	{
		if(value)
		{
			isLooping = true;
			SetWaypointGroup(waypointsInGroup);
		}

		else
		{
			isLooping = false;
			SetWaypointGroup(waypointsInGroup);
		}
	}


	protected void PlaceAtStart()
	{
		Debug.Log (wayPoints[0]);
		spaceshipGroup.transform.position = wayPoints[0].transform.position;
	}

	protected void IncWaypoint()
	{

		if(currentWaypoint < wayPoints.Count - 1)
		{
			currentWaypoint++;
		}
		else{
			currentWaypoint = 0;
		}

	}

	protected void ExtractWaypoints()
	{
		foreach(Transform child in waypointsInGroup.transform)
		{
			wayPoints.Add(child);
			child.GetComponent<MeshRenderer>().enabled = false;
		}

		wayPoints.Sort(CompareTransform);

		//If we are looping re-add the first node
//		if(isLooping)
//		{
//			wayPoints.Add(wayPoints[0]);
//		}
	}

	protected void MoveTowardCurrentWaypoint()
	{
		Vector3 direction = (wayPoints[currentWaypoint].position - spaceshipGroup.transform.position).normalized;

		//Move toward target;
		spaceshipGroup.transform.position += direction * speed * Time.deltaTime;
	}

	public void SignalWaypointReached( Transform waypointTriggered, SpaceWaypointActions action)
	{

		//Have we reached the end?

		if( !isLooping && currentWaypoint == wayPoints.Count - 1)
		{
			isNavigating = false;
		}
		Debug.Log ("SignalWaypointReached");
		if(waypointTriggered == wayPoints[currentWaypoint])
		{
			IncWaypoint();
		}

		switch( action)
		{
		case SpaceWaypointActions.none:
			break;
		case SpaceWaypointActions.lightsConverge:
			spaceshipTrackedLights.LightsConverge();
			break;
		case SpaceWaypointActions.scream:
			isNavigating = false;

			break;
		case SpaceWaypointActions.idle:
			//Idle();
			break;
		}
	}

	/// <summary>
	/// Sets the waypoint group.
	/// </summary>
	/// <param name="transform">Transform.</param>
	public void SetWaypointGroup(Transform transform)
	{
		wayPoints.Clear();
		waypointsInGroup = transform;
		currentWaypoint = 0;
		ExtractWaypoints();
		PlaceAtStart();
		IncWaypoint();
		StartNavigating();
	}
	/*
	protected IEnumerator SwipeAndWait()
	{
		isNavigating = false;
		skinnyMonsterScript.SetState(SkinnyMonster.SkinnyMonsterState.strike);
		//Trigger door to open here...
		yield return new WaitForSeconds(1.5f);

		isNavigating = true;
	}

	protected IEnumerator ScreamAndWait()
	{
		isNavigating = false;
		skinnyMonsterScript.SetState(SkinnyMonster.SkinnyMonsterState.scream);
		//Trigger door to open here...
		yield return new WaitForSeconds(4.0f);
		isNavigating = true;
	}

	private void Idle(){
		Debug.Log("idle state");
		isNavigating = false;
		skinnyMonsterScript.SetState(SkinnyMonster.SkinnyMonsterState.idle);
	}
	*/
	public void StartNavigating()
	{
		isNavigating = true;
		//skinnyMonsterScript.SetState(SkinnyMonster.SkinnyMonsterState.lumber);
	}

	// Update is called once per frame
	void Update () {
	
		if(isNavigating)
		{
			MoveTowardCurrentWaypoint();
		}
	}
}
