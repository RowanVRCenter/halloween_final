﻿using UnityEngine;
using System.Collections;

public class SpaceshipTrackedLights : MonoBehaviour {

	public Light l1;
	public Light l2;
	 
	public GameObject p1;
	public GameObject p2;

	public GameObject player;

	protected float currentAngle = 0.0f;
	protected float speedToLockOnLights = 200.0f;

	protected bool freeRoamingLights = true;

	protected float currentSpeed = 0.0f;

	// Use this for initialization
	void Start () {
	
	}

	IEnumerator LockOnPlayer()
	{
		p1.transform.parent = null;
		p2.transform.parent = null;
		l1.transform.parent = null;
		l2.transform.parent = null;
		Vector3 p1ToPlayer = player.transform.position - p1.transform.position;
		Vector3 p2ToPlayer = player.transform.position - p2.transform.position;

		while( p1ToPlayer.magnitude > 0.5f && p2ToPlayer.magnitude > 0.5f )
		{
			Debug.Log (p1ToPlayer);
			Debug.Log (p2ToPlayer);
			p1.transform.position = p1.transform.position + p1ToPlayer.normalized * Time.deltaTime * speedToLockOnLights;
			p2.transform.position = p2.transform.position + p2ToPlayer.normalized * Time.deltaTime * speedToLockOnLights;
			l1.transform.LookAt (p1.transform.position);
			l2.transform.LookAt (p2.transform.position);
			yield return null;
			p1ToPlayer = player.transform.position - p1.transform.position;
			p2ToPlayer = player.transform.position - p2.transform.position;
		}

		//Pull up player 1

		Vector3 toMothership = (transform.position - player.transform.position);
//		while( toMothership.magnitude > 80.0f )
//		{
//			currentSpeed += 0.25f;
//			player.transform.position = player.transform.position + toMothership.normalized * Time.deltaTime * currentSpeed;
//			yield return null;
//			toMothership = (transform.position - player.transform.position);
//		}


	}

	public void LightsConverge()
	{
		Debug.Log ("LightsConverge");
		freeRoamingLights = false;
		StartCoroutine(LockOnPlayer());
	}

	// Update is called once per frame
	void LateUpdate () {

		if(freeRoamingLights)
		{
			currentAngle += Time.deltaTime;
			p1.transform.localPosition = new Vector3( Mathf.Sin(currentAngle) * 30.0f, -20.0f, Mathf.Cos(currentAngle) * 30.0f );
			p2.transform.localPosition = new Vector3( Mathf.Sin(currentAngle + 1.8f) * 30.0f, -20.0f, Mathf.Cos(currentAngle + 1.8f) * 30.0f );

			l1.transform.LookAt (p1.transform.position);
			l2.transform.LookAt (p2.transform.position);
		}

	}
}
