﻿using UnityEngine;
using System.Collections;

public class WaypointSpaceTriggeredLightsConverge : MonoBehaviour {


	public SpaceWaypointManager waypointManager;

	// Use this for initialization
	void Start () {
		//waypointManager = GameObject.FindGameObjectWithTag("WaypointManager").GetComponent<WaypointManager>();
	}

	// Update is called once per frame
	void Update () {
	}

	void OnTriggerEnter( Collider other)
	{
		if(other.tag == "Spaceship")
		{
			waypointManager.SignalWaypointReached(this.gameObject.transform, SpaceWaypointManager.SpaceWaypointActions.lightsConverge);
		}
	}

}
