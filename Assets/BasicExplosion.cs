﻿using UnityEngine;
using System.Collections;

public class BasicExplosion : MonoBehaviour {

    private Light flare;

    // Use this for initialization
    void Start () {
        flare = GetComponentInChildren<Light>();
        StartCoroutine(ExplosionEffects());
    }

    IEnumerator ExplosionEffects()
    {
        float i = 0;
        while (i<1)
        {
            flare.intensity = Mathf.Lerp(0, 1, i);
            i += Time.deltaTime;
        }
        i = 0;
        while (i < 1)
        {
            flare.intensity = Mathf.Lerp(1,0, i);
            i += Time.deltaTime;
        }
        yield return new WaitForSeconds(30f);
        Destroy(this.gameObject);
    }
}
