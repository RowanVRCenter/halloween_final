﻿using UnityEngine;
using System.Collections;

public class WandController : MonoBehaviour
{
    private SteamVR_TrackedObject trackedObj;
    private GameObject pickup;

    Usable currentUsableObject = null;

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    void OnTriggerEnter(Collider collider) 
    {
        Usable usable = collider.gameObject.GetComponent<Usable>();

        Debug.Log("Wand Trigger Enter: " + collider.name);

        if (usable != null)
        {
                
            currentUsableObject = usable;
        }
    }

    void OnTriggerExit(Collider collider)
    {
        Usable usable = collider.gameObject.GetComponent<Usable>();

        Debug.Log("Wand Trigger Exit: " + collider.name);

        if (usable == currentUsableObject)
        {
            currentUsableObject = null;
        }
    }

    void Update()
    {
        SteamVR_Controller.Device controllerDevice = SteamVR_Controller.Input((int)trackedObj.index);

        if (currentUsableObject != null && controllerDevice.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad))
        { 
            currentUsableObject.Use(this.gameObject);
        }
    }

 }