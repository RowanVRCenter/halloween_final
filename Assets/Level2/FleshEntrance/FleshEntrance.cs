﻿using UnityEngine;
using System.Collections;

public class FleshEntrance : MonoBehaviour {

    Animator animator;

	// Use this for initialization
	void Start ()
    {
        animator = GetComponent<Animator>();
	}

    public void OpenMouth()
    {
        animator.SetTrigger("OpenMouth");
        GetComponent<AudioSource>().Play();
    }
}
