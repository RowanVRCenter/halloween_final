﻿using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour {

    SteamVR_ControllerManager controllerManager;

    SteamVR_TrackedObject leftTrackedObject;
    SteamVR_TrackedObject rightTrackedObject;

    SteamVR_Controller.Device leftControllerDevice = null;
    SteamVR_Controller.Device rightControllerDevice = null;

    public Light leftLight;
    public Light rightLight;

    public bool Moving { get;  private set; }

    public float movementSpeed = 1.5f;
    public float stoppingRadius = 0.1f;
    protected Vector3 target;

    protected GameObject targetGraphic;
    protected CharacterController characterController;

    static Vector3 gravity = new Vector3(0.0f, -9.81f, 0.0f);

    // Use this for initialization
    void Start () {
        targetGraphic = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        targetGraphic.transform.localScale = new Vector3(0.1f,0.1f,0.1f);
        targetGraphic.gameObject.GetComponent<SphereCollider>().enabled = false;
        characterController = GetComponent<CharacterController>();

        Moving = false;
        controllerManager = GetComponent<SteamVR_ControllerManager>();
        leftTrackedObject = controllerManager.left.GetComponent<SteamVR_TrackedObject>();
        rightTrackedObject = controllerManager.right.GetComponent<SteamVR_TrackedObject>();

        leftLight.enabled = !leftLight.enabled;
        rightLight.enabled = !rightLight.enabled;
    }
	
    void MovementUpdate(SteamVR_Controller.Device rightControllerDevice)
    {

        Vector3 move = gravity * Time.deltaTime;

        if (Moving) //We are moving
        {
            if (rightControllerDevice.GetTouch(SteamVR_Controller.ButtonMask.Trigger)) //Trigger still pressed
            {
                //Get distance to target
                Vector3 difference = target - this.gameObject.transform.position;
                if(difference.magnitude <= stoppingRadius) //Distance is close 
                {
                    Moving = false;
                    target = Vector3.zero;
                }
                else //Distance is far
                {
                    //Move toward target
                    //this.gameObject.transform.position += difference.normalized * movementSpeed * Time.deltaTime;
                    
                    move += difference.normalized * movementSpeed * Time.deltaTime;
                    //Debug.Log(move);
                }
            }

            else//Trigger No Longer Pressed  
            {
                Moving = false;
                target = Vector3.zero;
            }
        }

        else //We are not moving
        {
                RaycastHit rayHit;
                Ray ray = new Ray( rightTrackedObject.gameObject.transform.position, rightTrackedObject.gameObject.transform.forward);

                if (Physics.Raycast(ray, out rayHit, 20.0f))
                    {
                   // Debug.Log("Name: " + rayHit.collider.name);
                    if (rightControllerDevice.GetTouch(SteamVR_Controller.ButtonMask.Trigger)) //Check if trigger has been pressed
                    {

                        Moving = true;
                        target = rayHit.point;
                    }
                    targetGraphic.transform.position = rayHit.point;
                }
            }

        characterController.Move(move);

    }

	// Update is called once per frame
	void LateUpdate () {

        leftControllerDevice = SteamVR_Controller.Input((int)leftTrackedObject.index);
        rightControllerDevice = SteamVR_Controller.Input((int)rightTrackedObject.index);

        if(leftControllerDevice.GetTouchDown(SteamVR_Controller.ButtonMask.Grip))
        {
            leftLight.enabled = !leftLight.enabled;
        }

        
        if (rightControllerDevice.GetTouchDown(SteamVR_Controller.ButtonMask.Grip))
        {
            rightLight.enabled = !rightLight.enabled;
        }
        

        MovementUpdate(rightControllerDevice);

    }
}
