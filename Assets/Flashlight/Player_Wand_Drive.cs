﻿using UnityEngine;
using System.Collections;
using Valve.VR;

public class Player_Wand_Drive : MonoBehaviour {

    SteamVR_ControllerManager controllerManager;

    SteamVR_TrackedObject leftTrackedObject;
    SteamVR_TrackedObject rightTrackedObject;
    [SerializeField]
    GameObject headCam;

    SteamVR_Controller.Device leftControllerDevice = null;
    SteamVR_Controller.Device rightControllerDevice = null;

    public Light leftLight;
    public Light rightLight;

    public bool Moving { get; private set; }

    public float movementSpeed = 1f;

    protected CharacterController characterController;

    static Vector3 gravity = new Vector3(0.0f, -9.81f, 0.0f);

    // Use this for initialization
    void Start()
    {

        characterController = GetComponent<CharacterController>();

        Moving = false;
        controllerManager = GetComponent<SteamVR_ControllerManager>();
        leftTrackedObject = controllerManager.left.GetComponent<SteamVR_TrackedObject>();
        rightTrackedObject = controllerManager.right.GetComponent<SteamVR_TrackedObject>();

        leftLight.enabled = !leftLight.enabled;
        rightLight.enabled = !rightLight.enabled;
    }

    void MovementUpdate(SteamVR_Controller.Device rightControllerDevice, SteamVR_Controller.Device leftControllerDevice)
    {
        bool moving = false;
        Vector3 move = Vector3.zero;
        Vector3 moveRight = Vector3.zero,moveLeft = Vector3.zero;
        if (rightControllerDevice.GetTouch(SteamVR_Controller.ButtonMask.Trigger)) //Check if trigger has been pressed
        {
            moving = true;
            moveRight = rightTrackedObject.gameObject.transform.forward;
            moveRight.y = 0;
            moveRight = Vector3.Normalize(moveRight);
            //move += rightTrackedObject.gameObject.transform.forward*rightControllerDevice.GetAxis(EVRButtonId.k_EButton_SteamVR_Touchpad).y;
            //move += rightTrackedObject.gameObject.transform.right*rightControllerDevice.GetAxis(EVRButtonId.k_EButton_SteamVR_Touchpad).x;
        }
        if (leftControllerDevice.GetTouch(SteamVR_Controller.ButtonMask.Trigger)) //Check if trigger has been pressed
        {
            moving = true;
            moveLeft = leftTrackedObject.gameObject.transform.forward;
            moveLeft.y = 0;
            moveLeft = Vector3.Normalize(moveLeft);
            //move += leftTrackedObject.gameObject.transform.forward * leftControllerDevice.GetAxis(EVRButtonId.k_EButton_SteamVR_Touchpad).y;
            //move += leftTrackedObject.gameObject.transform.right * leftControllerDevice.GetAxis(EVRButtonId.k_EButton_SteamVR_Touchpad).x;
        }
        if (moving)
        {
            move = moveRight + moveLeft;
            move.y = 0;
            move = Vector3.Normalize(move) * movementSpeed;
        }
        characterController.SimpleMove(move);

    }

    // Update is called once per frame
    void LateUpdate()
    {

        leftControllerDevice = SteamVR_Controller.Input((int)leftTrackedObject.index);
        rightControllerDevice = SteamVR_Controller.Input((int)rightTrackedObject.index);

        if (leftControllerDevice.GetTouchDown(SteamVR_Controller.ButtonMask.Grip))
        {
            leftLight.enabled = !leftLight.enabled;
        }


        if (rightControllerDevice.GetTouchDown(SteamVR_Controller.ButtonMask.Grip))
        {
            rightLight.enabled = !rightLight.enabled;
        }


        MovementUpdate(rightControllerDevice, leftControllerDevice);

    }
}
